#!/usr/bin/env bash
## start PostgreSQL container

CONTAINER_CONFIG_PATH=/etc/docker.d
SOFTWARE_NAME=POSTGRESQL
CONFIG_SUFFIX=.${SOFTWARE_NAME,,}.conf
IMAGE_NAME=postgres
CONTAINER_INNER_PORT=5432
DATA_VOLUMN_PATH=~/data/${SOFTWARE_NAME,,}

Usage="Usage: $0 [CONFIG_NAME]"

if [ ! -z "$1" ]; then
  CONFIG_NAME=$1
fi

listContainerConfigFiles ${CONFIG_SUFFIX}

while [ -z "${CONFIG_NAME}" ]; do
  read -p "Input ${SOFTWARE_NAME} CONFIG_NAME: " CONFIG_NAME
done

## check file
filefound=0
if [ -f ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX} ]; then
  filefound=1
  FILE_NAME=${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
fi

if [ $filefound -eq 0 ]; then
  echo "The config file ${CONFIG_NAME} is not exist. Please check it."
  echo "If you do not have a config file, please initial one."
  exit 1
fi

## import environment variable
. ${FILE_NAME}

PORTS_EXPOSE=""
if [ "${EXPOSE_PORT}" != "0" -a "${EXPOSE_PORT}" != "" ]; then
  PORTS_EXPOSE="-p ${EXPOSE_PORT}:${CONTAINER_INNER_PORT}"
fi

COMMAND=(
docker run
  --name ${CONTAINER_NAME}
  --network ${CONTAINER_NETWORK}
  ${PORTS_EXPOSE}
  --restart always
  -v ${DATA_VOLUMN_PATH}/${CONTAINER_NAME}:/var/lib/postgresql/data
  -e POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
  -e POSTGRES_USER=${POSTGRES_USER}
  -e POSTGRES_DB=${POSTGRES_DB}
  --env-file ${CONTAINER_CONFIG_PATH}/common.env
  -d ${IMAGE_NAME}:${IMAGE_TAG}
)

echo ===== BEGIN =====
echo ${COMMAND[*]}
echo ===== END =====

read -n1 -p "Are you sure to start? (y/N) " YN
echo
if [ "${YN}" == "y" -o "${YN}" == "Y" ]; then
  echo -n Start ${SOFTWARE_NAME} container ${CONTAINER_NAME}...
  if [ ! -d "${DATA_VOLUMN_PATH}/${CONTAINER_NAME}" ]; then
    mkdir -p ${DATA_VOLUMN_PATH}/${CONTAINER_NAME}
  fi
  ${COMMAND[*]}
  echo Done!
else
  echo Quit!
fi
