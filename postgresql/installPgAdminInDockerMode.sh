#!/usr/bin/env bash
## install PgAdmin

SOFTWARE_NAME=PgAdmin
type installSoftwareInDockerMode

if [ $? != 0 ]; then
  echo "installSoftwareInDockerMode" not found!
  exit;
fi

installSoftwareInDockerMode ${SOFTWARE_NAME}
