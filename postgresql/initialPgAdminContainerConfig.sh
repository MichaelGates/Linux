#!/usr/bin/env bash
## Initial pgadmin docker config file

CONTAINER_CONFIG_PATH=/etc/docker.d
SOFTWARE_NAME=PGADMIN
CONFIG_SUFFIX=.${SOFTWARE_NAME,,}.conf

CONTAINER_INNER_PORT=80

DEFAULT_CONTAINER_NAME=${SOFTWARE_NAME,,}
DEFAULT_IMAGE_TAG=latest
DEFAULT_CONTAINER_NETWORK=webserver
DEFAULT_DOMAIN_NAME=localhost
DEFAULT_EXPOSE_PORT=5050

DEFAULT_PGADMIN_DEFAULT_PASSWORD=password
DEFAULT_POSTGRES_DB=postgres
DEFAULT_PGADMIN_CONFIG_ENHANCED_COOKIE_PROTECTION=True
DEFAULT_PGADMIN_CONFIG_LOGIN_BANNER="Authorised users only!"
DEFAULT_PGADMIN_CONFIG_CONSOLE_LOG_LEVEL=10

echo Initial ${SOFTWARE_NAME} container config file ...

while [ -z "${CONFIG_NAME}" ]; do
  read -p "Input CONFIG_NAME: " CONFIG_NAME
  if [ -f ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX} ]; then
    echo "${CONFIG_NAME}${CONFIG_SUFFIX} already existed."
    echo ===== BEGIN ===== ${CONFIG_NAME}${CONFIG_SUFFIX} =====
    cat ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
    echo ===== END ===== ${CONFIG_NAME}${CONFIG_SUFFIX} =====
    read -n1 -p "Overright? (y/N/q) " YN
    echo
    if [ "${YN}" == "q" -o "${YN}" == "Q" ]; then
      exit 0
    fi
    if [ "${YN}" != "y" -a "${YN}" != "Y" ]; then
      CONFIG_NAME=
    fi
  fi
done

read -p "Input CONTAINER_NAME (default:${DEFAULT_CONTAINER_NAME}): " CONTAINER_NAME
if [ -z "${CONTAINER_NAME}" ]; then
  CONTAINER_NAME=${DEFAULT_CONTAINER_NAME}
fi

read -p "Input IMAGE_TAG (default:${DEFAULT_IMAGE_TAG}): " IMAGE_TAG
if [ -z "${IMAGE_TAG}" ]; then
  IMAGE_TAG=${DEFAULT_IMAGE_TAG}
fi

read -p "Input CONTAINER_NETWORK (default:${DEFAULT_CONTAINER_NETWORK}): " CONTAINER_NETWORK
if [ -z "${CONTAINER_NETWORK}" ]; then
  CONTAINER_NETWORK=${DEFAULT_CONTAINER_NETWORK}
fi

read -p "Input EXPOSE_PORT (0 for not expose, default:${DEFAULT_EXPOSE_PORT}): " EXPOSE_PORT
if [ -z "${EXPOSE_PORT}" ]; then
  EXPOSE_PORT=${DEFAULT_EXPOSE_PORT}
fi

while [ -z "${PGADMIN_DEFAULT_EMAIL}" ]; do
  read -p "Input PGADMIN_DEFAULT_EMAIL: " PGADMIN_DEFAULT_EMAIL
done

read -p "Input PGADMIN_DEFAULT_PASSWORD (default:${DEFAULT_PGADMIN_DEFAULT_PASSWORD}): " PGADMIN_DEFAULT_PASSWORD
if [ -z "${PGADMIN_DEFAULT_PASSWORD}" ]; then
  PGADMIN_DEFAULT_PASSWORD=${DEFAULT_PGADMIN_DEFAULT_PASSWORD}
fi

## initial constant shell
echo -n Generate ${SOFTWARE_NAME} container config file...
if [ ! -d ${CONTAINER_CONFIG_PATH} ]; then
  sudo mkdir -p ${CONTAINER_CONFIG_PATH}
fi
NEW_CONFIG_FILE=${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
sudo cp ${CONTAINER_CONFIG_PATH}/template${CONFIG_SUFFIX} ${NEW_CONFIG_FILE}

sudo sed -i'' -e "s/{SOFTWARE_NAME}/${SOFTWARE_NAME}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CONTAINER_NAME}/${CONTAINER_NAME}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{IMAGE_TAG}/${IMAGE_TAG}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CONTAINER_NETWORK}/${CONTAINER_NETWORK}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{EXPOSE_PORT}/${EXPOSE_PORT}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CONTAINER_INNER_PORT}/${CONTAINER_INNER_PORT}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CREATE_TIME}/`date +'%Y-%m-%d %T'`/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CREATE_USER}/`whoami`/g" ${NEW_CONFIG_FILE}

sudo sed -i'' -e "s/{PGADMIN_DEFAULT_EMAIL}/${PGADMIN_DEFAULT_EMAIL}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{PGADMIN_DEFAULT_PASSWORD}/${PGADMIN_DEFAULT_PASSWORD}/g" ${NEW_CONFIG_FILE}

echo Done!

echo ===== BEGIN ===== ${NEW_CONFIG_FILE} =====
cat ${NEW_CONFIG_FILE}
echo ===== END ===== ${NEW_CONFIG_FILE} =====

echo Generate ${SOFTWARE_NAME} container config complete!
