## PgAdmin raspberry Pi
docker run --rm \
  --name pgadmin \
  --network webserver \
  -e PGADMIN_DEFAULT_EMAIL=michaelgates.cn@gmail.com \
  -e PGADMIN_DEFAULT_PASSWORD=password \
  -d biarms/pgadmin4:4.21

