#!/usr/bin/env bash
## install PostgreSQL

SOFTWARE_NAME=PostgreSQL
type installSoftwareInDockerMode

if [ $? != 0 ]; then
  echo "installSoftwareInDockerMode" not found!
  exit;
fi

installSoftwareInDockerMode ${SOFTWARE_NAME}
