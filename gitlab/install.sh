echo Installing GitLab...

while [ -z "${GitLab_DOMAIN}" ]; do
  read -p "GitLab domain: " GitLab_DOMAIN
done

curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash

sudo EXTERNAL_URL="https://${GitLab_DOMAIN}" yum install -y gitlab-ee

## configurate gitlab
sudo sh -c "cat etc/gitlab_append.rb >> /etc/gitlab/gitlab.rb"

## reconfig gitlab
sudo gitlab-ctl reconfigure

../certbot/genCert.sh ${GitLab_DOMAIN}

cp -pr etc/gitlab-omnibus-ssl-nginx.conf /data/nginx/conf/conf.d/
sed -i "s/{DOMAIN_NAME}/${GitLab_DOMAIN}/g" /data/nginx/conf/conf.d/gitlab-omnibus-ssl-nginx.conf

sudo usermod -a -G git nginx
sudo chmod g+rx /home/git/

sudo systemctl restart nginx