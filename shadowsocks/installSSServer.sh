#!/usr/bin/env bash
## Install shadowsocks

if [ -z "${ss_port}" ]; then
  read -p 'Input ss port (3389): ' ss_port
fi

if [ -z "${ss_port}" ]; then
  ss_port=3389
fi

while [ -z "${ss_password}" ]; do
  read -p "Input ss password: " ss_password
done

set -e

## install python3 related packages
echo Install python3
sudo yum install -y python3-setuptools python3

echo Install shadowsocks...
## the version of shadowsocks in pip3 library stops at 2.8.2,
## so, we use github archive file
# sudo pip3 install shadowsocks
sudo pip3 install https://github.com/shadowsocks/shadowsocks/archive/master.zip
echo Done!

## install libsodium from source code,
## because the new encryption methods need libsodium
function installLibsodiumFromSource() {
  echo Install libsodium...
  ## 因为这库是基于C语言的，所以我们先去安装GCC
  #yum -y groupinstall "Development Tools"
  sudo yum install -y gcc make
  ## 下载最新稳定版本
  wget https://download.libsodium.org/libsodium/releases/LATEST.tar.gz
  ## 解压
  tar xf LATEST.tar.gz && cd libsodium-stable
  ## 编译
  ./configure && make -j4 && sudo make install
  sudo sh -c "echo /usr/local/lib > /etc/ld.so.conf.d/usr_local_lib.conf"
  sudo ldconfig
  cd ..
  rm -rf libsodium-stable/
  echo Done.
}
## It is seems CentOS 8 works fine without installing libsodium,
## comment it temperarily
# installLibsodiumFromSource

## another way to install libsodium using yum
## not checked yet
# sudo yum -y install libsodium

function configShadowsocks() {
  echo Copy and config ...
  sudo cp -pr conf/shadowsocks.conf /etc/shadowsocks.conf
  sudo sed -i "s/{ss_port}/${ss_port}/g" /etc/shadowsocks.conf
  sudo sed -i "s/{ss_password}/${ss_password}/g" /etc/shadowsocks.conf
  echo Done!
}
configShadowsocks

echo Install as a service...
../tools/installService.sh shadowsocks
echo Done.

echo Restart Shadowsocks...
sudo systemctl restart shadowsocks
echo Done.
