## install nacos

VERSION=1.4.0

echo You are going to download nacos-server-${version}...

wget https://github.com/alibaba/nacos/releases/download/${VERSION}/nacos-server-${VERSION}.tar.gz
tar zxvf nacos-server-${VERSION}.tar.gz

echo Done!

# cd nacos/bin
# startup.sh -m standalone
