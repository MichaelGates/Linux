## Install nods.js

## Install from EPEL
## But the nodejs version is not update to date.
## See https://nodejs.org/en/download/package-manager/
#sudo yum install -y nodejs npm --enablerepo=epel

## On RHEL, CentOS or Fedora, for Node.js v8 LTS:
## See https://nodejs.org/en/download/package-manager
curl --silent --location https://rpm.nodesource.com/setup_8.x | sudo bash -
sudo yum -y install nodejs

## To install the Yarn package manager, run:
curl -sL https://dl.yarnpkg.com/rpm/yarn.repo | sudo tee /etc/yum.repos.d/yarn.repo
sudo yum install -y yarn

