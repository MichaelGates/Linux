#!/usr/bin/env bash

## Add crontab job
## If the job already existed, do nothing.

Usage=""
Usage=${Usage}" Usage:  $0 \"cron-command\""

if [ -z "$1" ]; then
  echo ${Usage}
  exit 1
else
  command=$1
fi

## Add to crontab task
crontab -l > crontab.bak
grep_result=`grep -F "${command}" crontab.bak`
if [ -z "$grep_result" ]; then
  cp -pr crontab.bak crontab.new
  echo "${command}" >> crontab.new
  crontab crontab.new
  rm crontab.new
  echo Added "\"${command}\"" to crontab!
else
  echo Skipped! "\"${command}\"" already exists in crontab. 
fi
rm crontab.bak
