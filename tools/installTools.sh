#!/usr/bin/env bash
## Install tools

curr_path=`pwd`
script_path=`cd "$(dirname $0)" && pwd`
cd ${script_path}

INSTALL_BIN_PATH=/usr/local/bin

sudo ln -sf ${PWD}/addCrontab.sh ${INSTALL_BIN_PATH}/addCrontab
sudo ln -sf ${PWD}/removeCrontab.sh ${INSTALL_BIN_PATH}/removeCrontab
sudo ln -sf ${PWD}/checkCommands.sh ${INSTALL_BIN_PATH}/checkCommands
sudo ln -sf ${PWD}/getOSVer.sh ${INSTALL_BIN_PATH}/getOSVer
sudo ln -sf ${PWD}/osinfo.sh ${INSTALL_BIN_PATH}/osinfo

cd ${curr_path}
