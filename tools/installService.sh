
Usage=""
Usage=${Usage}" Usage:  $0 \"service-name\""

if [ -z "$1" ]; then
  echo ${Usage}
  exit 1
else
  SERVICE_NAME=$1
fi

sudo cp -pr service/${SERVICE_NAME} /etc/rc.d/init.d/
sudo chkconfig --add ${SERVICE_NAME}
