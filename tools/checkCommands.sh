#!/usr/bin/env bash

## Prerequisites check

USAGE="usage: $0 [command1] [command2] ..."

if [[ $# -eq 0 ]]; then
  echo No command to check!
  echo $USAGE
  exit 0
fi

for command in $@; do
  if ! command -v $command >/dev/null 2>&1; then 
    echo \"$command\" not installed! Abort!
    exit 1
  fi
done

