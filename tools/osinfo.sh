#!/usr/bin/env bash

## fetch OS Info
## default output "ID VERSION_ID"
## e.g. "centos 7" "centos 8" "sles 12.5"

## usage
function usage() {
  Usage="Usage: $0 [options] \n"
  Usage="${Usage}  options: \n"
  Usage="${Usage}\t -h \t : help \n"
  Usage="${Usage}\t -n \t : print the os distribution id (eg. centos)\n"
  Usage="${Usage}\t -N \t : print the os distribution name (eg. CentOS) \n"
  Usage="${Usage}\t -v \t : print the os version id (eg. 7) \n"
  Usage="${Usage}\t -V \t : print the os version (eg. 7(Core)) \n"
  echo -e ${Usage}
}

DEFAULT_FORMAT="TRUE"
ID=unknown
VERSION_ID=unknown
NAME=unknown
VERSION=unknown

## parse options
while getopts 'hnNvV' OPT; do
  case $OPT in
    h)
      HELP="TRUE"
      ;;
    n)
      PRINT_ID="TRUE"
      DEFAULT_FORMAT="FALSE"
      ;;
    N)
      PRINT_NAME="TRUE"
      PRINT_ID=""
      DEFAULT_FORMAT="FALSE"
      ;;
    v)
      PRINT_VERSION_ID="TRUE"
      DEFAULT_FORMAT="FALSE"
      ;;
    V)
      PRINT_VERSION="TRUE"
      PRINT_VERSION_ID=""
      DEFAULT_FORMAT="FALSE"
      ;;
    ?)
      usage
      exit 1
  esac
done

if [ "${HELP}" = "TRUE" -o "$1" = "--help" ]; then
  usage
  exit 0
fi

## https://unix.stackexchange.com/questions/6345/how-can-i-get-distribution-name-and-version-number-in-a-simple-shell-script
if [ -f /etc/os-release ]; then
    ## freedesktop.org and systemd
    ## contains ID, NAME, VERSION_ID and VERSION
    . /etc/os-release
elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    ID=$(lsb_release -si)
    VERSION_ID=$(lsb_release -sr)
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    ID=$DISTRIB_ID
    VERSION_ID=$DISTRIB_RELEASE
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    ID=debian
    NAME=Debian
    VERSION_ID=$(cat /etc/debian_version)
elif [ -f /etc/SuSe-release ]; then
    # Older SuSE/etc.
    echo
elif [ -f /etc/redhat-release ]; then
    # Older Red Hat, CentOS, etc.
    echo 
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
fi

if [ "${DEFAULT_FORMAT}" == "TRUE" ]; then
  echo "${ID} ${VERSION_ID}"
  exit 0
fi

if [ "${PRINT_ID}" == "TRUE" ]; then
  id_or_name="${ID}"
elif [ "${PRINT_NAME}" == "TRUE" ]; then
  id_or_name="${NAME}"
fi

if [ "${PRINT_VERSION_ID}" == "TRUE" ]; then
  version_id_or_name="${VERSION_ID}"
elif [ "${PRINT_VERSION}" == "TRUE" ]; then
  version_id_or_name="${VERSION}"
fi

function addTailingSpace() {
  if [ -n "$1" ]; then
    echo "$1 "
  else
    echo "$1"
  fi
}

output_string="${id_or_name}"
output_string=$(addTailingSpace ${output_string})
output_string="${output_string}${version_id_or_name}"

echo ${output_string}
