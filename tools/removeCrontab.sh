#!/bin/bash
Usage=""
Usage=${Usage}" Usage:  $0 \"cron-command\""

if [ -z "$1" ]; then
  echo ${Usage}
  exit 1
else
  command=$1
fi

## Add to crontab task
crontab -l > crontab.bak
grep_result=`grep -F "${command}" crontab.bak`
if [ -z "$grep_result" ]; then
  echo Skipped! "\"${command}\"" not found!
else
  sed "s|${command}||g" crontab.bak > crontab.new
  crontab crontab.new
  rm crontab.new
  echo Removed "\"${command}\"" from crontab!
fi
rm crontab.bak
