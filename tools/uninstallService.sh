
Usage=""
Usage=${Usage}" Usage:  $0 \"service-name\""

if [ -z "$1" ]; then
  echo ${Usage}
  exit 1
else
  SERVICE_NAME=$1
fi

sudo chkconfig --del ${SERVICE_NAME}
sudo rm -f /etc/rc.d/init.d/${SERVICE_NAME}
