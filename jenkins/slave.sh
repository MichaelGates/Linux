## Jenkins slave

echo Prerequisites : JAVA
## Prerequisites check
. ../tools/checkCommands.sh java

if [ $? -ne 0 ]; then
  exit 1
fi

## Add jenkins user
sudo useradd -m jenkins -d /home/jenkins --comment "Jenkins Continuous Integration Slave" 

## Add jenkins to have sudo rights
# sudo usermod -aG wheel jenkins

sudo su - jenkins
## Setting .ssh folder rights
cd ~
if [ ! -d ".ssh" ]; then
  mkdir .ssh
fi
chmod 700 -R ~/.ssh

## Setting authorized_keys rights
if [ ! -f "~/.ssh/authorized_keys" ]; then
  touch ~/.ssh/authorized_keys
fi
chmod 600 ~/.ssh/authorized_keys

exit
