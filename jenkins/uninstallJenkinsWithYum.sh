## Uninstall jenkins with yum

while [ -z "${DOMAIN_NAME}" ]; do
  read -p "Input jenkins domain: " DOMAIN_NAME
done

## Stop jenkins
sudo systemctl stop jenkins

## Remove jenkins
sudo yum remove -y jenkins

## Remove nginx conf
. ../nginx/removeNginxConf.sh ${DOMAIN_NAME}

echo Jenkins uninstalled!
