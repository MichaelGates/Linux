## Jenkins master

echo Prerequisites : JAVA

## Prerequisites check
. ../tools/checkCommands.sh java

if [ $? -ne 0 ]; then
  exit 1
fi

## Create user if not exists
JENKINS_USER=jenkins
id $JENKINS_USER >& /dev/null
if [ $? -ne 0 ]; then
  sudo useradd -m jenkins -d /home/jenkins --comment "Jenkins Continuous Integration Server" 
fi

sudu su - jenkins
## Generate ssh key if not exists
if [ ! -f "~/.ssh/id_rsa" ]; then
  yes "" | ssh-keygen -t rsa -b 2048 -N ""
fi
# ssh jenkins@host 'mkdir -p .ssh && cat >> .ssh/authorized_keys' < ~/.ssh/id_rsa.pub

## exit su
exit
