## Uninstall jenkins

echo Uninstall jenkins...

while [ -z "${DOMAIN_NAME}" ]; do
  read -p "Input jenkins domain: " DOMAIN_NAME
done

## Stop service
sudo systemctl stop jenkins

## Remove service
. ../tools/uninstallService.sh jenkins

## Remove jenkins folder
if [ -z "${JENKINS_HOME}" ]; then
  sudo rm -rf ${JENKINS_HOME}
fi

sudo userdel jenkins
sudo rm -rf /home/jenkins/

## remove nginx conf file
. ../nginx/removeNginxConf.sh ${DOMAIN_NAME}

echo Jenkins uninstalled!