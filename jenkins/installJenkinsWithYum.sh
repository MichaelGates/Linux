## Install jenkins with yum

while [ -z "${DOMAIN_NAME}" ]; do
  read -p "Input jenkins domain: " DOMAIN_NAME
done

read -p "echo Input jenkins http port\(8080\): " JENKINS_PORT
if [ -z "${JENKINS_PORT}" ]; then
  JENKINS_PORT=8080
fi

sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo
sudo rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key
sudo yum install jenkins

sudo sed -i "s/JENKINS_PORT=\"8080\"/JENKINS_PORT=\"${JENKINS_PORT}\"/g" /etc/sysconfig/jenkins

export DOMAIN_NAME
export APP_PORT=${JENKINS_PORT}

## Add nginx conf
. ../nginx/addNginxConf.sh conf/jenkins.https.conf

echo Jenkins installed!