## Start Jenkins Container

DOCKER_NETWORK=webserver
IMAGE_TAG=lts
CONTAINER_DATA_BASE=~/data/jenkins

docker network ls | grep ${DOCKER_NETWORK} > /dev/null
if [ ! $? -eq 0 ]; then
  echo Docker network ${DOCKER_NETWORK} not found!
  exit 1
fi

echo Starting jenkins ...
docker run \
  --network ${DOCKER_NETWORK} \
  --name jenkins \
  --user `id -u`:`id -g` \
  --restart always \
  -e TZ=Asia/Hong_Kong \
  -v ${CONTAINER_DATA_BASE}/main:/var/jenkins_home \
  -v ${CONTAINER_DATA_BASE}/backup/.jenkins:/var/jenkins_backup \
  -d jenkins/jenkins:${IMAGE_TAG}

echo Started!