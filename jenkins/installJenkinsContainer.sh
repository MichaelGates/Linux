## install jenkins with docker

echo Pulling jenkins lts image ...
docker pull jenkins/jenkins:lts
echo Pulled!

echo Creating jenkins home folder.
mkdir ~/.jenkins
echo Created!
