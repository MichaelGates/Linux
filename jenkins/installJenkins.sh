## Install jenkins

echo Prerequisites : Java

JENKINS_INSTALL_PATH=/usr/lib/jenkins
JENKINS_HOME="/var/lib/jenkins"

while [ -z "${DOMAIN_NAME}" ]; do
  read -p "Input jenkins domain: " DOMAIN_NAME
done

read -p "Input jenkins http port\(8080\):" JENKINS_PORT
if [ -z "${JENKINS_PORT}" ]; then
  JENKINS_PORT=8080
fi

export DOMAIN_NAME 
export APP_PORT=${JENKINS_PORT}

sudo yum install -y wget
if [ ! -f "jenkins.war" ]; then
  wget -N http://mirrors.jenkins.io/war/latest/jenkins.war
fi

## Add a user to run jenkins
sudo useradd --create-home --comment "Account for running jenkins" --shell /bin/bash jenkins

sudo mkdir -p ${JENKINS_INSTALL_PATH}
sudo mkdir -p ${JENKINS_HOME}
sudo cp -pr jenkins.war ${JENKINS_INSTALL_PATH}/
sudo chown -R jenkins:jenkins ${JENKINS_HOME}

## Add nginx conf
. ../nginx/addNginxConf.sh conf/jenkins.https.conf

## Copy jenkins config file
sudo cp -pr conf/jenkins /etc/sysconfig/
sudo sed -i "s/JENKINS_PORT=\"8080\"/JENKINS_PORT=\"${JENKINS_PORT}\"/g" /etc/sysconfig/jenkins

## Install jenkins as a system service
. ../tools/installService.sh jenkins

echo Jenkins installed!
