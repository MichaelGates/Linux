#!/usr/bin/env bash

## run ansible role certbot

## usage
function usage() {
  Usage="Usage: $0 [options] \n"
  Usage="${Usage}  options: \n"
  Usage="${Usage}\t -h        : help \n"
  Usage="${Usage}\t -H string : destination host \n"
  Usage="${Usage}\t -d string : domain name \n"
  Usage="${Usage}\t -e string : email \n"
  Usage="${Usage}\t -p string : extra params for docker container (key=value)\n"
  echo -e ${Usage}
}

## parse options
while getopts 'hH:d:e:p:' OPT; do
  case $OPT in
    h)
      HELP="TRUE";;
    H)
      host="$OPTARG";;
    d)
      domain="$OPTARG";;
    e)
      email="$OPTARG";;
    p)
      params="$OPTARG";;
    ?)
      usage
      exit 1
  esac
done

## input parameter if not set
function inputParam() {
  while [ -z "${host}" ]; do
    read -p "Input host (must defined in inventory): " host
  done

  while [ -z "${domain}" ]; do
    read -p "Input domain name: " domain
  done

  while [ -z "${email}" ]; do
    read -p "Input email: " email
  done

  extra_params=""
  if [ "${params}" ]; then
    extra_params="-e ${params}"
  fi
}
inputParam

ansible-playbook certbot.yml -e "host=${host}" -e "domain=${domain}" -e "email=${email}" ${extra_params}
