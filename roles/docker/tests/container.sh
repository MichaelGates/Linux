#!/usr/bin/env bash

## run ansible role docker

## usage
function usage() {
  Usage="Usage: $0 [options] \n"
  Usage="${Usage}  options: \n"
  Usage="${Usage}\t -h        : help \n"
  Usage="${Usage}\t -H string : destination host \n"
  Usage="${Usage}\t -f string : file defines docker container vars \n"
  Usage="${Usage}\t -e string : extra params for docker container (key=value)\n"
  echo -e ${Usage}
}

## parse options
while getopts 'hH:f:e:' OPT; do
  case $OPT in
    h)
      HELP="TRUE";;
    H)
      host="$OPTARG";;
    f)
      vars_file="$OPTARG";;
    e)
      extra_param="$OPTARG";;
    ?)
      usage
      exit 1
  esac
done

## input parameter if not set
function inputParam() {
  while [ -z "${host}" ]; do
    read -p "Input host (must defined in inventory): " host
  done

  while [ -z "${vars_file}" ]; do
    read -p "Input ansible vars file: " vars_file
  done
}
inputParam

function checkParam() {
  if [ ! -f "${vars_file}" ]; then
    echo "${vars_file} not exist!"
    exit 1
  fi
}
checkParam

ansible-playbook docker_container.yml -e "HOST=${host}" -e "vars_file=${vars_file}" -e "${extra_param}"
