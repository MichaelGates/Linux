#!/usr/bin/env bash

## run ansible role nginx

## usage
function usage() {
  Usage="Usage: $0 [options] \n"
  Usage="${Usage}  options: \n"
  Usage="${Usage}\t -h        : help \n"
  Usage="${Usage}\t -H string : destination host \n"
  Usage="${Usage}\t -d string : application domain name \n"
  Usage="${Usage}\t -m string : server mode : [host]|proxy \n"
  Usage="${Usage}\t -a string : proxied application host (when srever mode is proxy) \n"
  Usage="${Usage}\t -p string : proxied application port (when srever mode is proxy) \n"
  echo -e ${Usage}
}

## parse options
while getopts 'hH:d:m:a:p:' OPT; do
  case $OPT in
    h)
      HELP="TRUE";;
    H)
      host="$OPTARG";;
    d)
      domain="$OPTARG";;
    m)
      server_mode="$OPTARG";;
    a)
      app_host="$OPTARG";;
    p)
      app_port="$OPTARG";;
    ?)
      usage
      exit 1
  esac
done

## input parameter if not set
function inputParam() {
  while [ -z "${host}" ]; do
    read -p "Input host (must defined in inventory): " host
  done

  while [ -z "${domain}" ]; do
    read -p "Input application domain: " domain
  done

  if [ -z "${server_mode}" ]; then
    read -p "Input server mode [host(default)|proxy]: " server_mode
  fi

  if [ -z "${server_mode}" ]; then
    server_mode="host"
  fi

  if [ "${server_mode}" == "proxy" ]; then
    while [ -z "${app_host}" ]; do
      read -p "Input application host: " app_host
    done
    param_app_host="-e app_host=${app_host}"
    while [ -z "${app_port}" ]; do
      read -p "Input application port: " app_port
    done
    param_app_port="-e app_port=${app_port}"
  fi
}
inputParam

function checkParam() {
  if [ "${server_mode}" != "host" -a "${server_mode}" != "proxy" ]; then
    echo "${server_mode} must be host or proxy!"
    exit 1
  fi
}
checkParam

ansible-playbook config.yml -e "host=${host}" -e "domain=${domain}" -e "server_mode=${server_mode}" ${param_app_host} ${param_app_port}
