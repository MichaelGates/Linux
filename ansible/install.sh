#!/usr/bin/env bash

## Install ansible
## see https://www.ansible.com/resources/get-started

set -e

function installAnsibleWithYum() {
  echo Install ansible with yum
  sudo yum install -y epel-release ansible
}
installAnsibleWithYum

function checkInstallation() {
  ansible --version
  echo Ansible installed.
}
checkInstallation
