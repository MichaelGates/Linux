# Linux Services
目前以 CentOS 为主，支持 CentOS 7，目前部分兼容 CentOS 8.

## Services
目前包括下列服务
* ddclient
* docker
* nginx
* certbot
* squid
* Java 8
* Jira Software
* Jira Service Management
* Confluence
* Jenkins
* SonarQube
* Nexus
* MySQL
* phpMyAdmin
* postgreSQL
* pgAdmin

持续更新中...
