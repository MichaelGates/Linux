## Add a user to sudo group
echo This will add a new user and add to sudo group\(wheel\).
read -p "Input username:" USERNAME
sudo useradd --create-home --shell /bin/bash ${USERNAME}
sudo usermod -aG wheel ${USERNAME}
