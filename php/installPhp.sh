## Install php
NGINX_INSTALL_PATH=/data/nginx

read -p "Please input doamin name: " DOMAIN_NAME

## config repo
sudo rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

## install
sudo yum remove -y php71w-common
sudo yum install -y php72w-cli php72w-fpm php72w-mbstring php72w-pdo php72w-gd php72w-mysql php72w-xml php72w-soap php72w-xmlrpc php72w-opcache

## config php
sudo cp -pr /etc/php.ini /etc/php.ini.default
sudo sed -i '/;cgi.fix_pathinfo=1/a\cgi.fix_pathinfo=0' /etc/php.ini
sudo systemctl enable php-fpm
sudo systemctl start php-fpm

sudo setenforce 0
sudo sed -i "s/SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config

../certbot/genCert.sh ${DOMAIN_NAME}

cp -pr html/phpinfo.php ${NGINX_INSTALL_PATH}/html/
../nginx/addNginxConf.sh conf/php.https.conf

echo Php installed!
echo Try http://${DOMAIN_NAME}/phpinfo.php
