
## prepare vairable
OS_USER=`whoami`
HAPROXY_VER_MAJOR=1
HAPROXY_VER_MINOR=9
HAPROXY_VER_MAINTENANCE=2
HAPROXY_INSTALL_PATH=/home/ha/haproxy
HAPROXY_OS_USER=ha

HAPROXY_VER=${HAPROXY_VER_MAJOR}.${HAPROXY_VER_MINOR}.${HAPROXY_VER_MAINTENANCE}
SRC_FILENAME=haproxy-${HAPROXY_VER}

## enter script folder
run_path=`pwd`
script_path=`cd "$(dirname $0)" && pwd`
cd ${script_path}

## Exit immediately if a command exits with a non-zero status.
set -e

echo Install haproxy...

## Install dependencies
sudo yum install -y gcc-c++ wget pcre-devel openssl-devel

## create user if not exists
id ${HAPROXY_OS_USER} >& /dev/null
if [ $? -ne 0 ]; then
  sudo useradd --user-group ${HAPROXY_OS_USER}
fi

if [ -d "${HAPROXY_INSTALL_PATH}" ]; then
  sudo mkdir -p ${HAPROXY_INSTALL_PATH}
fi

if [ ! -f "${SRC_FILENAME}.tar.gz" ]; then
  wget -c http://www.haproxy.org/download/${HAPROXY_VER_MAJOR}.${HAPROXY_VER_MINOR}/src/${SRC_FILENAME}.tar.gz
fi

tar zxvf ${SRC_FILENAME}.tar.gz
cd ${SRC_FILENAME}

## make options
make_opts=(
  TARGET=linux2628
  USE_PCRE=1
  USE_OPENSSL=1
)

make ${make_opts[*]}
if [ $? -ne 0 ]; then
  echo make haproxy error!
  exit 1
fi

sudo make install PREFIX=${HAPROXY_INSTALL_PATH}
if [ $? -ne 0 ]; then
  echo Install haproxy error!
  exit 1
fi

if [ ! -f "/usr/sbin/haproxy" ]; then
  sudo ln -s ${HAPROXY_INSTALL_PATH}/sbin/haproxy /usr/sbin/haproxy
fi

cd ..
rm -rf ${SRC_FILENAME}

sudo mkdir -p ${HAPROXY_INSTALL_PATH}/conf
sudo cp -pr conf/* ${HAPROXY_INSTALL_PATH}/conf/
sudo cp -pr etc/rsyslog.d/* /etc/rsyslog.d/

## SYSLOGD_OPTIONS="-c 2 -r -m 0"
# sudo echo SYSLOGD_OPTIONS="-c 2 -r -m 0" >> /etc/sysconfig/rsyslog

sudo systemctl restart rsyslog

sudo chown -R ${HAPROXY_OS_USER}:${HAPROXY_OS_USER} ${HAPROXY_INSTALL_PATH}

## install as service
. ../tools/installService.sh haproxy

cd ${run_path}

echo Haproxy installed!
