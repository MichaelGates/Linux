## Uninstall gradle
echo Uninstall gradle...

GRADLE_INSTALL_PATH=/opt/gradle

## remove environment variable setting
sudo rm -f /etc/profile.d/gradle.sh

## remove gradle folder
if [ -n "${GRADLE_INSTALL_PATH}" ]; then
  sudo rm -rf ${GRADLE_INSTALL_PATH}
fi

echo Gradle uninstalled!
