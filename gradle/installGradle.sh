## Install gradle
## see https://gradle.org/install/
echo Install gradle...

GRADLE_INSTALL_PATH=/opt/gradle
GRADLE_DEFAULT_VERSION=4.9

read -p "Input Gradle version: (4.9) " gradleVersion

if [ -z "gradleVersion" ]; then
  gradleVersion=${GRADLE_DEFAULT_VERSION}
fi

## download binary file
wget -N https://downloads.gradle.org/distributions/gradle-${gradleVersion}-bin.zip

## unzip
sudo mkdir ${GRADLE_INSTALL_PATH}
sudo unzip -d /opt/gradle gradle-${gradleVersion}-bin.zip

## Add Gradle varable to system profile
cp -pr gradle_profile.sh gradle.sh
sed -i "s/{GRADLE_VERSION}/${gradleVersion}/g" gradle.java
sudo mv gradle.sh /etc/profile.d/gradle.sh
. /etc/profile.d/gradle.sh

echo Gradle installed!
