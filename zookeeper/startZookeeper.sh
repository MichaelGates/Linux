#!/usr/bin/env bash

SOFTWARE=zookeeper

IMAGE_NAME=zookeeper
IMAGE_TAG=3.8.0-temurin
CONTAINER_NAME=zk-1

docker run -d \
  --name ${CONTAINER_NAME} \
  --restart always \
  -e JVMFLAGS="-Xmx1024m" \
  --network zookeeper \
  -p 2181:2181 \
  -e "ZOO_MY_ID=1" \
  -e "ZOO_SERVERS=server.1=zk-1:2888:3888;2181" \
  ${IMAGE_NAME}:${IMAGE_TAG}

#  -e "ZOO_SERVERS=server.1=zk-1:2888:3888;2181 server.2=zk-2:2888:3888;2181 server.3=zk-3:2888:3888;2181" \
#  -v ~/data/${SOFTWARE}/${CONTAINER_NAME}/zoo.cfg:/conf/zoo.cfg \
#  -v ~/data/${SOFTWARE}/${CONTAINER_NAME}/myid:/data/myid \