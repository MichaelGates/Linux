#!/usr/bin/env bash

## This script is for proxy server
echo This will install squid, and shadowsocks.

## squid needed parameters
while [ -z "${DOMAIN_NAME}" ]; do
  read -p "Input DOMAIN_NAME: " DOMAIN_NAME
done
while [ -z "${squid_username}" ]; do
  read -p "Input squid username: " squid_username
done
while [ -z "${squid_password}" ]; do
  read -p "Input squid password: " squid_password
done

## shadowsocks needed parameters
read -p 'Input ss port (3389): ' ss_port
if [ -z "${ss_port}" ]; then
  ss_port=3389
fi
while [ -z "${ss_password}" ]; do
  read -p "Input ss password: " ss_password
done

## Step 1: Install squid
cd squid
./installSquid.sh -d ${DOMAIN_NAME} -u ${squid_username} -p ${squid_password}
cd ..

## Step 2: Install shadowsocks
cd shadowsocks
. ./installSSServer.sh
cd ..

echo Now checking...
echo ===== squid =====
ps -ef | grep squid
sudo netstat -tunpl | grep 8129
echo ===== shadowsocks =====
ps -ef | grep ssserver
sudo netstat -tunpl | grep 3389

echo Done!
