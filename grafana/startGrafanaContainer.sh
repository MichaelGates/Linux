#!/usr/bin/env bash
## start grafana container

CONTAINER_CONFIG_PATH=/etc/docker.d
SOFTWARE_NAME=Grafana
CONFIG_SUFFIX=.${SOFTWARE_NAME,,}.conf

Usage="Usage: $0 [CONFIG_NAME]"

if [ ! -z "$1" ]; then
  CONFIG_NAME=$1
fi

listContainerConfigFiles ${CONFIG_SUFFIX}

while [ -z "${CONFIG_NAME}" ]; do
  read -p "Input ${SOFTWARE_NAME} CONFIG_NAME: " CONFIG_NAME
done

FILE_NAME=${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}

## check file
if [ ! -f ${FILE_NAME} ]; then
  echo "The config file ${CONFIG_NAME} is not exist. Please check it."
  echo "If you do not have a config file, please initial one."
  exit 1
fi

startContainer -f ${CONFIG_NAME}${CONFIG_SUFFIX}
