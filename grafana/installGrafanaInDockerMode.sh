#!/usr/bin/env bash

SOFTWARE_NAME=Grafana
CONFIG_SUFFIX=.${SOFTWARE_NAME,,}.conf
CONTAINER_CONFIG_PATH=/etc/docker.d
CONTAINER_DATA_HOME=~/data/${SOFTWARE_NAME,,}
INSTALL_BIN_PATH=/usr/local/bin

if [ ! -d "${CONTAINER_DATA_HOME}" ]; then
  mkdir -p ${CONTAINER_DATA_HOME}
fi

echo Create links and template config file.
sudo rm ${INSTALL_BIN_PATH}/initial${SOFTWARE_NAME}ContainerConfig
sudo ln -s ${PWD}/initial${SOFTWARE_NAME}ContainerConfig.sh ${INSTALL_BIN_PATH}/initial${SOFTWARE_NAME}ContainerConfig
sudo rm ${INSTALL_BIN_PATH}/start${SOFTWARE_NAME}Container
sudo ln -s ${PWD}/start${SOFTWARE_NAME}Container.sh ${INSTALL_BIN_PATH}/start${SOFTWARE_NAME}Container
sudo cp template${CONFIG_SUFFIX} ${CONTAINER_CONFIG_PATH}/
echo Done!
