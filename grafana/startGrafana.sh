#!/usr/bin/env bash
## start grafana

SOFTWARE=grafana
IMAGE_NAME=grafana/grafana
IMAGE_TAG=9.2.4
CONTAINER_NAME=grafana

CONTAINER_DATA_BASE=~/data

docker run -d \
  --restart always \
  -p 3000:3000 \
  --name ${CONTAINER_NAME} \
  -v ${CONTAINER_DATA_BASE}/${SOFTWARE}/${CONTAINER_NAME}/data:/var/lib/grafana \
  -v ${CONTAINER_DATA_BASE}/${SOFTWARE}/${CONTAINER_NAME}/logs:/var/log/grafana \
  ${IMAGE_NAME}:${IMAGE_TAG}
