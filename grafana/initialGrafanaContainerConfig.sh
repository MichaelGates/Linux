#!/usr/bin/env bash
## Initial grafana docker config file

CONTAINER_CONFIG_PATH=/etc/docker.d
SOFTWARE_NAME=GRAFANA
CONFIG_SUFFIX=.${SOFTWARE_NAME,,}.conf
IMAGE=grafana/grafana
CONTAINER_INNER_PORT=80
RESTART_POLICY=on-failure
CONTAINER_INNER_DIRECTORY=/var/lib/grafana
DEFAULT_CONFIG_NAME=main
USER_ID=WHO_AM_I

echo Initial ${SOFTWARE_NAME} container config file ...

## config name
while [ -z "${CONFIG_NAME}" ]; do
  read -p "Input CONFIG_NAME (default:${DEFAULT_CONFIG_NAME}): " CONFIG_NAME
  if [ -z "${CONFIG_NAME}" ]; then
    CONFIG_NAME=${DEFAULT_CONFIG_NAME}
  fi
  if [ -f ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX} ]; then
    echo "${CONFIG_NAME}${CONFIG_SUFFIX} already existed."
    echo ===== ${CONFIG_NAME}${CONFIG_SUFFIX} =====
    echo ======= BEGIN ======= 
    cat ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
    echo =======  END  ======= 
    read -n1 -p "Overright? (y/N/q) " YN
    echo
    if [ "${YN}" == "q" -o "${YN}" == "Q" ]; then
      exit 0
    fi
    if [ "${YN}" != "y" -a "${YN}" != "Y" ]; then
      CONFIG_NAME=
    fi
  fi
done

## grafana parameters


initialContainerConfig -s ${SOFTWARE_NAME} \
                       -x ${CONFIG_SUFFIX} \
                       -i ${IMAGE} \
                       -f ${CONFIG_NAME} \
                       -p ${CONTAINER_INNER_PORT} \
                       -r ${RESTART_POLICY} \
                       -u ${USER_ID} \
                       -D ${CONTAINER_INNER_DIRECTORY}

NEW_CONFIG_FILE=${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
sudo bash -c "cat ${CONTAINER_CONFIG_PATH}/template${CONFIG_SUFFIX} >> ${NEW_CONFIG_FILE}"

#sudo sed -i'' -e "s/{PGADMIN_DEFAULT_EMAIL}/${PGADMIN_DEFAULT_EMAIL}/g" ${NEW_CONFIG_FILE}
#sudo sed -i'' -e "s/{PGADMIN_DEFAULT_PASSWORD}/${PGADMIN_DEFAULT_PASSWORD}/g" ${NEW_CONFIG_FILE}

echo Done!

echo ===== BEGIN ===== ${NEW_CONFIG_FILE} =====
cat ${NEW_CONFIG_FILE}
echo ===== END ===== ${NEW_CONFIG_FILE} =====

echo Generate ${SOFTWARE_NAME} container config complete!
