#!/usr/bin/env bash
## start cadvisor

SOFTWARE=cadvisor
IMAGE_NAME=gcr.io/cadvisor/cadvisor
# use the latest release version from https://github.com/google/cadvisor/releases
IMAGE_TAG=latest
#IMAGE_TAG=v0.45.0
CONTAINER_NAME=cadvisor
EXPOSE_PORT=8080

sudo docker run \
  --volume=/:/rootfs:ro \
  --volume=/var/run:/var/run:ro \
  --volume=/sys:/sys:ro \
  --volume=/var/lib/docker/:/var/lib/docker:ro \
  --volume=/dev/disk/:/dev/disk:ro \
  --publish=${EXPOSE_PORT}:8080 \
  --detach=true \
  --name=cadvisor \
  --privileged \
  --device=/dev/kmsg \
  ${IMAGE_NAME}:${IMAGE_TAG}
