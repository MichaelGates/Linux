#!/usr/bin/env bash
## start SonarQube container

CONTAINER_CONFIG_PATH=/etc/docker.d
SOFTWARE_NAME=SONARQUBE
CONFIG_SUFFIX=.${SOFTWARE_NAME,,}.conf
IMAGE_NAME=sonarqube
CONTAINER_INNER_PORT=9000
DATA_VOLUMN_PATH=~/data/${SOFTWARE_NAME,,}

Usage="Usage: $0 [CONFIG_NAME]"

if [ ! -z "$1" ]; then
  CONFIG_NAME=$1
fi

echo Exist config files:
echo ==================
ls -l ${CONTAINER_CONFIG_PATH}/*${CONFIG_SUFFIX} | awk -F'/' '{print $NF}' | awk -F"${CONFIG_SUFFIX}" '{print $1}'
echo ==================

while [ -z "${CONFIG_NAME}" ]; do
  read -p "Input ${SOFTWARE_NAME} CONFIG_NAME: " CONFIG_NAME
done

## check file
filefound=0
if [ -f ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX} ]; then
  filefound=1
  FILE_NAME=${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
fi

if [ $filefound -eq 0 ]; then
  echo "The config file ${CONFIG_NAME} is not exist. Please check it."
  echo "If you do not have a config file, please initial one."
  exit 1
fi

## import environment variable
. ${FILE_NAME}

PORTS_EXPOSE=""
if [ "${EXPOSE_PORT}" != "0" -a "${EXPOSE_PORT}" != "" ]; then
  PORTS_EXPOSE="-p ${EXPOSE_PORT}:${CONTAINER_INNER_PORT}"
fi

COMMAND=(
docker run
  --name ${CONTAINER_NAME}
  --network ${CONTAINER_NETWORK}
  --restart always
  ${PORTS_EXPOSE}
  -e sonar.jdbc.url=${SONAR_JDBC_URL}
  -e sonar.jdbc.username=${SONAR_JDBC_USERNAME}
  -e sonar.jdbc.password=${SONAR_JDBC_PASSWORD}
#  -v ${DATA_VOLUMN_PATH}/${CONTAINER_NAME}/conf:/opt/sonarqube/conf
  -v ${DATA_VOLUMN_PATH}/${CONTAINER_NAME}/extensions:/opt/sonarqube/extensions
  -v ${DATA_VOLUMN_PATH}/${CONTAINER_NAME}/logs:/opt/sonarqube/logs
  -v ${DATA_VOLUMN_PATH}/${CONTAINER_NAME}/data:/opt/sonarqube/data
  --env-file ${CONTAINER_CONFIG_PATH}/common.env
  --stop-timeout 300
  -d ${IMAGE_NAME}:${IMAGE_TAG}
)

echo ===== BEGIN =====
echo ${COMMAND[*]}
echo ===== END =====

read -n1 -p "Are you sure to start? (y/N) " YN
echo
if [ "${YN}" == "y" -o "${YN}" == "Y" ]; then
  echo -n Start ${SOFTWARE_NAME} container ${CONTAINER_NAME}...
  if [ ! -d "${DATA_VOLUMN_PATH}/${CONTAINER_NAME}" ]; then
    mkdir -p ${DATA_VOLUMN_PATH}/${CONTAINER_NAME}
  fi
  ${COMMAND[*]}
  echo Done!
else
  echo Quit!
fi
