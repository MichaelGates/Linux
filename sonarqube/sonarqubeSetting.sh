#!/bin/bash

## see https://hub.docker.com/_/sonarqube/
sudo sysctl -w vm.max_map_count=262144
sudo sysctl -w fs.file-max=65536
ulimitn=`ulimit -n`
if [ $ulimitn -lt 65536 ]; then
  ulimit -n 65536
fi

ulimitu=`ulimit -u`
if [ $ulimitu -lt 8192 ]; then
  ulimit -u 4096
fi
