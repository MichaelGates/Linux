#!/usr/bin/env bash
## Initial sonarqube docker config file

CONTAINER_CONFIG_PATH=/etc/docker.d
SOFTWARE_NAME=SONARQUBE
CONFIG_SUFFIX=.${SOFTWARE_NAME,,}.conf

DEFAULT_CONTAINER_NAME=${SOFTWARE_NAME,,}
DEFAULT_IMAGE_TAG=latest
DEFAULT_CONTAINER_NETWORK=webserver
DEFAULT_EXPOSE_PORT=9000
DEFAULT_REMOVE_CONTAINER_AFTER_STOP=Y

DEFAULT_SONAR_JDBC_URL=jdbc:postgresql://localhost/sonarqube
DEFAULT_SONAR_JDBC_USERNAME=sonarqube
DEFAULT_SONAR_JDBC_PASSWORD=password

echo Initial ${SOFTWARE_NAME} container config file ...

while [ -z "${CONFIG_NAME}" ]; do
  read -p "Input CONFIG_NAME: " CONFIG_NAME
  if [ -f ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX} ]; then
    echo "${CONFIG_NAME}${CONFIG_SUFFIX} already existed."
    echo ===== BEGIN ===== ${CONFIG_NAME}${CONFIG_SUFFIX} =====
    cat ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
    echo ===== END ===== ${CONFIG_NAME}${CONFIG_SUFFIX} =====
    read -n1 -p "Overright? (y/N/q) " YN
    echo
    if [ "${YN}" == "q" -o "${YN}" == "Q" ]; then
      exit 0
    fi
    if [ "${YN}" != "y" -a "${YN}" != "Y" ]; then
      CONFIG_NAME=
    fi
  fi
done

read -p "Input CONTAINER_NAME (default:${DEFAULT_CONTAINER_NAME}): " CONTAINER_NAME
if [ -z "${CONTAINER_NAME}" ]; then
  CONTAINER_NAME=${DEFAULT_CONTAINER_NAME}
fi

read -p "Input IMAGE_TAG (default:${DEFAULT_IMAGE_TAG}): " IMAGE_TAG
if [ -z "${IMAGE_TAG}" ]; then
  IMAGE_TAG=${DEFAULT_IMAGE_TAG}
fi

read -p "Input CONTAINER_NETWORK (default:${DEFAULT_CONTAINER_NETWORK}): " CONTAINER_NETWORK
if [ -z "${CONTAINER_NETWORK}" ]; then
  CONTAINER_NETWORK=${DEFAULT_CONTAINER_NETWORK}
fi

read -p "Input EXPOSE_PORT (0 for not expose, default:${DEFAULT_EXPOSE_PORT}): " EXPOSE_PORT
if [ -z "${EXPOSE_PORT}" ]; then
  EXPOSE_PORT=${DEFAULT_EXPOSE_PORT}
fi

read -p "Input REMOVE_CONTAINER_AFTER_STOP (default:${DEFAULT_REMOVE_CONTAINER_AFTER_STOP}): " REMOVE_CONTAINER_AFTER_STOP
if [ -z "$REMOVE_CONTAINER_AFTER_STOP" ]; then
  REMOVE_CONTAINER_AFTER_STOP=${DEFAULT_REMOVE_CONTAINER_AFTER_STOP}
fi

## 
read -p "Input SONAR_JDBC_URL (default:${DEFAULT_SONAR_JDBC_URL}): " SONAR_JDBC_URL
if [ -z "${SONAR_JDBC_URL}" ]; then
  SONAR_JDBC_URL=${DEFAULT_SONAR_JDBC_URL}
fi

read -p "Input SONAR_JDBC_USERNAME (default:${DEFAULT_SONAR_JDBC_USERNAME}): " SONAR_JDBC_USERNAME
if [ -z "${SONAR_JDBC_USERNAME}" ]; then
  SONAR_JDBC_USERNAME=${DEFAULT_SONAR_JDBC_USERNAME}
fi

read -p "Input SONAR_JDBC_PASSWORD (default:${DEFAULT_SONAR_JDBC_PASSWORD}): " SONAR_JDBC_PASSWORD
if [ -z "${SONAR_JDBC_PASSWORD}" ]; then
  SONAR_JDBC_PASSWORD=${DEFAULT_SONAR_JDBC_PASSWORD}
fi

## initial constant shell
echo -n Generate ${SOFTWARE_NAME} container config file...
if [ ! -d ${CONTAINER_CONFIG_PATH} ]; then
  sudo mkdir -p ${CONTAINER_CONFIG_PATH}
fi
NEW_CONFIG_FILE=${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
sudo cp ${CONTAINER_CONFIG_PATH}/template${CONFIG_SUFFIX} ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{SOFTWARE_NAME}/${SOFTWARE_NAME}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CREATE_TIME}/`date +'%Y-%m-%d %T'`/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CREATE_USER}/`whoami`/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CONTAINER_NAME}/${CONTAINER_NAME}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{IMAGE_TAG}/${IMAGE_TAG}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CONTAINER_NETWORK}/${CONTAINER_NETWORK}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{EXPOSE_PORT}/${EXPOSE_PORT}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{REMOVE_CONTAINER_AFTER_STOP}/${REMOVE_CONTAINER_AFTER_STOP}/g" ${NEW_CONFIG_FILE}

sudo sed -i'' -e "s|{SONAR_JDBC_URL}|${SONAR_JDBC_URL}|g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{SONAR_JDBC_USERNAME}/${SONAR_JDBC_USERNAME}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{SONAR_JDBC_PASSWORD}/${SONAR_JDBC_PASSWORD}/g" ${NEW_CONFIG_FILE}

echo Done!

echo ===== BEGIN ===== ${NEW_CONFIG_FILE} =====
cat ${NEW_CONFIG_FILE}
echo ===== END ===== ${NEW_CONFIG_FILE} =====

echo Generate ${SOFTWARE_NAME} container config complete!
