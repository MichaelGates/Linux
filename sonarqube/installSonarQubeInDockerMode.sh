#!/usr/bin/env bash
## install SonarQube

SOFTWARE_NAME=SonarQube
type installSoftwareInDockerMode

if [ $? != 0 ]; then
  "installSoftwareInDockerMode" not found!
  exit;
fi

installSoftwareInDockerMode ${SOFTWARE_NAME}

echo Setting sysctl config.
./sonarqubeSetting.sh
sudo cp etc/sysctl.d/99-sonarqube.conf /etc/sysctl.d/
sudo cp etc/security/limits.d/99-sonarqube.conf /etc/security/limits.d/

echo Done!
