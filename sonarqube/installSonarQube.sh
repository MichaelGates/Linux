
## Prerequisite JRE
echo "Prerequisite: JAVA"

## Prerequisites check
. ../tools/checkCommands.sh java 

## prepare vairable
OS_USER=`whoami`

## Fill this:
SOFTWARE_NAME=sonarqube
SOFTWARE_OS_USER=sonarqube
SOFTWARE_VER_MAJOR=7
SOFTWARE_VER_MINOR=8
SOFTWARE_VER_MAINTENANCE=
SOFTWARE_INSTALL_PATH=/home/${SOFTWARE_OS_USER}

SOFTWARE_VER=${SOFTWARE_VER_MAJOR}.${SOFTWARE_VER_MINOR}
SRC_FILENAME=${SOFTWARE_NAME}-${SOFTWARE_VER}.zip
SRC_UNZIP_FOLDER_NAME=${SOFTWARE_NAME}-${SOFTWARE_VER}
DOWNLOAD_URL=https://binaries.sonarsource.com/Distribution/sonarqube/${SRC_FILENAME}

## enter script folder
run_path=`pwd`
script_path=`cd "$(dirname $0)" && pwd`
cd ${script_path}

## Exit immediately if a command exits with a non-zero status.
set -e

echo Install ${SOFTWARE_NAME}...

## Fill this:
## Install dependencies
sudo yum install -y wget zip unzip

## Add os user
if [ ${SOFTWARE_OS_USER} ]; then
  if [ ! id ${SOFTWARE_OS_USER} ]; then
    sudo useradd --create-home --comment "Account for running ${SOFTWARE_NAME}" --shell /bin/bash ${SOFTWARE_OS_USER}
  fi
fi

if [ -d "${SOFTWARE_INSTALL_PATH}" ]; then
  sudo mkdir -p ${SOFTWARE_INSTALL_PATH}
fi

if [ ! -f "${SRC_FILENAME}" ]; then
  wget -c ${DOWNLOAD_URL}
fi

sudo unzip ${SRC_FILENAME} -d ${SOFTWARE_INSTALL_PATH}

sudo chown -R ${SOFTWARE_OS_USER}:${SOFTWARE_OS_USER} ${SOFTWARE_INSTALL_PATH} 

cd ${run_path}

echo ${SOFTWARE_NAME} installed!
