
docker run \
  -p 8086:8086 \
  -p 2003:2003 \
  -e INFLUXDB_GRAPHITE_ENABLED=true \
  --env-file influxdb.conf \
  -v ~/data/influxdb/influxdb:/var/lib/influxdb \
  --name influxdb
  -d influxdb
