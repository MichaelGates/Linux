## uninstall docker-ce in Raspbian

sudo apt-get remove docker docker-engine docker.io containerd runc

sudo apt-get purge docker-ce

sudo apt-get remove --auto-remove docker

sudo rm -rf /var/lib/docker

