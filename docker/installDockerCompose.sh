#!/usr/bin/env bash

## install docker compose

## usage
function usage() {
  Usage="Usage: $0 [options] \n"
  Usage="${Usage}  options: \n"
  Usage="${Usage}\t -h        : help \n"
  Usage="${Usage}\t -C        : in mainland China \n"
  echo -e ${Usage}
}

## parse options
while getopts 'hC' OPT; do
  case $OPT in
    h)
      HELP="TRUE";;
    C)
      isInChina="Y";;
    ?)
      usage
      exit 1
  esac
done

if [ "${HELP}" = "TRUE" -o "$1" = "--help" ]; then
  usage
  exit 0
fi

SOFTWARE_VER_MAJOR=1
SOFTWARE_VER_MINOR=29
SOFTWARE_VER_MAINTENANCE=2
SOFTWARE_VER=${SOFTWARE_VER_MAJOR}.${SOFTWARE_VER_MINOR}.${SOFTWARE_VER_MAINTENANCE}

if [ "${isInChina}" == "Y" -o "${isInChina}" == "y" ]; then
  sudo curl -L https://get.daocloud.io/docker/compose/releases/download/${SOFTWARE_VER}/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
else 
  sudo curl -L https://github.com/docker/compose/releases/download/${SOFTWARE_VER}/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
fi

sudo chmod +x /usr/local/bin/docker-compose

docker-compose --version
