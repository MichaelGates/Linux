## stop docker container
Usage=""
Usage=${Usage}" Usage:  $0 \"container-name\""

if [ -z "$1" ]; then
  echo ${Usage}
  exit 1
else
  CONTAINER_NAME=$1
fi

## stop container
if [ "$(docker ps -q -f name=${CONTAINER_NAME})" ]; then
  docker stop ${CONTAINER_NAME}
fi

## remove container
if [ "$(docker ps -aq -f status=exited -f name=${CONTAINER_NAME})" ]; then
  docker rm ${CONTAINER_NAME}
fi
