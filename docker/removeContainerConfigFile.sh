#!/bin/bash
## Display container config files.
## Support special suffix.

CONTAINER_CONFIG_PATH=/etc/docker.d

Usage=""
Usage=${Usage}" Usage:  $0 [CONFIG_FILE]"

if [ "$1" = "-h" -o "$1" = "--help" ]; then
  echo -e ${Usage}
  exit 0
fi

CONFIG_FILE=""
if [ ! -z "$1" ]; then
  CONFIG_FILE=$1
fi

set -e

if [ -z "${CONFIG_FILE}" ]; then
  listContainerConfigFiles
  read -p "Input file name: " CONFIG_FILE
fi

showContainerConfigFile ${CONFIG_FILE}

read -n1 -p "Are you sure to delete? (y/N) " YN
echo
if [ "${YN}" = "y" -o "${YN}" = "Y" ]; then
  echo -n Delete ${CONTAINER_CONFIG_PATH}/${CONFIG_FILE} ...
  sudo rm ${CONTAINER_CONFIG_PATH}/${CONFIG_FILE}
  echo
  echo Done!
else
  echo Quit!
fi
