#!/usr/bin/env bash

## Install docker
## see https://docs.docker.com/install/linux/docker-ce/centos/

## usage
function usage() {
  Usage="Usage: $0 [options] \n"
  Usage="${Usage}  options: \n"
  Usage="${Usage}\t -h        : help \n"
  Usage="${Usage}\t -C        : in mainland China \n"
  echo -e ${Usage}
}

## parse options
while getopts 'hC' OPT; do
  case $OPT in
    h)
      HELP="TRUE";;
    C)
      isInChina="Y";;
    ?)
      usage
      exit 1
  esac
done

if [ "${HELP}" = "TRUE" -o "$1" = "--help" ]; then
  usage
  exit 0
fi

## Uninstall older versions of docker
sudo yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine

## Install dependency
sudo yum install -y yum-utils device-mapper-persistent-data lvm2

## Set up the stable repository
if [ "${isInChina}" == "Y" -o "${isInChina}" == "y" ]; then
  sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
  sudo yum makecache fast
else
  sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
fi

## Enable the edge and test repositories. These repositories are included
## in the docker.repo file above but are disabled by default. You can 
## enable them alongside the stable repository.
# sudo yum-config-manager --enable docker-ce-edge
# sudo yum-config-manager --enable docker-ce-test

## You can disable the edge or test repository by running the
## yum-config-manager command with the --disable flag.
## The following command disables the edge repository.
# sudo yum-config-manager --disable docker-ce-edge
# sudo yum-config-manager --disable docker-ce-test

## Install docker-ce
echo Install Docker...
sudo yum install -y docker-ce docker-ce-cli containerd.io

## Install docker-compose
if [ -f './installDockerCompose.sh' ]; then
  ./installDockerCompose.sh
  echo Docker-compose installed!
fi
## docker group users can run docker commands without 'sudo'
# sudo groupadd docker
USER=`whoami`
sudo usermod -aG docker ${USER}

## Start with system
sudo systemctl enable docker

## Start Docker
sudo systemctl start docker

## Verify Docker
sudo docker run --rm hello-world

## Enable ipv4 forwarding
## start docker can change ip_forward to 1 automatically
ip_forward=`cat /proc/sys/net/ipv4/ip_forward`
if [ "${ip_forward}" = "0" ]; then
  echo -n Enable net.ipv4.ip_forward ... 
  sudo sh -c "echo net.ipv4.ip_forward=1 > /etc/sysctl.d/customer.conf"
  . ../tools/getOSVer.sh
  if [ "$ID" = "centos" -a "$VERSION_ID" = "7" ]; then
    sudo systemctl restart network
  elif [ "$ID" = "centos" -a "$VERSION_ID" = "8" ]; then
    sudo systemctl restart NetworkManager
  else
    sudo systemctl restart network
  fi
  echo Done!
fi

## install Docker Command
./installDockerCommand.sh

## hint
echo REMINDER!! ${USER} has added to docker group. You should re-lgoin to make it effect!

echo Docker installed!
