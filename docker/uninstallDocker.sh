## Uninstall docker
echo Uninstall Docker...

## Stop docker
sudo systemctl stop docker

## Uninstall older versions of docker
sudo yum remove -y docker-ce docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-selinux docker-engine-selinux docker-engine

## Remove docker repository
sudo rm -f /etc/yum.repos.d/docker-ce.repo

## Remove lib
sudo rm -rf /var/lib/docker

echo Done!