#!/usr/bin/env bash
## Initial docker config file

## usage
Usage="Usage: $0 [options] \n"
Usage="${Usage}  options: \n"
Usage="${Usage}\t -h        : help \n"
Usage="${Usage}\t -s string : SOFTWARE_NAME \n"
Usage="${Usage}\t -x string : CONFIG_SUFFIX \n"
Usage="${Usage}\t -i string : IMAGE \n"
Usage="${Usage}\t -t string : IMAGE_TAG \n"
Usage="${Usage}\t -f string : CONFIG_NAME \n"
Usage="${Usage}\t -c string : CONTAINER_NAME \n"
Usage="${Usage}\t -d        : RUN_AS_DAEMON \n"
Usage="${Usage}\t -n string : CONTAINER_NETWORK \n"
Usage="${Usage}\t -p string : CONTAINER_INNER_PORT \n"
Usage="${Usage}\t -P string : EXPOSE_PORT (0 for not expose) \n"
Usage="${Usage}\t -r string : RESTART_POLICY \n"
Usage="${Usage}\t -m        : REMOVE_CONTAINER_AFTER_STOP \n"
Usage="${Usage}\t -u string : USER_ID \n"
Usage="${Usage}\t -g string : GROUP_ID \n"
Usage="${Usage}\t -v        : MOUNT_DATA_VOLUME \n"
Usage="${Usage}\t -V string : HOST_VOLUME \n"
Usage="${Usage}\t -D string : CONTAINER_INNER_DIRECTORY \n"
Usage="${Usage}\t -C string : CUSTOMER_PARAMS \n"
Usage="${Usage}\t -e string : EXEC_COMMAND \n"
Usage="${Usage}\t -E        : CALL_FROM_EXTERNAL \n"

## TODO: demo

## parse options
while getopts 'hs:x:i:t:f:c:dn:p:P:r:mu:g:vV:D:C:e:E' OPT; do
  case $OPT in
    h)
      HELP="TRUE";;
    s)
      SOFTWARE_NAME="$OPTARG";;
    x)
      CONFIG_SUFFIX="$OPTARG";;
    i)
      IMAGE="$OPTARG";;
    t)
      IMAGE_TAG="$OPTARG";;
    f)
      CONFIG_NAME="$OPTARG";;
    c)
      CONTAINER_NAME="$OPTARG";;
    d)
      RUN_AS_DAEMON="Y";;
    n)
      CONTAINER_NETWORK="$OPTARG";;
    p)
      CONTAINER_INNER_PORT="$OPTARG";;
    P)
      EXPOSE_PORT="$OPTARG";;
    r)
      RESTART_POLICY="$OPTARG";;
    m)
      REMOVE_CONTAINER_AFTER_STOP="Y";;
    u)
      USER_ID="$OPTARG";;
    g)
      GROUP_ID="$OPTARG";;
    v)
      MOUNT_DATA_VOLUME="Y";;
    V)
      HOST_VOLUME="$OPTARG";;
    D)
      CONTAINER_INNER_DIRECTORY="$OPTARG";;
    C)
      CUSTOMER_PARAMS="$OPTARG";;
    e)
      EXEC_COMMAND="$OPTARG";;
    E)
      CALL_FROM_EXTERNAL="Y";;
    ?)
      echo -e "${Usage}"
      exit 1
  esac
done

if [ "${HELP}" = "TRUE" -o "$1" = "--help" ]; then
  echo -e ${Usage}
  exit 0
fi

CONTAINER_CONFIG_PATH=/etc/docker.d

DEFAULT_IMAGE_TAG=latest
DEFAULT_CONFIG_NAME=main
DEFAULT_RUN_AS_DAEMON=Y
DEFAULT_CONTAINER_NETWORK=default
DEFAULT_MOUNT_DATA_VOLUME=N
DEFAULT_VOLUME_BASE=~/data
DEFAULT_RESTART_POLICY=on-failure
DEFAULT_REMOVE_CONTAINER_AFTER_STOP=N
DEFAULT_USER_ID=
DEFAULT_GROUP_ID=

#echo Initial container config file ...

## software name
while [ -z "${SOFTWARE_NAME}" ]; do
  read -p "Input SOFTWARE_NAME: " SOFTWARE_NAME
done

## config suffix
while [ -z "${CONFIG_SUFFIX}" ]; do
  read -p "Input CONFIG_SUFFIX (default:.${SOFTWARE_NAME,,}.conf): " CONFIG_SUFFIX
  if [ -z "${CONFIG_SUFFIX}" ]; then
    CONFIG_SUFFIX=.${SOFTWARE_NAME,,}.conf
  fi
done

## config name
while [ -z "${CONFIG_NAME}" ]; do
  read -p "Input CONFIG_NAME (default:${DEFAULT_CONFIG_NAME}): " CONFIG_NAME
  if [ -z "${CONFIG_NAME}" ]; then
    CONFIG_NAME=${DEFAULT_CONFIG_NAME}
  fi
  if [ -f ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX} ]; then
    echo "${CONFIG_NAME}${CONFIG_SUFFIX} already existed."
    echo ===== ${CONFIG_NAME}${CONFIG_SUFFIX} =====
    echo ======= BEGIN ======= 
    cat ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
    echo =======  END  ======= 
    read -n1 -p "Overright? (y/N/q) " YN
    echo
    if [ "${YN}" == "q" -o "${YN}" == "Q" ]; then
      exit 0
    fi
    if [ "${YN}" != "y" -a "${YN}" != "Y" ]; then
      CONFIG_NAME=
    fi
  fi
done

## docker image
while [ -z "${IMAGE}" ]; do
  read -p "Input IMAGE: " IMAGE
done

## image tag
while [ -z "${IMAGE_TAG}" ]; do
  read -p "Input IMAGE_TAG (default:${DEFAULT_IMAGE_TAG}): " IMAGE_TAG
  if [ -z "${IMAGE_TAG}" ]; then
    IMAGE_TAG=${DEFAULT_IMAGE_TAG}
  fi
done

## container name
while [ -z "${CONTAINER_NAME}" ]; do
  read -p "Input CONTAINER_NAME (default:${SOFTWARE_NAME,,}): " CONTAINER_NAME
  if [ -z "${CONTAINER_NAME}" ]; then
    CONTAINER_NAME=${SOFTWARE_NAME,,}
  fi
done

## run as daemon
while [ -z "${RUN_AS_DAEMON}" ]; do
  read -p "Input RUN_AS_DAEMON (default:${DEFAULT_RUN_AS_DAEMON}): " RUN_AS_DAEMON
  if [ -z "${RUN_AS_DAEMON}" ]; then
    RUN_AS_DAEMON=${DEFAULT_RUN_AS_DAEMON}
  fi
done

## container restart policy
while [ -z "${RESTART_POLICY}" ]; do
  read -p "Input RESTART_POLICY (no/on-failure/always/unless-stopped, default:${DEFAULT_RESTART_POLICY}): " RESTART_POLICY
  if [ -z "${RESTART_POLICY}" ]; then
    RESTART_POLICY=${DEFAULT_RESTART_POLICY}
  fi
 
  ## auto restart container can not be removed after stop
  if [ "${RESTART_POLICY}" != "no" -a "${RESTART_POLICY}" != "on-failure" -a "${RESTART_POLICY}" != "always" -a "${RESTART_POLICY}" != "unless-stopped" ]; then
    RESTART_POLICY=""
  fi
done

## auto restart container can not be removed after stop
if [ "${RESTART_POLICY}" = "on-failure" -o "${RESTART_POLICY}" = "always" -o "${RESTART_POLICY}" = "unless-stopped" ]; then
  REMOVE_CONTAINER_AFTER_STOP="N"
fi

## whether remove container atfer stop
while [ -z "${REMOVE_CONTAINER_AFTER_STOP}" ]; do
  read -n1 -p "Input REMOVE_CONTAINER_AFTER_STOP (y/N): " REMOVE_CONTAINER_AFTER_STOP
  if [ -z "${REMOVE_CONTAINER_AFTER_STOP}" ]; then
    REMOVE_CONTAINER_AFTER_STOP=${DEFAULT_REMOVE_CONTAINER_AFTER_STOP}
  fi
done

## container network
while [ -z "${CONTAINER_NETWORK}" ]; do
  read -p "Input CONTAINER_NETWORK (default:${DEFAULT_CONTAINER_NETWORK}): " CONTAINER_NETWORK
  if [ -z "${CONTAINER_NETWORK}" ]; then
    CONTAINER_NETWORK=${DEFAULT_CONTAINER_NETWORK}
  fi
done

## expose port
while [ -z "${EXPOSE_PORT}" ]; do
  read -p "Input EXPOSE_PORT (0 for not expose): " EXPOSE_PORT
done

## container inner port
if [ "${EXPOSE_PORT}" != "0" -a -z "${CONTAINER_INNER_PORT}" ]; then
  read -p "Input CONTAINER_INNER_PORT (default:${EXPOSE_PORT}): " CONTAINER_INNER_PORT
  if [ -z "${CONTAINER_INNER_PORT}" ]; then
    CONTAINER_INNER_PORT=${EXPOSE_PORT}
  fi
fi

## mount volume
while [ -z "${MOUNT_DATA_VOLUME}" ]; do
  read -n1 -p "Input MOUNT_DATA_VOLUME (y/N): " MOUNT_DATA_VOLUME
  echo 
  if [ -z "${MOUNT_DATA_VOLUME}" ]; then
    MOUNT_DATA_VOLUME=${DEFAULT_MOUNT_DATA_VOLUME}
  fi
done

## host volume
if [ "${MOUNT_DATA_VOLUME}" = "y" -o  "${MOUNT_DATA_VOLUME}" = "Y" ]; then
  while [ -z "${HOST_VOLUME}" ]; do
    read -p "Input HOST_VOLUME (default:${DEFAULT_VOLUME_BASE}/${SOFTWARE_NAME,,}/${CONTAINER_NAME}): " HOST_VOLUME
    if [ -z "${HOST_VOLUME}" ]; then
      HOST_VOLUME=${DEFAULT_VOLUME_BASE}/${SOFTWARE_NAME,,}/${CONTAINER_NAME}
    fi
  done
fi

## container inner folder
if [ "${MOUNT_DATA_VOLUME}" = "y" -o  "${MOUNT_DATA_VOLUME}" = "Y" ]; then
  while [ -z "${CONTAINER_INNER_DIRECTORY}" ]; do
    read -p "Input CONTAINER_INNER_DIRECTORY: " CONTAINER_INNER_DIRECTORY
  done
fi

## container runner user id
if [ -z "${USER_ID}" ]; then
  read -p "Input USER_ID for run container (WHO_AM_I / USER_ID / USER_NAME / NOT_SPECIFY, default: NOT_SPECIFY): " USER_ID
  if [ -z "${USER_ID}" ]; then
    USER_ID=${DEFAULT_USER_ID}
  fi
fi

## container runner group id
if [ -z "${GROUP_ID}" ]; then
  read -p "Input GROUP_ID for run container (MY_MAIN_GROUP / GROUP_ID / GROUP_NAME / NOT_SPECIFY, default: NOT_SPECIFY): " GROUP_ID
  if [ -z "${GROUP_ID}" ]; then
    GROUP_ID=${DEFAULT_GROUP_ID}
  fi
fi

## customer parameters
if [ "${CALL_FROM_EXTERNAL}" != "Y" -a -z "${CUSTOMER_PARAMS}" ]; then
  read -p "Input CUSTOMER_PARAMS (if applicable): " CUSTOMER_PARAMS
fi

## executer command
if [ "${CALL_FROM_EXTERNAL}" != "Y" -a -z "${EXEC_COMMAND}" ]; then
  read -p "Input EXEC_COMMAND (if applicable): " EXEC_COMMAND
fi

## initial constant shell
echo -n Generate ${SOFTWARE_NAME} container config file...
if [ ! -d ${CONTAINER_CONFIG_PATH} ]; then
  sudo mkdir -p ${CONTAINER_CONFIG_PATH}
fi
NEW_CONFIG_FILE=${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
sudo cp ${CONTAINER_CONFIG_PATH}/template.docker.conf ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{SOFTWARE_NAME}/${SOFTWARE_NAME}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CREATE_TIME}/`date +'%Y-%m-%d %T'`/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CREATE_USER}/`whoami`/g" ${NEW_CONFIG_FILE}

sudo sed -i'' -e "s|{IMAGE}|${IMAGE}|g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{IMAGE_TAG}/${IMAGE_TAG}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CONTAINER_NAME}/${CONTAINER_NAME}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{RUN_AS_DAEMON}/${RUN_AS_DAEMON}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{RESTART_POLICY}/${RESTART_POLICY}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{REMOVE_CONTAINER_AFTER_STOP}/${REMOVE_CONTAINER_AFTER_STOP}/g" ${NEW_CONFIG_FILE}

sudo sed -i'' -e "s/{CONTAINER_NETWORK}/${CONTAINER_NETWORK}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CONTAINER_INNER_PORT}/${CONTAINER_INNER_PORT}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{EXPOSE_PORT}/${EXPOSE_PORT}/g" ${NEW_CONFIG_FILE}

sudo sed -i'' -e "s/{USER_ID}/${USER_ID}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{GROUP_ID}/${GROUP_ID}/g" ${NEW_CONFIG_FILE}

sudo sed -i'' -e "s/{MOUNT_DATA_VOLUME}/${MOUNT_DATA_VOLUME}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s|{HOST_VOLUME}|${HOST_VOLUME}|g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s|{CONTAINER_INNER_DIRECTORY}|${CONTAINER_INNER_DIRECTORY}|g" ${NEW_CONFIG_FILE}

sudo sed -i'' -e "s/{CUSTOMER_PARAMS}/${CUSTOMER_PARAMS}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{EXEC_COMMAND}/${EXEC_COMMAND}/g" ${NEW_CONFIG_FILE}

echo Done!

if [ ${CALL_FROM_EXTERNAL} != "Y" ]; then
  echo ===== ${NEW_CONFIG_FILE} =====
  echo ======= BEGIN =======
  cat ${NEW_CONFIG_FILE}
  echo =======  END  =======
fi

echo Generate ${SOFTWARE_NAME} container config complete!
