#!/usr/bin/env bash

## Check whether the docker container running

Usage=""
Usage=${Usage}" Usage:  $0 \"docker container name\""

if [ -z "$1" ]; then
  echo ${Usage}
  exit 1
else
  CONTAINER_NAME=$1
fi

if [ -z $(docker ps -q -f "name=^/${CONTAINER_NAME}$") ]; then
  echo Docker container "${CONTAINER_NAME}" not found.
  exit 1
fi
echo Docker container "${CONTAINER_NAME}" found.
