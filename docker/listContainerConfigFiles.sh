#!/bin/bash
## Display container config files.
## Support special suffix.

CONTAINER_CONFIG_PATH=/etc/docker.d

Usage=""
Usage=${Usage}" Usage:  $0 [CONFIG_SUFFIX]"

if [ "$1" = "-h" -o "$1" = "--help" ]; then
  echo -e ${Usage}
  exit 0
fi

if [ ! -z "$1" ]; then
  CONFIG_SUFFIX=$1
fi

echo Exist ${CONFIG_SUFFIX} config files:
echo ==================
if [ ! -z "$CONFIG_SUFFIX" ]; then
  ls -l ${CONTAINER_CONFIG_PATH}/*${CONFIG_SUFFIX} | grep -v template | awk -F'/' '{print $NF}' | awk -F"${CONFIG_SUFFIX}" '{print $1}'
else
  ls -l ${CONTAINER_CONFIG_PATH}/*${CONFIG_SUFFIX} | grep -v template | awk -F'/' '{print $NF}'
fi
echo ==================
