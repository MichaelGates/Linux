## install Docker in Raspbian

## For Raspbian, installing using the repository is not yet supported.
## You must instead use the convenience script.
## see https://docs.docker.com/install/linux/docker-ce/debian/

## get the os user name
os_user=`whoami`

sudo curl -sSL https://get.docker.com | sh
sudo usermod -aG docker ${os_user}
