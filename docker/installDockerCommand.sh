#!/usr/bin/env bash

## Create docker container config directory
CONTAINER_CONFIG_PATH=/etc/docker.d
sudo mkdir -p ${CONTAINER_CONFIG_PATH}
sudo cp common.env ${CONTAINER_CONFIG_PATH}/
sudo cp template.docker.conf ${CONTAINER_CONFIG_PATH}/

if [ -f /usr/local/bin/installSoftwareInDockerMode ]; then
  sudo rm /usr/local/bin/installSoftwareInDockerMode
fi
sudo ln -s ${PWD}/installSoftwareInDockerMode.sh /usr/local/bin/installSoftwareInDockerMode

if [ -f /usr/local/bin/initialContainerConfig ]; then
  sudo rm /usr/local/bin/initialContainerConfig
fi
sudo ln -s ${PWD}/initialContainerConfig.sh /usr/local/bin/initialContainerConfig

if [ -f /usr/local/bin/startContainer ]; then
  sudo rm /usr/local/bin/startContainer
fi
sudo ln -s ${PWD}/startContainer.sh /usr/local/bin/startContainer

if [ -f /usr/local/bin/listContainerConfigFiles ]; then
  sudo rm /usr/local/bin/listContainerConfigFiles
fi
sudo ln -s ${PWD}/listContainerConfigFiles.sh /usr/local/bin/listContainerConfigFiles

if [ -f /usr/local/bin/showContainerConfigFile ]; then
  sudo rm /usr/local/bin/showContainerConfigFile
fi
sudo ln -s ${PWD}/showContainerConfigFile.sh /usr/local/bin/showContainerConfigFile

if [ -f /usr/local/bin/removeContainerConfigFile ]; then
  sudo rm /usr/local/bin/removeContainerConfigFile
fi
sudo ln -s ${PWD}/removeContainerConfigFile.sh /usr/local/bin/removeContainerConfigFile
