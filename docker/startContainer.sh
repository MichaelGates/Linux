#!/usr/bin/env bash
## start software container

CONTAINER_CONFIG_PATH=/etc/docker.d
CUSTOMER_PARAM=

Usage=" Usage: $0 [-h] [-f CONFIG_FILE_NAME] [-p CUSTOMER_PARAM] [-c COMMAND]"

## parse options
while getopts 'hf:c:p:' OPT; do
  case $OPT in
    h)
      HELP="TRUE";;
    f)
      CONFIG_FILE_NAME="$OPTARG";;
    c)
      EXEC_COMMAND="$OPTARG";;
    p)
      CUSTOMER_PARAM="$OPTARG";;
    ?)
      echo -e "${Usage}"
      exit 1
  esac
done

if [ "${HELP}" = "TRUE" -o "$1" = "--help" ]; then
  echo -e ${Usage}
  exit 0
fi

while [ -z "${CONFIG_FILE_NAME}" ]; do
  listContainerConfigFiles
  read -p "Input CONFIG_FILE_NAME: " CONFIG_FILE_NAME
done

## check config file exist
if [ ! -f ${CONTAINER_CONFIG_PATH}/${CONFIG_FILE_NAME} ]; then
  echo "The config file ${CONFIG_FILE_NAME} is not exist. Please check it."
  echo "If you do not have a config file, please create one."
  read -n1 -p "Input again? (Y/n) :" YN
  echo
  if [ "${YN}" = "n" -o "${YN}" = "N" ]; then
    echo Bye!
    exit 0
  fi
fi

FILE_NAME=${CONTAINER_CONFIG_PATH}/${CONFIG_FILE_NAME}

## import environment variable
. ${FILE_NAME}

PORTS_EXPOSE=""
if [ "${CONTAINER_INNER_PORT}" != "" -a "${EXPOSE_PORT}" != "" -a "${EXPOSE_PORT}" != "0" ]; then
  PORTS_EXPOSE="-p ${EXPOSE_PORT}:${CONTAINER_INNER_PORT}"
fi

REMOVE_FLAG_PARAM=""
if [ "${REMOVE_CONTAINER_AFTER_STOP}" = "y" -o "${REMOVE_CONTAINER_AFTER_STOP}" = "Y" ]; then
  REMOVE_FLAG_PARAM="--rm"
fi

VOLUMN_PARAM=""
if [ "${MOUNT_DATA_VOLUMN}" = "y" -o "${MOUNT_DATA_VOLUMN}" = "Y" ]; then
  VOLUMN_PARAM="-v ${HOST_VOLUMN}:${CONTAINER_INNER_DIRECTORY}"
fi

USER_PARAM=""
if [ "${USER_ID}" = "WHO_AM_I" ]; then
  USER_PARAM="--user $(id -u)"
else
  if [ "${USER_ID}" != "NOT_SPECIFY" -a "${USER_ID}" != "" ]; then
    USER_PARAM="--user ${USER_ID}"
  fi
fi

GROUP_PARAM=""
if [ "${GROUP_ID}" = "MY_MAIN_GROUP" ]; then
  GROUP_PARAM=":$(id -g)"
else
  if [ "${GROUP_ID}" != "NOT_SPECIFY" -a "${GROUP_ID}" != "" ]; then
    GROUP_PARAM=":${GROUP_ID}"
  fi
fi

COMMAND=(
docker run ${REMOVE_FLAG_PARAM}
  --name ${CONTAINER_NAME}
  --network ${CONTAINER_NETWORK}
  --restart ${RESTART_POLICY}
  ${PORTS_EXPOSE}
  ${USER_PARAM}${GROUP_PARAM}
  --env-file ${CONTAINER_CONFIG_PATH}/common.env
  ${VOLUMN_PARAM}
  ${CUSTOMER_PARAM}
  -d ${IMAGE}:${IMAGE_TAG}
  ${EXEC_COMMAND}
)

echo ===== BEGIN =====
echo ${COMMAND[*]}
echo ===== END =====

read -n1 -p "Are you sure to start? (y/N) " YN
echo
if [ "${YN}" == "y" -o "${YN}" == "Y" ]; then
  echo -n Start container ${CONTAINER_NAME}...
  echo 
  ${COMMAND[*]}
  echo Done!
else
  echo Quit!
fi
