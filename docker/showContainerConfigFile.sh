#!/bin/bash
## Display container config file content.

CONTAINER_CONFIG_PATH=/etc/docker.d

Usage=""
Usage=${Usage}" Usage:  $0 [CONFIG_FILE]"

if [ "$1" = "-h" -o "$1" = "--help" ]; then
  echo -e ${Usage}
  exit 0
fi

CONFIG_FILE=""
if [ ! -z "$1" ]; then
  CONFIG_FILE=$1
fi

if [ -z "${CONFIG_FILE}" ]; then
  listContainerConfigFiles
  read -p "Input file name: " CONFIG_FILE
fi

if [ ! -f "${CONTAINER_CONFIG_PATH}/${CONFIG_FILE}" ]; then 
  echo Config file not found!
  exit 1
fi

echo === ${CONTAINER_CONFIG_PATH}/${CONFIG_FILE} ===
echo ===== BEGIN =====
cat ${CONTAINER_CONFIG_PATH}/${CONFIG_FILE}
echo =====  ENG  =====
