#!/usr/bin/env bash

Usage=""
Usage=${Usage}" Usage:  $0 [SOFTWARE_NAME]"

if [ "$1" = "-h" -o "$1" = "--help" ]; then
  echo -e ${Usage}
  exit 0
fi

# get parameter SOFTWARE_NAME
if [ ! -z "$1" ]; then
  SOFTWARE_NAME=$1
fi

# input SOFTWARE_NAME
while [ -z "${SOFTWARE_NAME}" ]; do
  read -p "Input SOFTWARE_NAME: " SOFTWARE_NAME
done

# confirm operation
read -n1 -p "Are you sure to install ${SOFTWARE_NAME}? (y/N) " YN
echo
if [ "${YN}" != "y" -a "${YN}" != "Y" ]; then
  echo Quit!
  exit 0
fi

#SOFTWARE_NAME={SOFTWARE_NAME}
CONFIG_SUFFIX=.${SOFTWARE_NAME,,}.conf
CONTAINER_CONFIG_PATH=/etc/docker.d
CONTAINER_DATA_HOME=~/data/${SOFTWARE_NAME,,}
INSTALL_BIN_PATH=/usr/local/bin

if [ ! -d "${CONTAINER_DATA_HOME}" ]; then
  mkdir -p ${CONTAINER_DATA_HOME}
fi

echo Create links and template config file.
if [ -f ${INSTALL_BIN_PATH}/initial${SOFTWARE_NAME}ContainerConfig ]; then
  sudo rm ${INSTALL_BIN_PATH}/initial${SOFTWARE_NAME}ContainerConfig
fi
sudo ln -s ${PWD}/initial${SOFTWARE_NAME}ContainerConfig.sh ${INSTALL_BIN_PATH}/initial${SOFTWARE_NAME}ContainerConfig

if [ -f ${INSTALL_BIN_PATH}/start${SOFTWARE_NAME}Container ]; then
  sudo rm ${INSTALL_BIN_PATH}/start${SOFTWARE_NAME}Container
fi
sudo ln -s ${PWD}/start${SOFTWARE_NAME}Container.sh ${INSTALL_BIN_PATH}/start${SOFTWARE_NAME}Container

if [ -f ${CONTAINER_CONFIG_PATH}/template${CONFIG_SUFFIX} ]; then
  sudo rm ${CONTAINER_CONFIG_PATH}/template${CONFIG_SUFFIX}
fi
sudo cp template${CONFIG_SUFFIX} ${CONTAINER_CONFIG_PATH}/

echo Done!