## Diagnose ddns service

echo Cache file: /var/cache/ddclient/ddclient.cache
sudo cat /var/cache/ddclient/ddclient.cache
echo

echo PID file: /var/run/ddclient/ddclient.pid
sudo cat /var/run/ddclient/ddclient.pid
echo

echo ddclient related log FOR CentOS
sudo grep ddclient /var/log/messages

echo ddclient related log FOR Raspberry PI
sudo grep ddclient /var/log/syslog
