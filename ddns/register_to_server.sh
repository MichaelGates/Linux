## re-register ip to ddns server

## stop ddclient service
sudo systemctl stop ddclient

## remove cache file
sudo rm /var/cache/ddclient/ddclient.cache

## start ddclient service
sudo systemctl start ddclient

## show status
echo PID file: /var/run/ddclient/ddclient.pid
sudo cat /var/run/ddclient/ddclient.pid
echo

echo ddclient related log
sudo grep ddclient /var/log/messages
