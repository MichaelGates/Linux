#!/usr/bin/env bash
## Install ddns service

## usage
function usage() {
  Usage="Usage: $0 [options] \n"
  Usage="${Usage}  options: \n"
  Usage="${Usage}\t -h        : help \n"
  Usage="${Usage}\t -s string : DDNS_SERVER \n"
  Usage="${Usage}\t -d string : DDNS_DOMAIN \n"
  Usage="${Usage}\t -u string : DDNS_USERNAME \n"
  Usage="${Usage}\t -p string : DDNS_PASSWORD \n"
  echo -e ${Usage}
}

## parse options
while getopts 'hs:d:u:p:' OPT; do
  case $OPT in
    h)
      HELP="TRUE";;
    s)
      DDNS_SERVER="$OPTARG";;
    d)
      DDNS_DOMAIN="$OPTARG";;
    u)
      DDNS_USERNAME="$OPTARG";;
    p)
      DDNS_PASSWORD="$OPTARG";;
    ?)
      usage
      exit 1
  esac
done

if [ "${HELP}" = "TRUE" -o "$1" = "--help" ]; then
  usage
  exit 0
fi

## input parameter if not set
function inputParam() {
  while [ -z "${DDNS_SERVER}" ]; do
    read -p "Input ddns server'(e.g. now-dns.com)':" DDNS_SERVER
  done

  while [ -z "${DDNS_USERNAME}" ]; do
    read -p "Input ddns username:" DDNS_USERNAME
  done

  while [ -z "${DDNS_PASSWORD}" ]; do
    read -s -p "Input ddns password:" DDNS_PASSWORD
    echo
  done

  while [ -z "${DDNS_DOMAIN}" ]; do
    read -p "Input ddns domain:" DDNS_DOMAIN
  done
}
inputParam

## Remove ddclient if existed
yum list ddclient > /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo Remove DDNS...
  sudo yum remove -y ddclient
  echo Done.
fi

## Install epel repositories
sudo yum -y install epel-release

## Install ddns with yum

OS_VERSION=`osinfo.sh`
if [ "${OS_VERSION}" = "centos 8" -o "${OS_VERSION}"="rocky 8" ]; then
  echo This is CentOS 8 Server.
  sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
  echo Enable PowerTools
  ## sometimes the repo id is PowerTools, and sometimes it is powertools
  if [ -n "$(dnf repolist all | awk '{ print $1 }' | grep PowerTools)" ]; then
    sudo dnf config-manager --set-enabled PowerTools
  fi
  if [ -n "$(dnf repolist all | awk '{ print $1 }' | grep powertools)" ]; then
    sudo dnf config-manager --set-enabled powertools
  fi
fi

## set errexit
set -e

echo Install DDNS...
sudo yum install -y ddclient
sudo cp -pr /etc/ddclient.conf /etc/ddclient.conf.default
echo Done

## Config ddclient
cp -pr conf/ddclient_append.conf conf/ddclient.conf
sed -i "s/{ddns_server}/${DDNS_SERVER}/g" conf/ddclient.conf
sed -i "s/{ddns_domain}/${DDNS_DOMAIN}/g" conf/ddclient.conf
sed -i "s/{username}/${DDNS_USERNAME}/g" conf/ddclient.conf
sed -i "s/{password}/${DDNS_PASSWORD}/g" conf/ddclient.conf
sudo sh -c "cat /etc/ddclient.conf.default conf/ddclient.conf > /etc/ddclient.conf"
rm conf/ddclient.conf

## Add DDNS_DOMAIN to system profile
sudo cp ddns_profile.sh ddns.sh
sed -i "s/{DDNS_DOMAIN}/${DDNS_DOMAIN}/g" ddns.sh
sudo mv ddns.sh /etc/profile.d/ddns.sh
. /etc/profile.d/ddns.sh

## Start ddclient when system start
sudo chkconfig ddclient on
sudo systemctl start ddclient
systemctl status ddclient -l --no-pager

echo DDNS installed!
