## Prerequisite JRE
echo "Prerequisite: JRE"

## Prerequisites check
. ../tools/checkCommands.sh java

CONFLUENCE_VER=6.13.0
OS_CONFLUENCE_HOME=/home/confluence
CONFLUENCE_INSTALL_PATH=${OS_CONFLUENCE_HOME}/atlassian-confluence
CONFLUENCE_HOME=${OS_CONFLUENCE_HOME}/confluence-home

read -p "Input CONFLUENCE DOMAIN_NAME (localhost):" DOMAIN_NAME

if [ -z "$DOMAIN_NAME" ]; then
  DOMAIN_NAME=localhost
fi

read -p "Input CONFLUENCE_PORT (8090 as default):" CONFLUENCE_PORT

if [ -z "$CONFLUENCE_PORT" ]; then
  CONFLUENCE_PORT=8090
fi

sudo yum install -y wget

## Download confluence install file if not exist
if [ ! -f "atlassian-confluence-${CONFLUENCE_VER}.tar.gz" ]; then 
  wget https://www.atlassian.com/software/confluence/downloads/binary/atlassian-confluence-${CONFLUENCE_VER}.tar.gz
fi

sudo useradd --create-home --comment "Account for running Confluence" --shell /bin/bash confluence
sudo tar -xzvf atlassian-confluence-${CONFLUENCE_VER}.tar.gz -C ${OS_CONFLUENCE_HOME}

sudo mv ${OS_CONFLUENCE_HOME}/atlassian-confluence-${CONFLUENCE_VER} ${OS_CONFLUENCE_HOME}/atlassian-confluence
sudo mkdir -p ${CONFLUENCE_HOME}

## Change owner of ${CONFLUENCE_INSTALL_PATH} and ${CONFLUENCE_HOME}
sudo chown -R confluence:confluence ${CONFLUENCE_INSTALL_PATH}
sudo chown -R confluence:confluence ${CONFLUENCE_HOME}

## Backup default properties file
sudo cp -pr ${CONFLUENCE_INSTALL_PATH}/confluence/WEB-INF/classes/confluence-init.properties ${CONFLUENCE_INSTALL_PATH}/confluence/WEB-INF/classes/confluence-init.properties.default

line=`sudo grep "confluence.home=${CONFLUENCE_HOME}" ${CONFLUENCE_INSTALL_PATH}/confluence/WEB-INF/classes/confluence-init.properties`
if [ -z "$line" ]; then
  ## Append CONFLUENCE_HOME config to properties file
  echo Config confluence-init.properties
  sudo su - confluence -c "echo -e \"\nconfluence.home=${CONFLUENCE_HOME}\" >> ${CONFLUENCE_INSTALL_PATH}/confluence/WEB-INF/classes/confluence-init.properties"
  ## Overwrite CONFLUENCE_HOME properties file
  # sudo echo confluence.home=${CONFLUENCE_HOME} > ${CONFLUENCE_INSTALL_PATH}/confluence/WEB-INF/classes/confluence-init.properties
fi

sudo cp -pr conf/server.xml ${CONFLUENCE_INSTALL_PATH}/conf/server.xml
if [ -n "$DOMAIN_NAME" ]; then
  echo Config server.xml
  sudo sed -i "s/{CONFLUENCE_PORT}/${CONFLUENCE_PORT}/g" ${CONFLUENCE_INSTALL_PATH}/conf/server.xml
  sudo sed -i "s/{DOMAIN_NAME}/${DOMAIN_NAME}/g" ${CONFLUENCE_INSTALL_PATH}/conf/server.xml
fi

## Change owner of ${CONFLUENCE_INSTALL_PATH} and ${CONFLUENCE_HOME}
sudo chown -R confluence:confluence ${CONFLUENCE_INSTALL_PATH}
sudo chown -R confluence:confluence ${CONFLUENCE_HOME}
## Remove other's rights of ${CONFLUENCE_INSTALL_PATH} and ${CONFLUENCE_HOME}
sudo chmod -R u=rwx,go-rwx ${CONFLUENCE_INSTALL_PATH}
sudo chmod -R u=rwx,go-rwx ${CONFLUENCE_HOME}

## Add nginx conf
../nginx/addNginxConf.sh conf/confluence.https.conf

## Install confluence as a system service
../tools/installService.sh confluence

## Check confluence every 5 minute. If confluence is down, then start it.
cp -pr checkAndStartConfluence.sh ~/cron/
../tools/addCrontab.sh "*/5 * * * * sh cron/checkAndStartConfluence.sh"

echo Starting confluence...
sudo systemctl start confluence

echo Finished!
