#!/usr/bin/env bash
## Start Concluence container

CONTAINER_CONFIG_PATH=/etc/docker.d
SOFTWARE_NAME=Confluence
CONFIG_SUFFIX=.${SOFTWARE_NAME,,}.conf
DATA_STORE_PATH=~/data/${SOFTWARE_NAME,,}
JDBC_PATH=~/data/jdbc
WINDOWS_FONTS_PATH=/usr/share/fonts/windowsfonts

Usage="Usage: $0 [CONFIG_FILE_NAME]"

if [ ! -z "$1" ]; then
  CONFIG_FILE_NAME=$1
fi

listContainerConfigFiles ${CONFIG_SUFFIX}

while [ -z "${CONFIG_FILE_NAME}" ]; do
  read -p "Input CONFLUENCE CONFIG_FILE_NAME: " CONFIG_FILE_NAME
done

## check file
filefound=0
if [ -f ${CONTAINER_CONFIG_PATH}/${CONFIG_FILE_NAME}${CONFIG_SUFFIX} ]; then
  filefound=1
  FILE_NAME=${CONTAINER_CONFIG_PATH}/${CONFIG_FILE_NAME}${CONFIG_SUFFIX}
fi

if [ $filefound -eq 0 ]; then
  echo "The config file ${CONFIG_FILE_NAME} is not exist. Please check it."
  echo "If you do not have a config file, please initial one."
  exit 1
fi

## import environment variable
if [ $filefound -eq 1 ]; then
  echo ===== BEGIN ===== ${FILE_NAME} =====
  cat ${FILE_NAME}
  echo ===== END ===== ${FILE_NAME} =====
  read -n1 -p "Are you sure to start? (y/N) " YN
  echo
  if [ "${YN}" == "y" -o "${YN}" == "Y" ]; then
    echo -n Importing confluence environment variable...
    . ${FILE_NAME}
    echo Done!
  else
    echo Quit!
    exit 0
  fi
fi

PORTS_EXPOSE=""
if [ "${CONFLUENCE_EXPOSE_PORT}" != "0" ]; then
  PORTS_EXPOSE="-p ${CONFLUENCE_EXPOSE_PORT}:8090"
fi

HTTPS_CONFIG=""
if [ "${CONFLUENCE_HTTPS_ENABLE}" == "y" -o "${CONFLUENCE_HTTPS_ENABLE}" == "Y" ]; then
  HTTPS_CONFIG="${HTTPS_CONFIG} -e ATL_PROXY_PORT=443 "
  HTTPS_CONFIG="${HTTPS_CONFIG} -e ATL_TOMCAT_SCHEME=https "
  HTTPS_CONFIG="${HTTPS_CONFIG} -e ATL_TOMCAT_SECURE=true "
fi

docker run \
  --name ${CONFLUENCE_CONTAINER_NAME} \
  --network ${CONFLUENCE_DOCKER_NETWORK} \
  ${PORTS_EXPOSE} \
  --restart always \
  --stop-timeout 300 \
  -v ${DATA_STORE_PATH}/${CONFLUENCE_CONTAINER_NAME}:/var/atlassian/application-data/confluence \
  -v ${WINDOWS_FONTS_PATH}:/usr/share/fonts/windowsfonts \
  -v ${JDBC_PATH}/mysql-connector-java-8.0.27.jar:/opt/atlassian/confluence/lib/mysql-connector-java-8.0.27.jar \
  --env-file ${CONTAINER_CONFIG_PATH}/common.env \
  -e ATL_PROXY_NAME=${CONFLUENCE_DOMAIN_NAME} \
  ${HTTPS_CONFIG} \
  -e CATALINA_OPTS="-Dall.jobs.ttl.hours=72 -Dconfluence.disable.peopledirectory.all=true -Dconfluence.document.conversion.fontpath=/usr/share/fonts/windowsfonts" \
  -d atlassian/confluence-server:${CONFLUENCE_VERSION}
