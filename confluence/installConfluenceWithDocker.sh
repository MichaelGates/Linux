## Install jira with docker image

CONFLUENCE_INSTALL_HOME=~/confluence

if [ ! -d "${CONFLUENCE_INSTALL_HOME}" ]; then
  mkdir ${CONFLUENCE_INSTALL_HOME}
fi

while [ -z "${CONFLUENCE_DOMAIN_NAME}" ]; do
  read -p "Input CONFLUENCE_DOMAIN_NAME of this server: " CONFLUENCE_DOMAIN_NAME
done

while [ -z "${CONFLUENCE_PORT}" ]; do
  read -p "Input CONFLUENCE_PORT of this server: " CONFLUENCE_PORT
done

## inital Jira configuration
. ./initialConfluence.sh

cp -pr conf/confluence_docker.conf ${CONFLUENCE_INSTALL_HOME}/conf/confluence_docker.conf

. ../nginx/addNginxConfWithDocker.sh ${CONFLUENCE_INSTALL_HOME}/conf/confluence_docker.conf

## pull docker image
docker pull atlassian/confluence-server
