#!/bin/bash

NEXUS_HOME=~/data/nexus

if [ ! -d "${NEXUS_HOME}" ]; then
  mkdir -p ${NEXUS_HOME}
fi

sudo ln -s ${PWD}/initialNexusContainerConfig.sh /usr/local/bin/initialNexusContainerConfig
sudo ln -s ${PWD}/startNexusContainer.sh /usr/local/bin/startNexusContainer
