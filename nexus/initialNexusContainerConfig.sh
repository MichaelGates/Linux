#!/bin/bash
## Initial nexus docker config file

CONTAINER_CONFIG_PATH=/etc/docker.d
CONFIG_SUFFIX=.nexus.conf
SOFTWARE_NAME=NEXUS

DEFAULT_CONTAINER_NAME=nexus
DEFAULT_IMAGE_TAG=latest
DEFAULT_CONTAINER_NETWORK=webserver
DEFAULT_EXPOSE_PORT=8081

echo Initial ${SOFTWARE_NAME} container config file ...

while [ -z "${CONFIG_NAME}" ]; do
  read -p "Input CONFIG_NAME: " CONFIG_NAME
  if [ -f ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX} ]; then
    echo "${CONFIG_NAME}${CONFIG_SUFFIX} already existed."
    echo ===== BEGIN ===== ${CONFIG_NAME}${CONFIG_SUFFIX} =====
    cat ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
    echo ===== END ===== ${CONFIG_NAME}${CONFIG_SUFFIX} =====
    read -n1 -p "Overright? (y/N/q) " YN
    echo
    if [ "${YN}" == "q" -o "${YN}" == "Q" ]; then
      exit 0
    fi
    if [ "${YN}" != "y" -a "${YN}" != "Y" ]; then
      CONFIG_NAME=
    fi
  fi
done

read -p "Input CONTAINER_NAME (default:${DEFAULT_CONTAINER_NAME}): " CONTAINER_NAME
if [ -z "${CONTAINER_NAME}" ]; then
  CONTAINER_NAME=${DEFAULT_CONTAINER_NAME}
fi

read -p "Input IMAGE_TAG (default:${DEFAULT_IMAGE_TAG}): " IMAGE_TAG
if [ -z "${IMAGE_TAG}" ]; then
  IMAGE_TAG=${DEFAULT_IMAGE_TAG}
fi

read -p "Input CONTAINER_NETWORK (default:${DEFAULT_CONTAINER_NETWORK}): " CONTAINER_NETWORK
if [ -z "${CONTAINER_NETWORK}" ]; then
  CONTAINER_NETWORK=${DEFAULT_CONTAINER_NETWORK}
fi

read -p "Input EXPOSE_PORT (0 for not expose, default:${DEFAULT_EXPOSE_PORT}): " EXPOSE_PORT
if [ -z "${EXPOSE_PORT}" ]; then
  EXPOSE_PORT=${DEFAULT_EXPOSE_PORT}
fi

## initial constant shell
echo -n Generate ${SOFTWARE_NAME} container config file...
cp template${CONFIG_SUFFIX} ${CONFIG_NAME}${CONFIG_SUFFIX}
sed -i'' -e "s/{CONTAINER_NAME}/${CONTAINER_NAME}/g" ${CONFIG_NAME}${CONFIG_SUFFIX}
sed -i'' -e "s/{IMAGE_TAG}/${IMAGE_TAG}/g" ${CONFIG_NAME}${CONFIG_SUFFIX}
sed -i'' -e "s/{CONTAINER_NETWORK}/${CONTAINER_NETWORK}/g" ${CONFIG_NAME}${CONFIG_SUFFIX}
sed -i'' -e "s/{EXPOSE_PORT}/${EXPOSE_PORT}/g" ${CONFIG_NAME}${CONFIG_SUFFIX}
sed -i'' -e "s/{CREATE_TIME}/`date +'%Y-%m-%d %T'`/g" ${CONFIG_NAME}${CONFIG_SUFFIX}
sed -i'' -e "s/{CREATE_USER}/`whoami`/g" ${CONFIG_NAME}${CONFIG_SUFFIX}
echo Done!

echo ===== BEGIN ===== ${CONFIG_NAME}${CONFIG_SUFFIX} =====
cat ${CONFIG_NAME}${CONFIG_SUFFIX}
echo ===== END ===== ${CONFIG_NAME}${CONFIG_SUFFIX} =====

echo -n Move ${CONFIG_NAME}${CONFIG_SUFFIX} to ${CONTAINER_CONFIG_PATH}... 
if [ ! -d ${CONTAINER_CONFIG_PATH} ]; then
  sudo mkdir -p ${CONTAINER_CONFIG_PATH}
fi
sudo mv ${CONFIG_NAME}${CONFIG_SUFFIX} ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
echo Done!

echo Generate ${SOFTWARE_NAME} container config complete!
