#!/usr/bin/env bash
## start mysqld exporter

docker network create my-mysql-network

MYSQL_HOST=""
MYSQL_USER=""
MYSQL_PASSWORD=""
MYSQL_SCHEMA=""

docker run -d \
  --restart always \
  --name mysqld_exporter \
  -p 9104:9104 \
  --network my-mysql-network \
  -e DATA_SOURCE_NAME="${MYSQL_USER}:${MYSQL_PASSWORD}@(${MYSQL_HOST}:3306)/" \
  prom/mysqld-exporter
