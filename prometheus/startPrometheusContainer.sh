#!/usr/bin/env bash
## start prometheus

SOFTWARE=prometheus
IMAGE_NAME=prom/prometheus
IMAGE_TAG=v2.39.1
CONTAINER_NAME=prometheus

CONTAINER_DATA_BASE=~/data

docker run -d \
  -p 9090:9090 \
  --restart always \
  --name ${CONTAINER_NAME} \
  -v ${CONTAINER_DATA_BASE}/${SOFTWARE}/${CONTAINER_NAME}/conf/prometheus.yml:/etc/prometheus/prometheus.yml \
  -v ${CONTAINER_DATA_BASE}/${SOFTWARE}/${CONTAINER_NAME}/data:/prometheus/data \
  ${IMAGE_NAME}:${IMAGE_TAG} \
  --config.file=/etc/prometheus/prometheus.yml \
  --storage.tsdb.path=/prometheus/data
