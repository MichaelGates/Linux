#!/usr/bin/env bash
## start blackbox exporter

SOFTWARE=prometheus
IMAGE_NAME=prom/blackbox-exporter
IMAGE_TAG=master
CONTAINER_NAME=blackbox_exporter

CONTAINER_DATA_BASE=~/data

docker run --rm -d \
  -p 9115:9115 \
  --restart always \
  --name ${CONTAINER_NAME} \
  -v ${CONTAINER_DATA_BASE}/${SOFTWARE}/${CONTAINER_NAME}/conf/blackbox.yml:/config \
  ${IMAGE_NAME}:${IMAGE_TAG} \
  --config.file=/config/blackbox.yml
