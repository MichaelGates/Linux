#!/usr/bin/env bash
## start node exporter

CONTAINER_NAME=node_exporter

docker run -d \
  --restart always \
  --net="host" \
  --pid="host" \
  --name ${CONTAINER_NAME} \
  -v "/:/host:ro,rslave" \
  prom/node-exporter:latest \
  --path.rootfs=/host
