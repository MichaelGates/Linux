#!/usr/bin/env bash
## Initial jira service management docker config file

CONTAINER_CONFIG_PATH=/etc/docker.d
SOFTWARE_NAME=JiraSM
CONFIG_SUFFIX=.jirasm.conf
IMAGE_NAME=atlassian/jira-servicemanagement

RUN_AS_DAEMON=Y
RESTART_POLICY=always
REMOVE_CONTAINER_AFTER_STOP=N
CONTAINER_INNER_PORT=8080
USER_ID=NOT_SPECIFY
GROUP_ID=NOT_SPECIFY
MOUNT_DATA_VOLUME=/var/atlassian/application-data/jira
HOST_VOLUME=~/data/jirasm
CUSTOMER_PARAMS=
EXEC_COMMAND=

DEFAULT_IMAGE_TAG=latest
DEFAULT_CONFIG_NAME=main
DEFAULT_CONTAINER_NAME=jirasm
DEFAULT_CONTAINER_NETWORK=webserver
DEFAULT_EXPOSE_PORT=0
DEFAULT_DOMAIN_NAME=localhost
DEFAULT_ENABLE_HTTPS=N

echo "SOFTWARE_NAME is: "${SOFTWARE_NAME}
echo "IMAGE_NAME is: "${IMAGE_NAME}

while [ -z "${CONFIG_NAME}" ]; do
  read -p "Input CONFIG_NAME (default:${DEFAULT_CONFIG_NAME}): " CONFIG_NAME
  if [ -z "${CONFIG_NAME}" ]; then
    CONFIG_NAME=${DEFAULT_CONFIG_NAME}
  fi
  if [ -f ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX} ]; then
    echo "${CONFIG_NAME}${CONFIG_SUFFIX} already existed."
    echo ===== ${CONFIG_NAME}${CONFIG_SUFFIX} =====
    echo ======= BEGIN ======= 
    cat ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
    echo =======  END  ======= 
    read -n1 -p "Overright? (y/N/q) " YN
    echo
    if [ "${YN}" == "q" -o "${YN}" == "Q" ]; then
      exit 0
    fi
    if [ "${YN}" != "y" -a "${YN}" != "Y" ]; then
      CONFIG_NAME=
    fi
  fi
done

read -p "Input IMAGE_TAG (default:${DEFAULT_IMAGE_TAG}): " IMAGE_TAG
if [ -z "${IMAGE_TAG}" ]; then
  IMAGE_TAG=${DEFAULT_IMAGE_TAG}
fi

read -p "Input CONTAINER_NAME (default:${DEFAULT_CONTAINER_NAME}): " CONTAINER_NAME
if [ -z "${CONTAINER_NAME}" ]; then
  CONTAINER_NAME=${DEFAULT_CONTAINER_NAME}
fi

echo "RUN_AS_DAEMON is: "${RUN_AS_DAEMON}

read -p "Input CONTAINER_NETWORK (default:${DEFAULT_CONTAINER_NETWORK}): " CONTAINER_NETWORK
if [ -z "${CONTAINER_NETWORK}" ]; then
  CONTAINER_NETWORK=${DEFAULT_CONTAINER_NETWORK}
fi

echo "RESTART_POLICY is: "${RESTART_POLICY}
echo "REMOVE_CONTAINER_AFTER_STOP is: "${REMOVE_CONTAINER_AFTER_STOP}
echo "CONTAINER_INNER_PORT is: "${CONTAINER_INNER_PORT}

read -p "Input EXPOSE_PORT (0 for not expose, default:${DEFAULT_EXPOSE_PORT}): " EXPOSE_PORT
if [ -z "${EXPOSE_PORT}" ]; then
  EXPOSE_PORT=${DEFAULT_EXPOSE_PORT}
fi

## jira service management special fields
read -p "Input DOMAIN_NAME (default:${DEFAULT_DOMAIN_NAME} ): " DOMAIN_NAME
if [ -z "$DOMAIN_NAME" ]; then
  DOMAIN_NAME=${DEFAULT_DOMAIN_NAME}
fi

read -n1 -p "Input ENABLE_HTTPS (default:${DEFAULT_ENABLE_HTTPS}): " ENABLE_HTTPS
if [ -z "${ENABLE_HTTPS}" ]; then
  ENABLE_HTTPS=${DEFAULT_ENABLE_HTTPS}
fi

## read customer params
read -p "Input CUSTOMER_PARAMS (if applicable): " CUSTOMER_PARAMS
## read executor command
read -p "Input EXEC_COMMAND (if applicable): " EXEC_COMMAND


initialParams="${initialParams} -s ${SOFTWARE_NAME}"
initialParams="${initialParams} -x ${CONFIG_SUFFIX}"
initialParams="${initialParams} -i ${IMAGE_NAME}"
initialParams="${initialParams} -t ${IMAGE_TAG}"
initialParams="${initialParams} -f ${CONFIG_NAME}"
initialParams="${initialParams} -c ${CONTAINER_NAME}"
initialParams="${initialParams} -d"
initialParams="${initialParams} -n ${CONTAINER_NETWORK}"
initialParams="${initialParams} -p ${CONTAINER_INNER_PORT}"
initialParams="${initialParams} -P ${EXPOSE_PORT}"
initialParams="${initialParams} -r ${RESTART_POLICY}"
#initialParams="${initialParams} -m"
initialParams="${initialParams} -u ${USER_ID}"
initialParams="${initialParams} -g ${GROUP_ID}"
initialParams="${initialParams} -v"
initialParams="${initialParams} -V ${HOST_VOLUME}/${CONTAINER_NAME}"
initialParams="${initialParams} -D ${MOUNT_DATA_VOLUME}"
initialParams="${initialParams} -E"

if [ "${CUSTOMER_PARAMS}" ]; then
  initialParams="${initialParams} -C ${CUSTOMER_PARAMS}"
fi
if [ "${EXEC_COMMAND}" ]; then
  initialParams="${initialParams} -e ${EXEC_COMMAND}"
fi

## call general script to generate constant file
initialContainerConfig ${initialParams}

## add software special parameters
NEW_CONFIG_FILE=${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
sudo sh -c "cat ${CONTAINER_CONFIG_PATH}/template${CONFIG_SUFFIX} >> ${NEW_CONFIG_FILE}"
sudo sed -i'' -e "s/{DOMAIN_NAME}/${DOMAIN_NAME}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{ENABLE_HTTPS}/${ENABLE_HTTPS}/g" ${NEW_CONFIG_FILE}
echo Done!

echo ===== BEGIN ===== ${NEW_CONFIG_FILE} =====
cat ${NEW_CONFIG_FILE}
echo ===== END ===== ${NEW_CONFIG_FILE} =====

echo Generate ${SOFTWARE_NAME} container config complete!
