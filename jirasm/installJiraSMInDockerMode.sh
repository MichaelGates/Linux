#!/usr/bin/env bash
## install Jira Service Management

SOFTWARE_NAME=JiraSM
type installSoftwareInDockerMode

if [ $? != 0 ]; then
  "installSoftwareInDockerMode" not found!
  exit;
fi

installSoftwareInDockerMode ${SOFTWARE_NAME}
