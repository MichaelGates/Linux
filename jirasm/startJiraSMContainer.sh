#!/usr/bin/env bash
## start jira service management container

CONTAINER_CONFIG_PATH=/etc/docker.d
SOFTWARE_NAME=JiraSM
CONFIG_SUFFIX=.${SOFTWARE_NAME,,}.conf
DATA_VOLUME_PATH=~/data/${SOFTWARE_NAME,,}

Usage="Usage: $0 [CONFIG_NAME]"

if [ ! -z "$1" ]; then
  CONFIG_NAME=$1
fi

listContainerConfigFiles ${CONFIG_SUFFIX}

while [ -z "${CONFIG_NAME}" ]; do
  read -p "Input ${SOFTWARE_NAME} CONFIG_NAME: " CONFIG_NAME
done

## check file
if [ ! -f ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX} ]; then
  echo "The config file ${CONFIG_NAME} is not exist. Please check it."
  echo "If you do not have a config file, please initial one."
  exit 1
fi

## import environment variable
. ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}

DAEMON_CONFIG=""
if [ "${RUN_AS_DAEMON}" == "y" -o "${RUN_AS_DAEMON}" == "Y" ]; then
  DAEMON_CONFIG=" -d"
fi

PORTS_EXPOSE=""
if [ "${EXPOSE_PORT}" != "0" -a "${EXPOSE_PORT}" != "" ]; then
  PORTS_EXPOSE="-p ${EXPOSE_PORT}:${CONTAINER_INNER_PORT}"
fi

HTTPS_CONFIG=""
if [ "${ENABLE_HTTPS}" == "y" -o "${ENABLE_HTTPS}" == "Y" ]; then
  HTTPS_CONFIG="${HTTPS_CONFIG} -e ATL_PROXY_PORT=443 "
  HTTPS_CONFIG="${HTTPS_CONFIG} -e ATL_TOMCAT_SCHEME=https "
  HTTPS_CONFIG="${HTTPS_CONFIG} -e ATL_TOMCAT_SECURE=true "
fi

VOLUME_CONFIG=""
if [ "${MOUNT_DATA_VOLUME}" == "y" -o "${MOUNT_DATA_VOLUME}" == "Y" ]; then
  VOLUME_CONFIG="${VOLUME_CONFIG} -v ${HOST_VOLUME}:${CONTAINER_INNER_DIRECTORY}"
fi

## see https://hub.docker.com/r/atlassian/jira-servicemanagement
COMMAND=(
docker run ${DAEMON_CONFIG}
  --name ${CONTAINER_NAME}
  --network ${CONTAINER_NETWORK}
  --restart ${RESTART_POLICY}
  --env-file ${CONTAINER_CONFIG_PATH}/common.env
  ${PORTS_EXPOSE}
  ${VOLUME_CONFIG}
  ${HTTPS_CONFIG}
  ${CUSTOMER_PARAMS}
  ${IMAGE}:${IMAGE_TAG}
  ${EXEC_COMMAND}
)

echo ===== BEGIN =====
echo ${COMMAND[*]}
echo ===== END =====

read -n1 -p "Are you sure to start? (y/N) " YN
echo
if [ "${YN}" == "y" -o "${YN}" == "Y" ]; then
  echo -n Start ${SOFTWARE_NAME} container ${CONTAINER_NAME}...
  if [ ! -d "${HOST_VOLUME}" ]; then
    mkdir -p ${HOST_VOLUME}
  fi
  ${COMMAND[*]}
  echo Done!
else
  echo Quit!
fi
