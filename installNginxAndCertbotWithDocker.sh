#!/usr/bin/env bash
## install nginx and certbot with docker

## Prepare parameters
while [ -z "${DOMAIN_NAME}" ]; do
  read -p "Input DOMAIN_NAME of this server(DDNS DOMAIN): " DOMAIN_NAME
done

while [ -z "${EMAIL}" ]; do
  read -p "Input SSL certification contact's email: " EMAIL
done

set -e

## Install nginx
cd nginx
./installNginxInDockerMode.sh
./startNginxContainer.sh
cd ..

## Install Certbot
cd certbot
./initialCertbotInDockerMode.sh
./generateCertInDockerMode.sh -d ${DOMAIN_NAME} -e ${EMAIL}
cd ..

cd nginx
./addNginxConfInDockerMode.sh -d ${DOMAIN_NAME} -h localhost -p 80 -f conf_docker/conf.d/main.conf.default
nginx -s reload
cd ..

echo OK!