
## Install Nginx/Certbot or not
while [ -z "${isInstallNginx}" ]; do
  read -n1 -p "Install Nginx and Certbot? [Y/n] " isInstallNginx
  case ${isInstallNginx} in
    "N"|"n" )
      isInstallNginx=n
      ;;
    "Y"|"y"|"" )
      isInstallNginx=y
      ;;
    *)
      isInstallNginx=
      ;;
  esac
done

## Install proxy
while [ -z "${isInstallProxy}" ]; do
  read -n1 -p "Install Squid and SS? [y/N] " isInstallProxy
  case ${isInstallProxy} in
    "N"|"n"|"" )
      isInstallProxy=n
      ;;
    "Y"|"y" )
      isInstallProxy=y
      ;;
    *)
      isInstallProxy=
      ;;
  esac
done

## Install Nginx and certbot
if [ "${isInstallNginx}" == "y" ]; then
  . ./installNginxAndCertbotWithDocker.sh
fi

## Install proxy software
if [ "${isInstallProxy}" == "y" ]; then
  . ./installProxyServer.sh
fi