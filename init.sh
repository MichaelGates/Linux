#!/usr/bin/env bash

## Initial server

chmod -R u+x *.sh */*.sh
tools/installTools.sh
./setTimezone.sh
other/installDig.sh
other/installTelnet.sh

## record boot time
addCrontab '@reboot echo `date` >> ~/boot.log'

## Install DDNS or not
while [ -z "${isInstallDDNS}" ]; do
  read -n1 -p "Install DDNS? [Y/n] " isInstallDDNS
  case ${isInstallDDNS} in
    "N"|"n" )
      isInstallDDNS=n
      ;;
    "Y"|"y"|"" )
      isInstallDDNS=y
      ;;
    *)
      isInstallDDNS=
      ;;
  esac
done

## Input DDNS install parameters
if [ "${isInstallDDNS}" == "y" ]; then
  read -p "Input ddns server'(default now-dns.com)':" DDNS_SERVER
  if [ -z "${DDNS_SERVER}" ]; then
    DDNS_SERVER=now-dns.com
  fi

  while [ -z "${DDNS_DOMAIN}" ]; do
    read -p "Input ddns domain: " DDNS_DOMAIN
  done

  while [ -z "${DDNS_USERNAME}" ]; do
    read -p "Input ddns username: " DDNS_USERNAME
  done

  while [ -z "${DDNS_PASSWORD}" ]; do
    read -s -p "Input ddns password: " DDNS_PASSWORD
    echo
  done
fi

## Install Docker or not
while [ -z "${isInstallDocker}" ]; do
  read -n1 -p "Install Docker? [Y/n] " isInstallDocker
  case ${isInstallDocker} in
    "N"|"n" )
      isInstallDocker=n
      ;;
    "Y"|"y"|"" )
      isInstallDocker=y
      ;;
    *)
      isInstallDocker=
      ;;
  esac
done

## Is the server in China?
while [ -z "${isInChina}" ]; do
  read -n1 -p "Is this server in mainland China? [y/N] " isInChina
  case ${isInChina} in
    "N"|"n"|"" )
      isInChina=n
      ;;
    "Y"|"y" )
      isInChina=y
      ;;
    *)
      isInChina=
      ;;
  esac
done

## set errexit
set -e

## Install DDNS
if [ "${isInstallDDNS}" == "y" ]; then
  cd ddns
  ./installDdns.sh -s ${DDNS_SERVER} -u ${DDNS_USERNAME} -p ${DDNS_PASSWORD} -d ${DDNS_DOMAIN}
  cd ..
fi

## Install Docker
if [ "${isInstallDocker}" == "y" ]; then
  cd docker
  ./installDocker.sh -C
  cd ..
fi

