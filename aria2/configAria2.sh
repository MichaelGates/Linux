
## create folder
mkdir ~/.aria2

## create session file
touch ~/.aria2/aria2.session

cp -pr aria2.conf.default ~/.aria2/aria2.conf

DEFAULT_RPC_SECRET=secret
read -p 'Input rpc secret: ' RPC_SECRET
if [ -z ${RPC_SECRET} ]; then
  RPC_SECRET=${DEFAULT_RPC_SECRET}
fi

sed -i "s|{HOME}|${HOME}|g" ~/.aria2/aria2.conf
sed -i "s/{RPC_SECRET}/${RPC_SECRET}/g" ~/.aria2/aria2.conf
