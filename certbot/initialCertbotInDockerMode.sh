#!/usr/bin/env bash
## Initial Certbot With Docker

INSTALL_BIN_PATH=/usr/local/bin

echo Creating generateCertInDockerMode.sh link in ${INSTALL_BIN_PATH} folder.
sudo ln -sf ${PWD}/generateCertInDockerMode.sh ${INSTALL_BIN_PATH}/generateCertInDockerMode
echo Done.

echo Creating renewCert.sh link in ${INSTALL_BIN_PATH} folder.
sudo ln -sf ${PWD}/renewCert.sh ${INSTALL_BIN_PATH}/renewCert
echo Done.

## Add to crontab task
echo Add crontab job...
## renew certifications when reboot
addCrontab "@reboot ${INSTALL_BIN_PATH}/renewCert.sh"
## renew certifications everyday at mid-night
addCrontab "0 0 * * * ${INSTALL_BIN_PATH}/renewCert.sh"
echo Done.

echo Begin to pull certbot docker image. This may take a few minutes...
docker pull certbot/certbot
echo Done!

