#!/usr/bin/env bash
## install certbot command to bin path

INSTALL_BIN_PATH=/usr/local/bin

## create link
echo Creating certbot link in ${INSTALL_BIN_PATH} folder.
sudo ln -sf ${PWD}/sbin/certbot ${INSTALL_BIN_PATH}/certbot
echo Done!
