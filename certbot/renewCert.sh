## certbot renew
CERTBOT_HOME=~/data/certbot

## get SSL cert only
docker run --rm \
  -v /etc/letsencrypt:/etc/letsencrypt \
  -v ${CERTBOT_HOME}/www:/var/www/certbot \
  -v ${CERTBOT_HOME}/logs:/var/log/letsencrypt \
  -v /var/lib/letsencrypt:/var/lib/letsencrypt \
  certbot/certbot renew
