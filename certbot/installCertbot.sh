#!/usr/bin/env bash
## Install certbot, and generate configuration 

while [ -z "${EMAIL}" ]; do
  read -p "Input contacter email:" EMAIL
done

echo Install certbot...

## Update python-urllib3 and python-requests
sudo yum install -y pip
sudo pip uninstall --yes requests urllib3
sudo yum remove -y python-urllib3 python-requests
sudo yum install -y python-urllib3 python-requests

## Install certbot and certbot-nginx
sudo yum install -y certbot-nginx

## Create certbot account
sudo certbot --nginx -n --agree-tos --email ${EMAIL}

## Add to crontab task
. ../tools/addCrontab.sh "0 0 * * * sudo certbot renew"

## Add to crontab task
. ../tools/addCrontab.sh "@reboot sudo certbot renew "

echo Certbot installed!
