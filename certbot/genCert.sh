
Usage=""
Usage=${Usage}" Usage:  $0 DOMAIN_NAME"

if [ -z "$1" ]; then
  echo ${Usage}
  exit 1
else
  DOMAIN_NAME=$1
fi

## get SSL cert only
sudo certbot --nginx certonly -n --domains ${DOMAIN_NAME}

## give other user accessing certifications rights
sudo chmod -R 755 /etc/letsencrypt/live
sudo chmod -R 755 /etc/letsencrypt/archive
