CERTBOT_HOME=/data/certbot

## get SSL cert only
docker run --rm \
  --env-file common.env \
  -v /etc/letsencrypt:/etc/letsencrypt \
  -v ${CERTBOT_HOME}/www:/var/www/certbot \
  -v ${CERTBOT_HOME}/logs:/var/log/letsencrypt \
  -v /var/lib/letsencrypt:/var/lib/letsencrypt \
  certbot/certbot:arm32v6-latest \
  renew

## give other user accessing certifications rights
sudo chmod -R 755 /etc/letsencrypt/live
sudo chmod -R 755 /etc/letsencrypt/archive
