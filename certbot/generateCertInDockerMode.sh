#!/usr/bin/env bash

## generate SSL Certification using docker container
## accept parameters: DOMAIN_NAME EMAIL

## usage
function usage() {
  Usage="Usage: $0 [options] \n"
  Usage="${Usage}  options: \n"
  Usage="${Usage}\t -h        : help \n"
  Usage="${Usage}\t -e string : EMAIL \n"
  Usage="${Usage}\t -d string : DOMAIN_NAME \n"
  echo -e ${Usage}
}

## parse options
while getopts 'hd:e:' OPT; do
  case $OPT in
    h)
      HELP="TRUE";;
    d)
      DOMAIN_NAME="$OPTARG";;
    e)
      EMAIL="$OPTARG";;
    ?)
      usage
      exit 1
  esac
done

if [ "${HELP}" = "TRUE" -o "$1" = "--help" ]; then
  usage
  exit 0
fi

## input parameter if not set
function inputParam() {
  while [ -z "${DOMAIN_NAME}" ]; do
    read -p "Input DOMAIN_NAME: " DOMAIN_NAME
  done

  while [ -z "${EMAIL}" ]; do
    read -p "Input HTTPS contact's email: " EMAIL
  done
}
inputParam

CERTBOT_HOME=~/data/certbot
CONTAINER_CONFIG_PATH=/etc/docker.d

## get SSL cert only
docker run --rm \
  --env-file ${CONTAINER_CONFIG_PATH}/common.env \
  -v /etc/letsencrypt:/etc/letsencrypt \
  -v ${CERTBOT_HOME}/www:/var/www/certbot \
  -v ${CERTBOT_HOME}/logs:/var/log/letsencrypt \
  -v /var/lib/letsencrypt:/var/lib/letsencrypt \
  certbot/certbot \
  certonly --webroot --webroot-path=/var/www/certbot --email ${EMAIL} --agree-tos --no-eff-email -d ${DOMAIN_NAME}

## give other user accessing certifications rights
sudo chmod -R 755 /etc/letsencrypt/live
sudo chmod -R 755 /etc/letsencrypt/archive
