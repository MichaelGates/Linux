#!/usr/bin/env bash

## Initial timezone and reset date time format
echo Changing timezone to Asia/Hong_Kong
sudo timedatectl set-timezone Asia/Hong_Kong

function installChrony() {
  ## Install chrony
  sudo yum install -y chrony
  ## Enable and start chronyd
  sudo systemctl enable chronyd
  sudo systemctl start chronyd  
}
function installNtp() {
  ## Install ntp
  sudo yum install -y ntp
  ## Enable and start ntpd
  sudo systemctl enable ntpd
  sudo systemctl start ntpd
}

OS_VERSION=`osinfo.sh`
if [ "${OS_VERSION}" = "centos 8" -o "${OS_VERSION}"="rocky 8" ]; then
  installChrony
else
  installNtp
fi

date

#echo Time sync...
#sudo ntpdate us.pool.ntp.org

## Restart crond and rsyslog to ensure crond execute in correct time and syslog logs the correct time
sudo systemctl restart crond
sudo systemctl restart rsyslog
