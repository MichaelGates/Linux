#!/usr/bin/env bash
## Restart nginx

CONTAINER_NAME=nginx

set -e

docker stop ${CONTAINER_NAME}
docker rm ${CONTAINER_NAME}
startNginx
