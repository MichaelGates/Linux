#!/usr/bin/env bash
INSTALL_BIN_PATH=/usr/local/bin

## create link
echo Creating nginx related links to ${INSTALL_BIN_PATH} folder.

sudo ln -sf ${PWD}/sbin/nginx ${INSTALL_BIN_PATH}/nginx

sudo ln -sf ${PWD}/startNginxContainer.sh ${INSTALL_BIN_PATH}/startNginx

sudo ln -sf ${PWD}/reloadNginxConfig.sh ${INSTALL_BIN_PATH}/reloadNginxConfig

sudo ln -sf ${PWD}/changeNginxConfigStatus.sh ${INSTALL_BIN_PATH}/changeNginxConfigStatus

sudo ln -sf ${PWD}/enableNginxConfig.sh ${INSTALL_BIN_PATH}/enableNginxConfig

sudo ln -sf ${PWD}/disableNginxConfig.sh ${INSTALL_BIN_PATH}/disableNginxConfig

sudo ln -sf ${PWD}/addNginxConfInDockerMode.sh ${INSTALL_BIN_PATH}/addNginxConfInDockerMode

sudo ln -sf ${PWD}/addNginxConfInDockerModeWithCert.sh ${INSTALL_BIN_PATH}/addNginxConfInDockerModeWithCert

echo Created!
