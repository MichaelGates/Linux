#!/usr/bin/env bash

## Add application nginx config file
## Acceptable parameter: CONFIG_FILE DOMAIN_NAME EMAIL APP_HOST APP_PORT

## usage
function usage() {
  Usage="Usage: $0 [options] \n"
  Usage="${Usage}  options: \n"
  Usage="${Usage}\t --help    : help \n"
  Usage="${Usage}\t -f string : CONFIG_FILE \n"
  Usage="${Usage}\t -d string : DOMAIN_NAME \n"
  Usage="${Usage}\t -e string : EMAIL \n"
  Usage="${Usage}\t -h string : APP_HOST \n"
  Usage="${Usage}\t -p string : APP_PORT \n"
  echo -e ${Usage}
}

## parse options
while getopts 'f:d:e:h:p:' OPT; do
  case $OPT in
    f)
      CONFIG_FILE="$OPTARG";;
    d)
      DOMAIN_NAME="$OPTARG";;
    e)
      EMAIL="$OPTARG";;
    h)
      APP_HOST="$OPTARG";;
    p)
      APP_PORT="$OPTARG";;
    ?)
      usage
      exit 1
  esac
done

if [ "$1" = "--help" ]; then
  usage
  exit 0
fi

NGINX_HOME_PATH=~/data/nginx/nginx

## input parameter if not set
function inputParam() {
  while [ -z "${DOMAIN_NAME}" ]; do
    read -p "Input APPLICATION DOMAIN_NAME: " DOMAIN_NAME
  done

  while [ -z "${EMAIL}" ]; do
    read -p "Input email: " EMAIL
  done

  while [ -z "${APP_HOST}" ]; do
    read -p "Input APP HOST: " APP_HOST
  done

  while [ -z "${APP_PORT}" ]; do
    read -p "Input APP PORT: " APP_PORT
  done
}
inputParam

set -e

generateCertInDockerMode -d ${DOMAIN_NAME} -e ${EMAIL}

if [ -n "${CONFIG_FILE}" ]; then
  config_file_param="-f ${CONFIG_FILE}"
fi
addNginxConfInDockerMode -d ${DOMAIN_NAME} -h ${APP_HOST} -p ${APP_PORT} ${config_file_param}
