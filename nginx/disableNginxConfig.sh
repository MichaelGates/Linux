## Disable nginx config for some domain

Usage="Usage: $0 DOMAIN_NAME \n"
Usage="${Usage} Disable nginx config file for DOMAIN_NAME"

if [ "$1" = "-h" -o "$1" = "--help" ]; then
  echo -e ${Usage}
  exit 0
fi

if [ $# -gt 1 ]; then
  echo ${Usage}
  exit 0
fi

if [ ! -z $1 ]; then
  DOMAIN_NAME=$1
fi

while [ -z "${DOMAIN_NAME}" ]; do
  read -p "Input APPLICATION DOMAIN_NAME: " DOMAIN_NAME
done

changeNginxConfigStatus disable ${DOMAIN_NAME}
