#!/usr/bin/env bash
## Reload nginx nginx config

nginx -t
if [ $? -eq 0 ]; then
  nginx -s reload
fi
