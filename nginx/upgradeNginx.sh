## Update nginx version

OS_USER=`whoami`
NGINX_INSTALL_PATH=/data/nginx

while [ -z "${NGINX_VER}" ]; do
  read -p "Input new nginx Ver:" NGINX_VER
done

if [ ! -f "nginx-${NGINX_VER}.tar.gz" ]; then
  wget -c https://nginx.org/download/nginx-${NGINX_VER}.tar.gz
fi
tar zxvf nginx-${NGINX_VER}.tar.gz
cd nginx-${NGINX_VER}

## configure options
configure_opts=(
  --prefix=${NGINX_INSTALL_PATH}
  --with-http_ssl_module
  --with-stream
  --with-http_v2_module
  --with-http_stub_status_module
  --user=nginx
  --group=nginx
  --with-http_geoip_module
  --with-http_realip_module
)

./configure ${configure_opts[*]}
if [ $? -ne 0 ]; then
  echo nginx configure error!
  exit 1
fi
make && sudo make install
if [ $? -ne 0 ]; then
  echo Nginx install error!
  exit 1
fi
sudo chown -R ${OS_USER}:${OS_USER} ${NGINX_INSTALL_PATH}
cd ..
rm -rf nginx-${NGINX_VER}

sudo systemctl stop nginx
sudo systemctl start nginx

# sudo kill -USR2 `cat ${NGINX_INSTALL_PATH}/logs/nginx.pid`
# sudo kill -WINCH `cat ${NGINX_INSTALL_PATH}/logs/nginx.pid.oldbin`
# sudo kill -QUIT `cat ${NGINX_INSTALL_PATH}/logs/nginx.pid.oldbin`
