    server {
        listen       80;
        server_name  {DOMAIN_NAME};

        location / {
            rewrite ^(.*)$         https://$host$1 permanent;
        }
    }

    server {
        listen       443 ssl http2;
        server_name  {DOMAIN_NAME};

        access_log                 /var/log/nginx/access_{DOMAIN_NAME}.log main buffer=64k flush=1m;
        error_log                  /var/log/nginx/error_{DOMAIN_NAME}.log error;

        ssl_certificate            /etc/letsencrypt/live/{DOMAIN_NAME}/fullchain.pem;
        ssl_certificate_key        /etc/letsencrypt/live/{DOMAIN_NAME}/privkey.pem;
        ssl_trusted_certificate    /etc/letsencrypt/live/{DOMAIN_NAME}/chain.pem;

        include                    ssl_params;

        resolver                   8.8.8.8 8.8.4.4 valid=300s;
        resolver_timeout           5s;

        add_header                 Strict-Transport-Security "max-age=31536000" always;
        add_header                 X-Content-Type-Options nosniff;
        add_header                 X-Frame-Options sameorigin;
        add_header                 Cache-Control  no-cache;

        if ($request_method !~ ^(GET|HEAD|POST|OPTIONS)$ ) {
            return                444;
        }

        location /mystatus {
            stub_status;
        }

        location / {
            root   html;
            index  index.html index.htm;
            
            #proxy_pass             http://localhost:${APP_PORT};
            #include                proxy_params;

            #add_header             Cache-Control  max-age=604800;
            #add_header             Cache-Status   $upstream_cache_status;

            #proxy_cache            cache_zone;
            # set expire time of code 200/304 : 10 minutes
            #proxy_cache_valid      200 304  10m;
            # set expire time of code 404 : 1 minute
            #proxy_cache_valid      404      1m;

        }
    }

    server {
        listen                     80 default_server;
        server_name                _;
        return 444;
    }

    server {
        listen                     443 ssl http2 default_server;
        server_name                _;
        ssl_certificate            /etc/letsencrypt/live/{DOMAIN_NAME}/fullchain.pem;
        ssl_certificate_key        /etc/letsencrypt/live/{DOMAIN_NAME}/privkey.pem;
        ssl_trusted_certificate    /etc/letsencrypt/live/{DOMAIN_NAME}/chain.pem;
        return 444;
    }
