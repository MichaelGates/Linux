## Install GeoIP database
echo Download GeoIP database...

GEOIP_INSTALL_PATH=/usr/local/share/GeoIP
[ ! -d "${GEOIP_INSTALL_PATH}" ] && sudo mkdir -p ${GEOIP_INSTALL_PATH}

if [ ! -f "${GEOIP_INSTALL_PATH}/GeoIP.dat" ]; then
  wget https://raw.githubusercontent.com/MichaelXIE/GeoLite2/master/GeoIP.dat.gz
  ## original link is unavailiable
  # wget http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz
  gunzip GeoIP.dat.gz
  sudo mv GeoIP.dat ${GEOIP_INSTALL_PATH}/
fi

if [ ! -f "${GEOIP_INSTALL_PATH}/GeoLiteCity.dat" ]; then
  wget https://raw.githubusercontent.com/MichaelXIE/GeoLite2/master/GeoLiteCity.dat.gz
  ## original link is unavailiable
  # wget http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz
  gunzip GeoLiteCity.dat.gz
  sudo mv GeoLiteCity.dat ${GEOIP_INSTALL_PATH}/
fi

ls -l ${GEOIP_INSTALL_PATH}

echo GeoIP database installed in ${GEOIP_INSTALL_PATH}.
