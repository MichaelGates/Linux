## Add apps to nginx conf

NGINX_INSTALL_PATH=/data/nginx

Usage=""
Usage=${Usage}" Usage:  $0 \"path/to/configfile\""

if [ -z "$1" ]; then
  echo ${Usage}
  exit 1
else
  conf_file=$1
fi

while [ -z "${DOMAIN_NAME}" ]; do
  read -p "Input APP DOMAIN_NAME: " DOMAIN_NAME
done

while [ -z "${APP_PORT}" ]; do
  read -p "Input APP PORT: " APP_PORT
done

sh ../certbot/genCert.sh ${DOMAIN_NAME}

sudo cp -pr ${conf_file} ${NGINX_INSTALL_PATH}/conf/conf.d/${DOMAIN_NAME}.conf
sudo sed -i "s/{DOMAIN_NAME}/${DOMAIN_NAME}/g" ${NGINX_INSTALL_PATH}/conf/conf.d/${DOMAIN_NAME}.conf
sudo sed -i "s/{APP_PORT}/${APP_PORT}/g" ${NGINX_INSTALL_PATH}/conf/conf.d/${DOMAIN_NAME}.conf

sudo systemctl reload nginx
