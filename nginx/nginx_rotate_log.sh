## rotate nginx logs

NGINX_INSTALL_PATH=/data/nginx
YESTERDAY=`date -d yesterday +%Y%m%d`

## copy logs to history path
mkdir -p ${NGINX_INSTALL_PATH}/logs/history/${YESTERDAY}
mv ${NGINX_INSTALL_PATH}/logs/*.log ${NGINX_INSTALL_PATH}/logs/history/${YESTERDAY}/

sudo kill -USR1 `sudo cat ${NGINX_INSTALL_PATH}/logs/nginx.pid`
#sleep 1
#sudo gzip access.log.0    # do something with access.log.0
