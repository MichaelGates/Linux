#!/usr/bin/env bash

## Add application nginx config file
## Acceptable parameter: CONFIG_FILE DOMAIN_NAME APP_HOST APP_PORT

## usage
function usage() {
  Usage="Usage: $0 [options] \n"
  Usage="${Usage}  options: \n"
  Usage="${Usage}\t --help    : help \n"
  Usage="${Usage}\t -f string : CONFIG_FILE \n"
  Usage="${Usage}\t -d string : DOMAIN_NAME \n"
  Usage="${Usage}\t -h string : APP_HOST \n"
  Usage="${Usage}\t -p string : APP_PORT \n"
  echo -e ${Usage}
}

## parse options
while getopts 'f:d:h:p:' OPT; do
  case $OPT in
    f)
      CONFIG_FILE="$OPTARG";;
    d)
      DOMAIN_NAME="$OPTARG";;
    h)
      APP_HOST="$OPTARG";;
    p)
      APP_PORT="$OPTARG";;
    ?)
      usage
      exit 1
  esac
done

if [ "$1" = "--help" ]; then
  usage
  exit 0
fi

NGINX_HOME_PATH=~/data/nginx/nginx
DEFAULT_CONFIG_FILE=~/data/nginx/default/conf.d/domain.conf.default

## input parameter if not set
function inputParam() {
  while [ -z "${DOMAIN_NAME}" ]; do
    read -p "Input APPLICATION DOMAIN_NAME: " DOMAIN_NAME
  done

  while [ -z "${APP_HOST}" ]; do
    read -p "Input APP HOST: " APP_HOST
  done

  while [ -z "${APP_PORT}" ]; do
    read -p "Input APP PORT: " APP_PORT
  done

  if [ -z "${CONFIG_FILE}" ]; then
    CONFIG_FILE=${DEFAULT_CONFIG_FILE}
  fi
}
inputParam

if [ ! -f "${CONFIG_FILE}" ]; then
  echo CONFIG_FILE "${CONFIG_FILE}" not found.
  exit 1;
fi

set -e

echo Creating nginx config file...
sudo cp -pr ${CONFIG_FILE} ${NGINX_HOME_PATH}/conf/conf.d/${DOMAIN_NAME}.conf
sudo sed -i "s/{DOMAIN_NAME}/${DOMAIN_NAME}/g" ${NGINX_HOME_PATH}/conf/conf.d/${DOMAIN_NAME}.conf
sudo sed -i "s/{APP_HOST}/${APP_HOST}/g" ${NGINX_HOME_PATH}/conf/conf.d/${DOMAIN_NAME}.conf
sudo sed -i "s/{APP_PORT}/${APP_PORT}/g" ${NGINX_HOME_PATH}/conf/conf.d/${DOMAIN_NAME}.conf

echo Creating logs folder for ${DOMAIN_NAME}...
if [ ! -d "${NGINX_HOME_PATH}/logs/${DOMAIN_NAME}" ]; then
  sudo mkdir ${NGINX_HOME_PATH}/logs/${DOMAIN_NAME}
  sudo chown 101:101 ${NGINX_HOME_PATH}/logs/${DOMAIN_NAME}
fi

echo Done!
