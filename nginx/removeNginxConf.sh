## Add apps to nginx conf

NGINX_INSTALL_PATH=/data/nginx

Usage=""
Usage=${Usage}" Usage:  $0 \"DOMAIN_NAME\""

DOMAIN_NAME=$1

while [ -z "${DOMAIN_NAME}" ]; do
  read -p "Input APP DOMAIN_NAME: " DOMAIN_NAME
done

sudo rm -f ${NGINX_INSTALL_PATH}/conf/conf.d/${DOMAIN_NAME}.https.conf

sudo systemctl reload nginx
