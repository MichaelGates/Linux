## Change nginx config

Usage="Usage: $0 [action] [DOMAIN_NAME] \n"
Usage="${Usage} action enable/disable"

DISABLE_SUFFIX=.disable

if [ ! -z $1 ]; then
  action=$1
fi 

if [ ! -z $2 ]; then
  DOMAIN_NAME=$2
fi 

while [ -z "${action}" ]; do
  read -p "Input action (enable / disable): " action
done

while [ -z "${DOMAIN_NAME}" ]; do
  read -p "Input APPLICATION DOMAIN_NAME: " DOMAIN_NAME
done

NGINX_HOME=~/data/nginx/nginx
CONFIG_FILE=${NGINX_HOME}/conf/conf.d/${DOMAIN_NAME}.conf

if [ "${action}" == "enable" ]; then
  if [ ! -f ${CONFIG_FILE}${DISABLE_SUFFIX} ]; then
    echo Can not find ${CONFIG_FILE}${DISABLE_SUFFIX}.
    exit 1
  fi
  echo -n Enable ${DOMAIN_NAME} config...
  mv ${CONFIG_FILE}${DISABLE_SUFFIX} ${CONFIG_FILE}
  nginx -t
  if [ $? -eq 0 ]; then
    nginx -s reload
  else
    echo -n Nginx test failed! Restoring...
    mv ${CONFIG_FILE} ${CONFIG_FILE}${DISABLE_SUFFIX}
    echo Done!
    exit 1
  fi
fi

if [ "${action}" == "disable" ]; then
  if [ ! -f ${CONFIG_FILE} ]; then
    echo Can not find ${CONFIG_FILE}.
    exit 1
  fi
  echo -n Disable ${DOMAIN_NAME} config...
  mv ${CONFIG_FILE} ${CONFIG_FILE}${DISABLE_SUFFIX}
  echo Done!

  echo -n Testing nginx config...
  nginx -t
  if [ $? -eq 0 ]; then
    nginx -s reload
    echo Done! Nginx Reloaded!
  else
    echo -n Nginx test failed! Please continue to check!
#    mv ${CONFIG_FILE}${DISABLE_SUFFIX} ${CONFIG_FILE}
#    echo Done!
  fi
fi
