
OS_USER=`whoami`
NGINX_VER=1.16.1
NGINX_INSTALL_PATH=/data/nginx

curr_path=`pwd`
script_path=`cd "$(dirname $0)" && pwd`

cd ${script_path}

echo Install nginx...

## Install dependencies
sudo yum install -y gcc-c++ pcre pcre-devel zlib zlib-devel openssl openssl-devel wget GeoIP-devel

sudo useradd --system --no-create-home --shell /sbin/nologin --user-group nginx
if [ ! -d "${NGINX_INSTALL_PATH}" ]; then
  sudo mkdir -p ${NGINX_INSTALL_PATH}
fi

if [ ! -f "nginx-${NGINX_VER}.tar.gz" ]; then
  wget -c https://nginx.org/download/nginx-${NGINX_VER}.tar.gz
fi
tar zxvf nginx-${NGINX_VER}.tar.gz
cd nginx-${NGINX_VER}

## configure options
configure_opts=(
  --prefix=${NGINX_INSTALL_PATH}
  --with-http_ssl_module
  --with-stream
  --with-http_v2_module
  --with-http_stub_status_module
  --user=nginx
  --group=nginx
  --with-http_geoip_module
  --with-http_realip_module
)

./configure ${configure_opts[*]}
if [ $? -ne 0 ]; then
  echo nginx configure error!
  exit 1
fi
make && sudo make install
if [ $? -ne 0 ]; then
  echo Nginx install error!
  exit 1
fi
sudo chown -R ${OS_USER}:${OS_USER} ${NGINX_INSTALL_PATH}
cd ..
rm -rf nginx-${NGINX_VER}

## Install GeoIp database
. ./installGeoIpDb.sh

## Because certbot need to access executer and conf files, create soft link to PATH
if [ ! -f "/usr/sbin/nginx" ]; then
  sudo ln -s ${NGINX_INSTALL_PATH}/sbin/nginx /usr/sbin/nginx
fi

if [ ! -d "/etc/nginx" ]; then
  sudo ln -s ${NGINX_INSTALL_PATH}/conf /etc/nginx
fi

cp -pr conf/* ${NGINX_INSTALL_PATH}/conf/

## log rotate
if [ ! -d "~/cron" ]; then
  mkdir -p ~/cron
fi
cp -pr nginx_rotate_log.sh ~/cron/nginx_rotate_log.sh
. ../tools/addCrontab.sh "0 0 * * * sh ~/cron/nginx_rotate_log.sh"

. ../tools/installService.sh nginx

sudo systemctl start nginx

cd ${curr_path}

echo Nginx installed!