#!/usr/bin/env bash

## Install nginx in docker mode
echo === Install Nginx in Docker mode ===

## Prerequisite
prerequisites=(
  docker
)
echo "Prerequisite: ${prerequisites[*]}"

## Exit immediately if a command exits with a non-zero status.
set -e
## Prerequisites check
checkCommands ${prerequisites[*]}

SOFTWARE_NAME=NGINX
CONFIG_SUFFIX=.${SOFTWARE_NAME,,}.conf
CONTAINER_CONFIG_PATH=/etc/docker.d
CONTAINER_DATA_HOME=~/data/${SOFTWARE_NAME,,}
INSTALL_BIN_PATH=/usr/local/bin

## create link
./installNginxCommand.sh

## Copy config files
function copyDefaultConfigFiles() {
  echo Copying nginx config files...
  sudo cp stable.nginx.conf ${CONTAINER_CONFIG_PATH}/

  if [ ! -d "${CONTAINER_DATA_HOME}" ]; then
    mkdir -p ${CONTAINER_DATA_HOME}
  fi

  if [ -d "${CONTAINER_DATA_HOME}/default" ]; then
    sudo rm -rf ${CONTAINER_DATA_HOME}/default
  fi
  mkdir -p ${CONTAINER_DATA_HOME}/default
  cp -r conf_docker/* ${CONTAINER_DATA_HOME}/default/

  echo Copied!
}
copyDefaultConfigFiles

## Install GeoIp database
./installGeoIpDb.sh

## Add to crontab task
addCrontab "0 1 * * * ${INSTALL_BIN_PATH}/nginx -s reload"

echo Nginx installed!