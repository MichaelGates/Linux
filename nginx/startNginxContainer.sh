#!/usr/bin/env bash
## start nginx container

CONTAINER_CONFIG_PATH=/etc/docker.d
SOFTWARE_NAME=NGINX
CONFIG_SUFFIX=.${SOFTWARE_NAME,,}.conf
IMAGE_NAME=nginx
DATA_VOLUMN_PATH=~/data/${SOFTWARE_NAME,,}

CONFIG_NAME=stable
CONTAINER_UID=101
CONTAINER_GID=101

## check file
CONFIG_FILE_NAME=${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
if [ ! -f ${CONFIG_FILE_NAME} ]; then
  echo "The config file ${CONFIG_NAME} is not exist. Please check it."
  echo "If you do not have a config file, please initial one."
  exit 1
fi

## import environment variable
. ${CONFIG_FILE_NAME}

## create docker network: ${CONTAINER_NETWORK}
isNetworkExisted=`docker network ls | grep ${CONTAINER_NETWORK}`
if [ -z "${isNetworkExisted}" ]; then
  echo Creating docker network "${CONTAINER_NETWORK}"...
  docker network create "${CONTAINER_NETWORK}"
  echo Created!
fi

set -e

## create container data folder
CONTAINER_DATA_PATH=${DATA_VOLUMN_PATH}/${CONTAINER_NAME}
if [ ! -d "${CONTAINER_DATA_PATH}" ]; then
  echo Creating Nginx Home folder...
  mkdir -p ${CONTAINER_DATA_PATH}
  echo Created!
fi

## copy default config files
if [ ! -d "${CONTAINER_DATA_PATH}/conf" ]; then
  cp -pr ${DATA_VOLUMN_PATH}/default/. ${CONTAINER_DATA_PATH}/conf/
fi

## create log folder
if [ ! -d "${CONTAINER_DATA_PATH}/logs" ]; then
  mkdir -p ${CONTAINER_DATA_PATH}/logs
fi

## change data folder owner to caintainer nginx worker uid, gid 
sudo chown -R ${CONTAINER_UID}:${CONTAINER_GID} ${CONTAINER_DATA_PATH}

if [ "${START_ON_BOOT}" == "Y" -o "${START_ON_BOOT}" == "y" ]; then
  echo Add nginx to crontab...
  addCrontab "@reboot sleep 10 && /usr/local/bin/startNginx"
  echo Added.
fi

EXPOSE_PORT="-p 80:80 -p 443:443"
if [ "${CONTAINER_NETWORK}" = "host" ]; then
  EXPOSE_PORT=""
fi

function checkContainerVolume() {
  if [ ! -f "${CONTAINER_DATA_PATH}/conf/nginx.conf" ]; then
    echo ${CONTAINER_DATA_PATH}/conf/nginx.conf not exists.
    exit 1
  fi
  if [ ! -f "${CONTAINER_DATA_PATH}/conf/proxy_params" ]; then
    echo ${CONTAINER_DATA_PATH}/conf/proxy_params not exists.
    exit 1
  fi
  if [ ! -f "${CONTAINER_DATA_PATH}/conf/ssl_params" ]; then
    echo ${CONTAINER_DATA_PATH}/conf/ssl_params not exists.
    exit 1
  fi
  if [ ! -d "${CONTAINER_DATA_PATH}/conf/conf.d" ]; then
    echo ${CONTAINER_DATA_PATH}/conf/conf.d not exists.
    exit 1
  fi
  if [ ! -d "${CONTAINER_DATA_PATH}/logs" ]; then
    echo ${CONTAINER_DATA_PATH}/logs not exists.
    exit 1
  fi
}
checkContainerVolume

## start nginx container
docker run -d \
  --name ${CONTAINER_NAME} \
  --network ${CONTAINER_NETWORK} \
  --restart always \
  ${EXPOSE_PORT} \
  -v ${CONTAINER_DATA_PATH}/conf/nginx.conf:/etc/nginx/nginx.conf \
  -v ${CONTAINER_DATA_PATH}/conf/logformat.conf:/etc/nginx/logformat.conf \
  -v ${CONTAINER_DATA_PATH}/conf/geoip.conf:/etc/nginx/geoip.conf \
  -v ${CONTAINER_DATA_PATH}/conf/proxy_params:/etc/nginx/proxy_params \
  -v ${CONTAINER_DATA_PATH}/conf/ssl_params:/etc/nginx/ssl_params \
  -v ${CONTAINER_DATA_PATH}/conf/conf.d:/etc/nginx/conf.d \
  -v ${CONTAINER_DATA_PATH}/logs:/var/log/nginx \
  -v /usr/local/share/GeoIP:/usr/local/share/GeoIP \
  -v /etc/letsencrypt:/etc/letsencrypt \
  -v ~/data/certbot/www:/var/www/certbot \
  --env-file ${CONTAINER_CONFIG_PATH}/common.env \
  ${IMAGE_NAME}:${IMAGE_TAG}
