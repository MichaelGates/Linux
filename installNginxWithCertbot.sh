## install Nginx with certbot

## Prepare parameters
while [ -z "${DOMAIN_NAME}" ]; do
  read -p "Input DOMAIN_NAME of this server: " DOMAIN_NAME
done

while [ -z "${EMAIL}" ]; do
  read -p "Input SSL certification contacter email: " EMAIL
done
export DOMAIN_NAME
export APP_PORT=0
export EMAIL

## Install nginx
cd nginx
. ./makeAndInstallNginx.sh
cd ..

## Install certbot
cd certbot
. ./installCertbot.sh
cd ..

cd nginx
. ./addNginxConf.sh conf/conf.d/main.https.conf.default
cd ..
