## start redis cli
. ./redis_const.sh

docker run -it --rm \
  --network redis-network \
  -e TZ=Asia/Hong_Kong \
  redis \
  redis-cli -h ${REDIS_CONTAINER_NAME}
