## initial redis with docker

echo Initial REDIS with docker.

DEFAULT_REDIS_CONTAINER_NAME=redis

read -p "Input REDIS_CONTAINER_NAME (${DEFAULT_REDIS_CONTAINER_NAME} as default): " REDIS_CONTAINER_NAME
if [ -z "$REDIS_CONTAINER_NAME" ]; then
  REDIS_CONTAINER_NAME=${DEFAULT_REDIS_CONTAINER_NAME}
fi

cp redis_const.default.sh redis_const.sh

sed -i "s/{REDIS_CONTAINER_NAME}/${REDIS_CONTAINER_NAME}/g" redis_const.sh

echo Done!