
. ./redis_const.sh

docker network rm redis-network
docker network create redis-network

docker run \
  --name ${REDIS_CONTAINER_NAME} \
  --network=redis-network \
  --restart always \
  -p 6379:6379 \
  -v ~/data/redis:/data \
  -e TZ=Asia/Hong_Kong \
  -d redis
