## Install redis with source code

echo Install redis with source code...

## Download the latest stable redis
## -N option to only download if the file has been updated
wget -N http://download.redis.io/redis-stable.tar.gz

sudo yum install -y tcl make gcc-c++
tar xzf redis-stable.tar.gz
cd redis-stable
make && make test && sudo make install

if [ $? -ne 0 ]; then
  echo Redis install error!
  exit 1
fi

src/redis-server --version

## Install your Redis-server Service:
sudo utils/install_server.sh <<EOL
6379
/etc/redis/6379.conf
/var/log/redis_6379.log
/var/lib/redis/6379
/usr/local/bin/redis-server
EOL

## Check redis service status
systemctl status redis_6379

## Start and enable redis service
sudo systemctl start redis_6379
sudo systemctl enable redis_6379

echo Redis installed!