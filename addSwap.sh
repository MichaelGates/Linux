## Add swap partition in CentOS

while [ -z "$swapSize" ]; do
  read -p "Input swap size(MB): " swapSize
  echo ""
done

## See https://www.digitalocean.com/community/tutorials/additional-recommended-steps-for-new-centos-7-servers
echo Create a swap file /swapfile. It may take several minutes ...

## Allocate the space you want to use for your swap file using the fallocate utility. 
## For example, if we need a 4 Gigabyte file, we can create a swap file located at /swapfile by typing the following
## See https://www.digitalocean.com/community/questions/sudo-swapon-swapfile-error
sudo dd if=/dev/zero of=/swapfile count=${swapSize} bs=1MiB

## After creating the file, we need to restrict access to the file so that other 
## users or processes cannot see what is written there:
sudo chmod 600 /swapfile

## We now have a file with the correct permissions. To tell our system to format the file for swap, we can type:
sudo mkswap /swapfile

## Now, tell the system it can use the swap file by typing:
sudo swapon /swapfile

## Our system is using the swap file for this session, but we need to modify a 
## system file so that our server will do this automatically at boot. You can do this by typing:
sudo sh -c 'echo "/swapfile none swap sw 0 0" >> /etc/fstab'

## With this addition, your system should use your swap file automatically at each boot.
swapon -s