## Install git using yum

while [ -z "${GIT_NAME}" ]; do
  read -p "Input your name for GIT:" GIT_NAME
done

while [ -z "${GIT_EMAIL}" ]; do
  read -p "Input your email for GIT:" GIT_EMAIL
done

sudo yum install -y git
echo Git version: `git --version`

## git global config
git config --global user.name "${GIT_NAME}" 
git config --global user.mail "${GIT_EMAIL}"
git config --list

## generate ssh key if not exists
if [ ! -f "~/.ssh/id_rsa" ]; then
  yes "" | ssh-keygen -t rsa -b 2048 -N "" -C "${GIT_EMAIL}"
fi
