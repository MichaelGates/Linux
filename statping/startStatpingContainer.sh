#!/usr/bin/env bash
## start statping

IMAGE_TAG=v0.90.74

docker run -d \
  -p 8888:8080 \
  --network web \
  --name statping \
  statping/statping:${IMAGE_TAG}
