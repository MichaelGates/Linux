## Install Maven

echo Prerequisites: Java

MAVEN_INSTALL_PATH=/data/maven
MAVEN_VERSION=3.5.4

sudo yum install -y wget
## Use us mirror by default, to choose other mirrors, see https://www.apache.org/mirrors/
## In China, change to http://mirrors.hust.edu.cn/apache/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz
wget http://www-us.apache.org/dist/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz

tar -zxvf apache-maven-${MAVEN_VERSION}-bin.tar.gz
sudo mv apache-maven-${MAVEN_VERSION} ${MAVEN_INSTALL_PATH}

sudo cp maven_profile.sh /etc/profile.d/maven.sh
sudo sed -i "s/{MAVEN_INSTALL_PATH}/${MAVEN_INSTALL_PATH}/g" /etc/profile.d/maven.sh
. /etc/profile.d/maven.sh

## Check installation
mvn --version

echo Maven installed!
echo To start, see https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html