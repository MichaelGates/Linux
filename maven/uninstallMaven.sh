## Uninstall Maven

MAVEN_INSTALL_PATH=/data/maven
MAVEN_VERSION=3.5.4

rm -rf ${MAVEN_INSTALL_PATH}
sudo rm /etc/profile.d/maven.sh

echo Maven uninstalled!
