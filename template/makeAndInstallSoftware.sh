## Prerequisite
prerequisites=(
#  java
)
echo "Prerequisite: ${prerequisites[*]}"

## Prerequisites check
. ../tools/checkCommands.sh ${prerequisites[*]}

## prepare vairable
OS_USER=`whoami`

## Fill this:
SOFTWARE_NAME=
SOFTWARE_OS_USER=
SOFTWARE_VER_MAJOR=
SOFTWARE_VER_MINOR=
SOFTWARE_VER_MAINTENANCE=
SOFTWARE_INSTALL_PATH=

SOFTWARE_VER=${SOFTWARE_VER_MAJOR}.${SOFTWARE_VER_MINOR}.${SOFTWARE_VER_MAINTENANCE}
SRC_FILENAME=${SOFTWARE_NAME}-${SOFTWARE_VER}.tar.gz
SRC_UNZIP_FOLDER_NAME=${SOFTWARE_NAME}-${SOFTWARE_VER}
DOWNLOAD_URL=https://${SRC_FILENAME}

## Install config
INSTALL_AS_A_SERVICE=N
BUILD_FROM_SOURCE=N

## enter script folder
run_path=`pwd`
script_path=`cd "$(dirname $0)" && pwd`
cd ${script_path}

## Exit immediately if a command exits with a non-zero status.
set -e

echo Install ${SOFTWARE_NAME}...

## Fill this:
## Install dependencies
dependencies=(
gcc-c++
wget
)
sudo yum install -y ${dependencies[*]}

## Add os user
if [ ${SOFTWARE_OS_USER} ]; then
  if id -u ${SOFTWARE_OS_USER} >/dev/null 2>&1; then
    echo "OS user ${SOFTWARE_OS_USER} exists"
  else
    echo "Add OS user: ${SOFTWARE_OS_USER}"
    sudo useradd --create-home --comment "Account for running ${SOFTWARE_NAME}" --shell /bin/bash ${SOFTWARE_OS_USER}
  fi
fi

## Create installation folder
if [ -d "${SOFTWARE_INSTALL_PATH}" ]; then
  echo "Create installation folder: ${SOFTWARE_INSTALL_PATH}"
  sudo mkdir -p ${SOFTWARE_INSTALL_PATH}
fi

## Download source file
if [ ! -f "${SRC_FILENAME}" ]; then
  echo "Downloading: ${DOWNLOAD_URL}"
  wget -c ${DOWNLOAD_URL}
fi

if [ "${BUILD_FROM_SOURCE}" == 'N' -a -d "${SOFTWARE_INSTALL_PATH}" ]; then
  echo "Unzip source file [ ${SRC_FILENAME} ] to [ ${SOFTWARE_INSTALL_PATH} ]"
  sudo tar zxvf ${SRC_FILENAME} -C ${SOFTWARE_INSTALL_PATH}
fi

if [ "${BUILD_FROM_SOURCE}" == 'Y' ]; then
  tar zxvf ${SRC_FILENAME}
  cd ${SRC_UNZIP_FOLDER_NAME}

  ## configure options
  configure_opts=(
  #  --prefix=${SOFTWARE_INSTALL_PATH}
  )

  ./configure ${configure_opts[*]}

  ## make options
  make_opts=(
  #  PREFIX=${SOFTWARE_INSTALL_PATH}
  )

  make ${make_opts[*]}
  if [ $? -ne 0 ]; then
    echo make ${SOFTWARE_NAME} error!
    exit 1
  fi

  sudo make install
  if [ $? -ne 0 ]; then
    echo Install ${SOFTWARE_NAME} error!
    exit 1
  fi

fi

#if [ ! -f "/usr/sbin/${SOFTWARE_NAME}" ]; then
#  sudo ln -s ${SOFTWARE_INSTALL_PATH}/sbin/${SOFTWARE_NAME} /usr/sbin/${SOFTWARE_NAME}
#fi

cd ..
rm -rf ${SRC_UNZIP_FOLDER_NAME}

## copy configuration files
# mkdir -p ${SOFTWARE_INSTALL_PATH}/conf
# cp -pr conf/* ${SOFTWARE_INSTALL_PATH}/conf/

## Change install path owner to software user
if [ ${SOFTWARE_OS_USER} ]; then
  sudo chown -R ${SOFTWARE_OS_USER}:${SOFTWARE_OS_USER} ${SOFTWARE_INSTALL_PATH}
fi

## install as service
if [ "${INSTALL_AS_A_SERVICE}" == "Y" ]; then
  . ../tools/installService.sh ${SOFTWARE_NAME}
fi

cd ${run_path}

echo ${SOFTWARE_NAME} installed!
