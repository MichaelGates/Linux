#!/usr/bin/env bash
## start mysql container for atlassian

ansible-playbook mysql_container.yml -e @vars/mysqld_vars_atlassian.yml
