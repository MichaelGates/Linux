#!/usr/bin/env bash

MYSQL_USER=root
MYSQL_PASSWORD=password
MYSQL_DATABASE=database
FILENAME=export

mysqldump --max_allowed_packet=1024M -u ${MYSQL_USER} -p ${MYSQL_PASSWORD} ${MYSQL_DATABASE} --routines > ${FILENAME}.sql
