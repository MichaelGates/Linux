#!/usr/bin/env bash

MYSQL_USER=root
MYSQL_PASSWORD=password
MYSQL_DATABASE=database
FILENAME=export

mysql -u ${MYSQL_USER} -p ${MYSQL_PASSWORD} ${MYSQL_DATABASE} < ${FILENAME}.sql
