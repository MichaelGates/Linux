## Delte mysql database

while [ -z "${MYSQL_ROOT_PASSWORD}" ]; do
  read -s -p "Input mysql root password: " MYSQL_ROOT_PASSWORD
done

while [ -z "${DB_NAME}" ]; do
  read -p "Input to be deleted mysql database name: " DB_NAME
done

cat > statement.sql <<EOL
DROP DATABASE IF EXISTS ${DB_NAME};
quit
EOL

mysql -u "root" "-p${MYSQL_ROOT_PASSWORD}" < "statement.sql"

rm statement.sql

echo Mysql database \'${DB_NAME}\' deleted!
