#!/usr/bin/env bash
## install phpmyadmin

SOFTWARE_NAME=PMA

type installSoftwareInDockerMode

if [ $? != 0 ]; then
  "installSoftwareInDockerMode" not found!
  exit;
fi

installSoftwareInDockerMode ${SOFTWARE_NAME}

echo Done!
