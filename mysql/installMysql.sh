## Install mysql

## install wget
sudo yum install -y wget yum-utils
## get mysql repo
wget https://repo.mysql.com//mysql80-community-release-el7-1.noarch.rpm

echo Adding the MySQL Yum Repository
sudo rpm -Uvh mysql80-community-release-el7-1.noarch.rpm

echo Disable mysql v8.0 and enable v5.7
sudo yum-config-manager --disable mysql80-community
sudo yum-config-manager --enable mysql57-community

sudo yum install -y mysql-community-server
echo Mysql install complete!

echo Remove mysql repo
sudo yum -y remove mysql80-community-release-el7-1.noarch

sudo systemctl enable mysqld
sudo systemctl start mysqld

## below will execute mysql sucure installation
mysql_secure_installation

## below will manual change mysql root password
# random_password=`grep "password" /var/log/mysqld.log | awk '{ print $NF }'`
# echo Default root password: ${random_password} 
# echo You can change it using : ALTER USER \'root\'@\'localhost\' IDENTIFIED BY \'New_Password\';

echo Mysql installed!