## Create Database and username

while [ -z "${MYSQL_ROOT_PASSWORD}" ]; do
  read -s -p "Input mysql root password: " MYSQL_ROOT_PASSWORD
done

while [ -z "${DB_NAME}" ]; do
  read -p "Input NEW database name: " DB_NAME
done

while [ -z "${DB_USERNAME}" ]; do
  read -p "Input NEW database username: " DB_USERNAME
done

while [ -z "${DB_PASSWORD}" ]; do
  read -p "Input NEW database password: " DB_PASSWORD
done

cat > statement.sql <<EOL
CREATE DATABASE ${DB_NAME};
CREATE USER ${DB_USERNAME}@localhost IDENTIFIED BY '${DB_PASSWORD}';
GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO ${DB_USERNAME}@localhost IDENTIFIED BY '${DB_PASSWORD}';
FLUSH PRIVILEGES;
quit
EOL

mysql -u "root" "-p${MYSQL_ROOT_PASSWORD}" < "statement.sql"

rm statement.sql

echo Mysql database and user created!
