## Delte mysql user

while [ -z "${MYSQL_ROOT_PASSWORD}" ]; do
  read -s -p "Input mysql root password: " MYSQL_ROOT_PASSWORD
done

while [ -z "${DB_USERNAME}" ]; do
  read -p "Input to be deleted mysql username:" DB_USERNAME
done

cat > statement.sql <<EOL
DROP USER IF EXISTS '${DB_USERNAME}'@'localhost';
quit
EOL

mysql -u "root" "-p${MYSQL_ROOT_PASSWORD}" < "statement.sql"

rm statement.sql

echo Mysql user \'${DB_USERNAME}\' deleted!
