#!/usr/bin/env bash
## install MySQL

SOFTWARE_NAME=MySql
JDBC_HOME=~/data/jdbc

type installSoftwareInDockerMode

if [ $? != 0 ]; then
  "installSoftwareInDockerMode" not found!
  exit;
fi

installSoftwareInDockerMode ${SOFTWARE_NAME}

if [ ! -d "${JDBC_HOME}" ]; then
  cp -pr jdbc ~/data/jdbc
fi

echo Done!
