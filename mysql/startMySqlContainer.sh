#!/bin/bash
## Start mysql in docker
## Parameter CONFIG_FILE_NAME

CONTAINER_CONFIG_PATH=/etc/docker.d
CONFIG_SUFFIX=.mysql.conf
DATA_STORE_PATH=~/data/mysql

Usage="Usage: $0 [CONFIG_FILE_NAME]"

if [ ! -z "$1" ]; then
  CONFIG_FILE_NAME=$1
fi

listContainerConfigFiles ${CONFIG_SUFFIX}

while [ -z "${CONFIG_FILE_NAME}" ]; do
  read -p "Input MySQL CONFIG_FILE_NAME: " CONFIG_FILE_NAME
done

## check file
filefound=0
if [ -f ${CONTAINER_CONFIG_PATH}/${CONFIG_FILE_NAME}${CONFIG_SUFFIX} ]; then
  filefound=1
  FILE_NAME=${CONTAINER_CONFIG_PATH}/${CONFIG_FILE_NAME}${CONFIG_SUFFIX}
fi
if [ -f ${CONTAINER_CONFIG_PATH}/${CONFIG_FILE_NAME} ]; then
  filefound=1
  FILE_NAME=${CONTAINER_CONFIG_PATH}/${CONFIG_FILE_NAME}
fi

if [ $filefound -eq 0 ]; then
  echo "The config file ${CONFIG_FILE_NAME} is not exist. Please check it."
  echo "If you do not have a config file, please initial one."
  exit 1
fi

## import environment variable
if [ $filefound -eq 1 ]; then
  echo ===== BEGIN ===== ${FILE_NAME} =====
  cat ${FILE_NAME}
  echo ===== END ===== ${FILE_NAME} =====
  read -n1 -p "Are you sure to start? (y/N) " YN
  echo
  if [ "${YN}" == "y" -o "${YN}" == "Y" ]; then
    echo -n Import mysql environment variable.
    . ${FILE_NAME}
    echo Done!
  else
    echo Quit!
    exit 0
  fi
fi

PORTS_EXPOSE=""
if [ "${MYSQL_EXPOSE_PORT}" != "0" ]; then
  PORTS_EXPOSE="-p ${MYSQL_EXPOSE_PORT}:3306"
fi

## start mysql container
docker run \
  --name ${MYSQL_CONTAINER_NAME} \
  --network=${MYSQL_DOCKER_NETWORK} \
  ${PORTS_EXPOSE} \
  --restart always \
  -v ${DATA_STORE_PATH}/${MYSQL_CONTAINER_NAME}:/var/lib/mysql \
  -v ${MYSQL_CONFIG_FILE}:/etc/mysql/conf.d/customer.my.cnf \
  -e MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD} \
  -e TZ=Asia/Hong_Kong \
  -d mysql:${MYSQL_VERSION} \
  --character-set-server=utf8mb4 \
  --collation-server=utf8mb4_unicode_ci
