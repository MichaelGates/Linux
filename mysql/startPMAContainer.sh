#!/bin/bash
## start phpMyAdmin container

CONTAINER_CONFIG_PATH=/etc/docker.d
CONFIG_SUFFIX=.pma.conf
SOFTWARE_NAME=phpMyAdmin
IMAGE_NAME=phpmyadmin/phpmyadmin
CONTAINER_INNER_PORT=80

Usage="Usage: $0 [CONFIG_NAME]"

if [ ! -z "$1" ]; then
  CONFIG_NAME=$1
fi

listContainerConfigFiles ${CONFIG_SUFFIX}

while [ -z "${CONFIG_NAME}" ]; do
  read -p "Input ${SOFTWARE_NAME} CONFIG_NAME: " CONFIG_NAME
done

## check file
filefound=0
if [ -f ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX} ]; then
  filefound=1
  FILE_NAME=${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
fi

if [ $filefound -eq 0 ]; then
  echo "The config file ${CONFIG_NAME} is not exist. Please check it."
  echo "If you do not have a config file, please initial one."
  exit 1
fi

## import environment variable
. ${FILE_NAME}

PORTS_EXPOSE=""
if [ "${EXPOSE_PORT}" != "0" -a "${EXPOSE_PORT}" != "" ]; then
  PORTS_EXPOSE="-p ${EXPOSE_PORT}:${CONTAINER_INNER_PORT}"
fi

REMOVE_FLAG_PARAM="--rm"
if [ "${REMOVE_CONTAINER_AFTER_STOP}" = "n" -o "${REMOVE_CONTAINER_AFTER_STOP}" = "N" ]; then
  REMOVE_FLAG_PARAM=""
fi

RESTART_POLICY="--restart always"
if [ "${REMOVE_FLAG_PARAM}" ]; then
  RESTART_POLICY=""
fi
COMMAND=(
docker run ${REMOVE_FLAG_PARAM}
  ${RESTART_POLICY}
  --name ${CONTAINER_NAME}
  --network ${CONTAINER_NETWORK}
  ${PORTS_EXPOSE}
  --env-file ${CONTAINER_CONFIG_PATH}/common.env
  -e PMA_HOST=${PMA_HOST}
  -e PMA_PORT=${PMA_PORT}
  -d ${IMAGE_NAME}:${IMAGE_TAG}
)

echo ===== BEGIN =====
echo ${COMMAND[*]}
echo ===== END =====

read -n1 -p "Are you sure to start? (y/N) " YN
echo
if [ "${YN}" == "y" -o "${YN}" == "Y" ]; then
  echo -n Start ${SOFTWARE_NAME} container ${CONTAINER_NAME}...
  ${COMMAND[*]}
  echo Done!
else
  echo Quit!
fi
