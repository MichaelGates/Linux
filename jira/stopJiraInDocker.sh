## stop JIRA SOFTWARE
. ./jira.conf

## stop container
if [ "$(docker ps -q -f name=${CONTAINER_NAME})" ]; then
  docker stop --time 300 ${CONTAINER_NAME}
fi

## remove container
if [ "$(docker ps -aq -f status=exited -f name=${CONTAINER_NAME})" ]; then
  docker rm ${CONTAINER_NAME}
fi