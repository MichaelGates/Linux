#!/usr/bin/env bash
## run JIRA in docker

CONTAINER_CONFIG_PATH=/etc/docker.d
SOFTWARE_NAME=Jira
CONFIG_SUFFIX=.${SOFTWARE_NAME,,}.conf
DATA_STORE_PATH=~/data/${SOFTWARE_NAME,,}
JDBC_HOME=~/data/jdbc

Usage="Usage: $0 [CONFIG_FILE_NAME]"

if [ ! -z "$1" ]; then
  CONFIG_FILE_NAME=$1
fi

listContainerConfigFiles ${CONFIG_SUFFIX}

while [ -z "${CONFIG_FILE_NAME}" ]; do
  read -p "Input JIRA CONFIG_FILE_NAME: " CONFIG_FILE_NAME
done

## check file
filefound=0
if [ -f ${CONTAINER_CONFIG_PATH}/${CONFIG_FILE_NAME}${CONFIG_SUFFIX} ]; then
  filefound=1
  FILE_NAME=${CONTAINER_CONFIG_PATH}/${CONFIG_FILE_NAME}${CONFIG_SUFFIX}
fi

if [ $filefound -eq 0 ]; then
  echo "The config file ${CONFIG_FILE_NAME} is not exist. Please check it."
  echo "If you do not have a config file, please initial one."
  exit 1
fi

## import environment variable
if [ $filefound -eq 1 ]; then
  echo ===== BEGIN ===== ${FILE_NAME} =====
  cat ${FILE_NAME}
  echo ===== END ===== ${FILE_NAME} =====
  read -n1 -p "Are you sure to start? (y/N) " YN
  echo
  if [ "${YN}" == "y" -o "${YN}" == "Y" ]; then
    echo -n Importing jira environment variable...
    . ${FILE_NAME}
    echo Done!
  else
    echo Quit!
    exit 0
  fi
fi

PORTS_EXPOSE=""
if [ "${JIRA_EXPOSE_PORT}" != "0" ]; then
  PORTS_EXPOSE="-p ${JIRA_EXPOSE_PORT}:8080"
fi

HTTPS_CONFIG=""
if [ "${JIRA_HTTPS_ENABLE}" == "y" -o "${JIRA_HTTPS_ENABLE}" == "Y" ]; then
  HTTPS_CONFIG="${HTTPS_CONFIG} -e ATL_PROXY_PORT=443 "
  HTTPS_CONFIG="${HTTPS_CONFIG} -e ATL_TOMCAT_SCHEME=https "
  HTTPS_CONFIG="${HTTPS_CONFIG} -e ATL_TOMCAT_SECURE=true "
fi

docker run \
  --name ${JIRA_CONTAINER_NAME} \
  --network ${JIRA_DOCKER_NETWORK} \
  --restart always \
  ${PORTS_EXPOSE} \
  --stop-timeout 300 \
  -v ${DATA_STORE_PATH}/${JIRA_CONTAINER_NAME}:/var/atlassian/application-data/jira \
  -v ${JDBC_HOME}/mysql-connector-java-8.0.27.jar:/opt/atlassian/jira/lib/mysql-connector-java-8.0.27.jar \
  --env-file ${CONTAINER_CONFIG_PATH}/common.env \
  -e ATL_PROXY_NAME=${JIRA_DOMAIN_NAME} \
  ${HTTPS_CONFIG} \
  -d atlassian/jira-software:${JIRA_VERSION}
