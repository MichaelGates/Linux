## start jira with docker-compose

## set configuration
. ./jira_const.sh

## copy the nginx configuration file to nginx config path
cp -p $PWD/conf/jira_docker.https.conf ${NGINX_CONFIG_PATH}/conf.d/${JIRA_DOMAIN_NAME}.https.conf

export JIRA_DOMAIN_NAME

## start with docker-compose
docker-compose -f docker-compose.yml -f ../nginx/docker-compose.yml up -d --no-recreate
