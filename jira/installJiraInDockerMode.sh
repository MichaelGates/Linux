#!/usr/bin/env bash
## install Jira Software

SOFTWARE_NAME=Jira
type installSoftwareInDockerMode

if [ $? != 0 ]; then
  "installSoftwareInDockerMode" not found!
  exit;
fi

installSoftwareInDockerMode ${SOFTWARE_NAME}
