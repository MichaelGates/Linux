## Install jira with docker image

JIRA_INSTALL_HOME=~/jira
JIRA_SERVICE_NAME=jira

if [ ! -d "${JIRA_INSTALL_HOME}" ]; then
  mkdir ${JIRA_INSTALL_HOME}
fi

while [ -z "${JIRA_DOMAIN_NAME}" ]; do
  read -p "Input JIRA_DOMAIN_NAME of this server: " JIRA_DOMAIN_NAME
done

while [ -z "${JIRA_PORT}" ]; do
  read -p "Input JIRA_PORT of this server: " JIRA_PORT
done

## inital Jira configuration
. ./initialJira.sh

cp -pr conf/jira_docker.conf ${JIRA_INSTALL_HOME}/conf/jira_docker.conf

. ../nginx/addNginxConfWithDocker.sh ${JIRA_INSTALL_HOME}/conf/jira_docker.conf

## pull docker image
docker pull atlassian/jira-software
