## Prerequisite JRE
echo "Prerequisite: JRE"

## Prerequisites check
. ../tools/checkCommands.sh java

DEFAULT_JIRA_VER=8.5.1
OS_JIRA_HOME=/home/jira
JIRA_INSTALL_PATH=${OS_JIRA_HOME}/jirasoftware
JIRA_HOME=${OS_JIRA_HOME}/jirasoftware-home

read -p "Input JIRA DOMAIN_NAME: " DOMAIN_NAME
if [ -z "$DOMAIN_NAME" ]; then
  DOMAIN_NAME=localhost
fi
export DOMAIN_NAME

read -p "Input JIRA_PORT (8080 as default): " JIRA_PORT
if [ -z "$JIRA_PORT" ]; then
  JIRA_PORT=8080
fi
export APP_PORT=${JIRA_PORT}

sudo yum install -y wget

## Read JIRA version
read -p "Input JIRA version (${DEFAULT_JIRA_VER}): " JIRA_VER
if [ -z "$JIRA_VER" ]; then
  JIRA_VER=${DEFAULT_JIRA_VER}
fi

## Download jira install file if not exist
if [ ! -f "atlassian-jira-software-${JIRA_VER}.tar.gz" ]; then 
  wget https://product-downloads.atlassian.com/software/jira/downloads/atlassian-jira-software-${JIRA_VER}.tar.gz
fi

sudo useradd --create-home --comment "Account for running JIRA Software" --shell /bin/bash jira
sudo tar -xzvf atlassian-jira-software-${JIRA_VER}.tar.gz -C ${OS_JIRA_HOME}

sudo mv ${OS_JIRA_HOME}/atlassian-jira-software-${JIRA_VER}-standalone ${OS_JIRA_HOME}/jirasoftware
sudo mkdir -p ${JIRA_HOME}

## change owner of ${JIRA_INSTALL_PATH} and ${JIRA_HOME}
sudo chown -R jira:jira ${JIRA_INSTALL_PATH}
sudo chown -R jira:jira ${JIRA_HOME}

## backup default properties file
sudo cp -pr ${JIRA_INSTALL_PATH}/atlassian-jira/WEB-INF/classes/jira-application.properties ${JIRA_INSTALL_PATH}/atlassian-jira/WEB-INF/classes/jira-application.properties.default

line=`sudo grep "jira.home=${JIRA_HOME}" ${JIRA_INSTALL_PATH}/atlassian-jira/WEB-INF/classes/jira-application.properties`
if [ -z "$line" ]; then
  echo Config jira-application.properties
  # append JIRA_HOME config to properties file
  # sudo su - jira -c "echo -e \"\njira.home=${JIRA_HOME}\" >> ${JIRA_INSTALL_PATH}/atlassian-jira/WEB-INF/classes/jira-application.properties"
  # overwrite JIRA_HOME properties file
  sudo su - jira -c "echo jira.home=${JIRA_HOME} > ${JIRA_INSTALL_PATH}/atlassian-jira/WEB-INF/classes/jira-application.properties"
fi

## backup default server.xml
sudo cp -pr ${JIRA_INSTALL_PATH}/conf/server.xml ${JIRA_INSTALL_PATH}/conf/server_default.xml
sudo cp -pr conf/server.xml ${JIRA_INSTALL_PATH}/conf/server.xml
if [ -n "$DOMAIN_NAME" ]; then
  echo Config server.xml
  sudo sed -i "s/{JIRA_PORT}/${JIRA_PORT}/g" ${JIRA_INSTALL_PATH}/conf/server.xml
  sudo sed -i "s/{DOMAIN_NAME}/${DOMAIN_NAME}/g" ${JIRA_INSTALL_PATH}/conf/server.xml
fi

## change owner of ${JIRA_INSTALL_PATH} and ${JIRA_HOME}
sudo chown -R jira:jira ${JIRA_INSTALL_PATH}
sudo chown -R jira:jira ${JIRA_HOME}
## remove other's rights of ${JIRA_INSTALL_PATH} and ${JIRA_HOME}
sudo chmod -R u=rwx,go-rwx ${JIRA_INSTALL_PATH}
sudo chmod -R u=rwx,go-rwx ${JIRA_HOME}

## Add jira to nginx conf
../nginx/addNginxConf.sh conf/jira.https.conf

## Install jira as a system service
../tools/installService.sh jira
echo Starting jira...
sudo systemctl start jira

## Check jira every 5 minute. If jira is down, then start it.
cp -pr checkAndStartJira.sh ~/cron/
../tools/addCrontab.sh "*/30 * * * * sh cron/checkAndStartJira.sh"

echo Finished!
