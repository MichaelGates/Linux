## start jira with docker-compose

## set configuration
. ./jira_const.sh

if [ -z "${NGINX_CONFIG_PATH}" ]; then
  echo Please set '${NGINX_CONFIG_PATH}'!
  exit 1;
fi

docker stop ${CONTAINER_NAME} && docker rm ${CONTAINER_NAME}

rm ${NGINX_CONFIG_PATH}/conf.d/${JIRA_DOMAIN_NAME}.https.conf
