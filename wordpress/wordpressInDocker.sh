
docker run --rm \
  --name wordpress \
  --network wordpress \
  -p 8080:80 \
  -e TZ=Asia/Hong_Kong \
  -e WORDPRESS_DB_HOST=wp_db:3306 \
  -e WORDPRESS_DB_USER=wp \
  -e WORDPRESS_DB_PASSWORD=password \
  -e WORDPRESS_DB_NAME=wp \
  -e WORDPRESS_TABLE_PREFIX='wp_' \
  -d wordpress

