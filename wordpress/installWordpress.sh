## Install wordpress

## Change to script path
curr_path=`pwd`
script_path=`cd "$(dirname $0)" && pwd`
cd ${script_path}
myself=`whoami`

while [ -z "${DOMAIN_NAME}" ]; do
  read -p "Input wordpress domain name: " DOMAIN_NAME
done

while [ -z "${DB_NAME}" ]; do
  read -p "Input wordpress database name:" DB_NAME
done

while [ -z "${DB_USERNAME}" ]; do
  read -p "Input wordpress database username: " DB_USERNAME
done

while [ -z "${DB_PASSWORD}" ]; do
  read -p "Input wordpress database password: " DB_PASSWORD
done

WORDPRESS_INSTALL_PATH=/var/www/${DOMAIN_NAME}

echo Install WordPress...

sudo yum install -y wget

if [ ! -f latest.tar.gz ]; then
  wget https://wordpress.org/latest.tar.gz
fi

tar -zxvf latest.tar.gz
sudo rm -rf ${WORDPRESS_INSTALL_PATH}
sudo mkdir -p ${WORDPRESS_INSTALL_PATH}
sudo cp -pr wordpress/* ${WORDPRESS_INSTALL_PATH}/
sudo chown -R ${myself}:${myself} ${WORDPRESS_INSTALL_PATH}/

echo Config wordpress...
## copy the config file
cp -pr ${WORDPRESS_INSTALL_PATH}/wp-config-sample.php ${WORDPRESS_INSTALL_PATH}/wp-config.php

sed -i "s/database_name_here/${DB_NAME}/g" ${WORDPRESS_INSTALL_PATH}/wp-config.php
sed -i "s/username_here/${DB_USERNAME}/g" ${WORDPRESS_INSTALL_PATH}/wp-config.php
sed -i "s/password_here/${DB_PASSWORD}/g" ${WORDPRESS_INSTALL_PATH}/wp-config.php

## generate salt
curl https://api.wordpress.org/secret-key/1.1/salt/ > salt.txt
sed -i 's/$/\r/' salt.txt
lineBegin=`cat ${WORDPRESS_INSTALL_PATH}/wp-config.php -n | grep \'AUTH_KEY\' | awk '{print $1}'`
lineEnd=`cat ${WORDPRESS_INSTALL_PATH}/wp-config.php -n | grep \'NONCE_SALT\' | awk '{print $1}'`

## Delete default KEY lines
sed -i "${lineBegin},${lineEnd}d" ${WORDPRESS_INSTALL_PATH}/wp-config.php
## insert salt.txt to wp-config.php
sed -i "`expr ${lineBegin} - 1` r salt.txt" ${WORDPRESS_INSTALL_PATH}/wp-config.php

sudo chown -R apache:apache ${WORDPRESS_INSTALL_PATH}/

rm salt.txt
rm -rf wordpress

## Back to execute path
cd ${curr_path}

../certbot/genCert.sh ${DOMAIN_NAME}
../nginx/addNginxConf.sh conf/wordpress.https.conf

echo Wordpress installed!






