if [ ! -z "$1" ]; then
  WP_DOMAIN=$1
fi

## confirm WP_DOMAIN is not null
while [ -z "${WP_DOMAIN}" ] ; do
  read -p "Enter WP_DOMAIN: " WP_DOMAIN
done

docker exec -it wp_${WP_DOMAIN} chown -R www-data:www-data /var/www/html/wp-content/
