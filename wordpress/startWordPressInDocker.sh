## Usage:  $0 WP_DOMAIN

if [ ! -z "$1" ]; then
  WP_DOMAIN=$1
fi

## confirm WP_DOMAIN is not null
while [ -z "${WP_DOMAIN}" ] ; do
  read -p "Enter WP_DOMAIN: " WP_DOMAIN
done

export WP_DOMAIN

## up
docker-compose up -d
