# start mysql for wordpress in docker

docker run --rm \
  --name wp_db \
  -v ~/data/wpdb:/var/lib/mysql \
  -p 3306:3306 \
  --network wordpress \
  -e MYSQL_ROOT_PASSWORD=mysqlpassword \
  -e MYSQL_DATABASE=wp \
  -e MYSQL_USER=wp \
  -e MYSQL_PASSWORD=password \
  -e TZ=Asia/Hong_Kong \
  -d mysql:5.7
