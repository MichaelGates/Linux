## Prepare wordpress with docker

WORDPRESS_HOME=~/software/wordpress

echo -n Create wordpress working folder...
mkdir -p ${WORDPRESS_HOME}
echo Done!

echo -n Copying file to wordpress working folder...
cp -pr * ${WORDPRESS_HOME}
echo Done!
