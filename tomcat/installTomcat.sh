## Install Tomcat
## See http://tomcat.apache.org/
## Prerequisites: Java

echo Prerequisites: Java

## Prerequisites check
. ../tools/checkCommands.sh java
if [ $? -ne 0 ]; then
  exit 1
fi

read -p "Input tomcat install path \(/data/tomcat\): " TOMCAT_INSTALL_PATH
if [ -z "${TOMCAT_INSTALL_PATH}" ]; then
  TOMCAT_INSTALL_PATH=/data/tomcat
fi

read -p "Input tomcat version to be installed \(9.0.27\): " TOMCAT_VERSION
if [ -z "${TOMCAT_VERSION}" ]; then
  TOMCAT_VERSION=9.0.27
fi

read -p "Input tomcat listen port\(8080\): " TOMCAT_LISTEN_PORT
if [ -z "${TOMCAT_LISTEN_PORT}" ]; then
  TOMCAT_LISTEN_PORT=8080
fi

sudo yum install -y wget
## Use us mirror by default, to choose other mirrors, see https://www.apache.org/mirrors/
## In China, change to http://mirrors.hust.edu.cn/apache/tomcat/tomcat-${TOMCAT_VERSION:0:1}/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz
if [ ! -f "apache-tomcat-${TOMCAT_VERSION}.tar.gz" ]; then
  wget http://www-us.apache.org/dist/tomcat/tomcat-${TOMCAT_VERSION:0:1}/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz
fi

tar -zxvf apache-tomcat-${TOMCAT_VERSION}.tar.gz
sudo mv apache-tomcat-${TOMCAT_VERSION} ${TOMCAT_INSTALL_PATH}

chmod a+x -R ${TOMCAT_INSTALL_PATH}
cp -pr ${TOMCAT_INSTALL_PATH}/conf/server.xml ${TOMCAT_INSTALL_PATH}/conf/server-default.xml
sed -i "s/port=\"8080\"/port=\"${TOMCAT_LISTEN_PORT}\"/g" ${TOMCAT_INSTALL_PATH}/conf/server.xml

sudo cp tomcat_profile.sh /etc/profile.d/tomcat.sh
sudo sed -i "s/{TOMCAT_INSTALL_PATH}/${TOMCAT_INSTALL_PATH}/g" /etc/profile.d/tomcat.sh
. /etc/profile.d/tomcat.sh

echo Tomcat installed!
