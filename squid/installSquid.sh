#!/usr/bin/env bash
## install Squid, compile is optional
## Acceptable parameter: DOMAIN_NAME squid_username squid_password

## usage
function usage() {
  Usage="Usage: $0 [options] \n"
  Usage="${Usage}  options: \n"
  Usage="${Usage}\t -h        : help \n"
  Usage="${Usage}\t -c        : compile from source \n"
  Usage="${Usage}\t -d string : DOMAIN_NAME \n"
  Usage="${Usage}\t -u string : squid username \n"
  Usage="${Usage}\t -p string : squid password \n"
  echo -e ${Usage}
}

## parse options
while getopts 'hcd:u:p:' OPT; do
  case $OPT in
    h)
      HELP="TRUE";;
    c)
      COMPILE="TRUE";;
    d)
      DOMAIN_NAME="$OPTARG";;
    u)
      squid_username="$OPTARG";;
    p)
      squid_password="$OPTARG";;
    ?)
      usage
      exit 1
  esac
done

if [ "${HELP}" = "TRUE" -o "$1" = "--help" ]; then
  usage
  exit 0
fi

## input parameter if not set
function inputParam() {
  while [ -z "${DOMAIN_NAME}" ]; do
    read -p "Input DOMAIN_NAME: " DOMAIN_NAME
  done

  while [ -z "${squid_username}" ]; do
    read -p "Input squid username: " squid_username
  done

  while [ -z "${squid_password}" ]; do
    read -p "Input squid password: " squid_password
  done
}
inputParam


OS_USER=`whoami`
SQUID_VER_MAJOR=5
SQUID_VER_MINOR=3
SQUID_VER=${SQUID_VER_MAJOR}.${SQUID_VER_MINOR}
INSTALLATION_PATH=/data/squid
EXEC_PATH=`pwd`
OS_USER_SQUID=squid

## Create user if not exists
id ${OS_USER_SQUID} >& /dev/null
if [ $? -ne 0 ]; then
  echo Create squid user
  sudo useradd --system --no-create-home --shell /sbin/nologin --user-group ${OS_USER_SQUID}
fi

## Create folder if not exists
if [ ! -d "${INSTALLATION_PATH}" ]; then
  sudo mkdir -p ${INSTALLATION_PATH}
  sudo chown -R ${OS_USER}:${OS_USER} ${INSTALLATION_PATH}
fi

set -e

if [ "${COMPILE}" == "TRUE" ]; then
  echo Compile Squid from source code, this may take several minutes!
  sudo yum install -y perl gcc autoconf automake make sudo gcc-c++ openssl openssl-devel
  wget http://www.squid-cache.org/Versions/v${SQUID_VER_MAJOR}/squid-${SQUID_VER}.tar.gz
  tar zxvf squid-${SQUID_VER}.tar.gz
  cd squid-${SQUID_VER}
  ./configure --prefix=${INSTALLATION_PATH} --with-openssl
  make
  make install
  cd ..
  rm -rf squid-${SQUID_VER}
else
  . getOSVer
  OS_NAME=$ID
  OS_VERSION=${VERSION_ID}
  filename=squid-${SQUID_VER}-${OS_NAME}-${OS_VERSION}.tar.gz
  rm -f ${filename}
  wget https://raw.githubusercontent.com/MichaelXIE/squid/main/CentOS/${filename}
  echo Uncompress Squid...
  tar zxvf ${filename}
  sudo rm -rf ${INSTALLATION_PATH}
  sudo mv squid ${INSTALLATION_PATH}
fi

sudo ln -sf ${INSTALLATION_PATH}/sbin/squid /usr/sbin/squid

cp -pr conf/squid.conf ${INSTALLATION_PATH}/etc/squid.conf
sed -i "s/{DOMAIN_NAME}/${DOMAIN_NAME}/g" ${INSTALLATION_PATH}/etc/squid.conf

cd ${INSTALLATION_PATH}
touch passwd
sudo yum install -y httpd-tools
htpasswd -b passwd ${squid_username} ${squid_password}

cd ${EXEC_PATH}
cp -pr service/squid.origin service/squid
sed -i "s/{OS_USER}/${OS_USER}/g" service/squid
../tools/installService.sh squid
rm service/squid

## Add crontab job
addCrontab "5 0 1 * * sudo systemctl reload squid"

## systemctl has some error in CentOs 8
## use squid instead
#sudo systemctl start squid
squid

echo Squid installed!
