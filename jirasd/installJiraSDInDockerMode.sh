#!/bin/bash

JIRASD_HOME=~/data/jirasd
CONTAINER_CONFIG_PATH=/etc/docker.d

if [ ! -d "${JIRASD_HOME}" ]; then
  mkdir -p ${JIRASD_HOME}
fi
if [ ! -d "${CONTAINER_CONFIG_PATH}" ]; then
  sudo mkdir -p ${CONTAINER_CONFIG_PATH}
fi

echo Create links and template config file.
sudo rm /usr/local/bin/initialJiraSDContainerConfig
sudo ln -s ${PWD}/initialJiraSDContainerConfig.sh /usr/local/bin/initialJiraSDContainerConfig
sudo rm /usr/local/bin/startJiraSDContainer
sudo ln -s ${PWD}/startJiraSDContainer.sh /usr/local/bin/startJiraSDContainer
sudo cp template.jirasd.conf ${CONTAINER_CONFIG_PATH}/
echo Done!
