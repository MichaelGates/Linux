## start jira service desk with docker-compose

## set configuration
. ./jira_service_desk_const.sh

## copylink the nginx configuration file to nginx config path
cp -p $PWD/conf/jira_sd.https.conf ${NGINX_CONFIG_PATH}/conf.d/${JIRA_SD_DOMAIN_NAME}.https.conf

export JIRA_SD_DOMAIN_NAME

## start with docker-compose
docker-compose -f docker-compose.yml -f ../nginx/docker-compose.yml up -d --no-recreate
