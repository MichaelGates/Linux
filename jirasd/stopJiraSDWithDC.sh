## start jira service desk with docker-compose

## set configuration
. ./jira_service_desk_const.sh

if [ -z "${NGINX_CONFIG_PATH}" ]; then
  echo Please set '${NGINX_CONFIG_PATH}'!
  exit 1;
fi

docker stop ${CONTAINER_NAME} && docker rm ${CONTAINER_NAME}

rm ${NGINX_CONFIG_PATH}/conf.d/${JIRA_SD_DOMAIN_NAME}.https.conf
