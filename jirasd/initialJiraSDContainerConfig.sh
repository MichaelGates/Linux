#!/bin/bash
## Initial jira service desk docker config file

CONTAINER_CONFIG_PATH=/etc/docker.d
CONFIG_SUFFIX=.jirasd.conf
SOFTWARE_NAME=JIRASD

DEFAULT_CONTAINER_NAME=jirasd
DEFAULT_IMAGE_TAG=latest
DEFAULT_CONTAINER_NETWORK=webserver
DEFAULT_DOMAIN_NAME=localhost
DEFAULT_EXPOSE_PORT=8080
DEFAULT_HTTPS_ENABLE=y

echo Initial ${SOFTWARE_NAME} container config file ...

while [ -z "${CONFIG_NAME}" ]; do
  read -p "Input CONFIG_NAME: " CONFIG_NAME
  if [ -f ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX} ]; then
    echo "${CONFIG_NAME}${CONFIG_SUFFIX} already existed."
    echo ===== BEGIN ===== ${CONFIG_NAME}${CONFIG_SUFFIX} =====
    cat ${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
    echo ===== END ===== ${CONFIG_NAME}${CONFIG_SUFFIX} =====
    read -n1 -p "Overright? (y/N/q) " YN
    echo
    if [ "${YN}" == "q" -o "${YN}" == "Q" ]; then
      exit 0
    fi
    if [ "${YN}" != "y" -a "${YN}" != "Y" ]; then
      CONFIG_NAME=
    fi
  fi
done

read -p "Input CONTAINER_NAME (default:${DEFAULT_CONTAINER_NAME}): " CONTAINER_NAME
if [ -z "${CONTAINER_NAME}" ]; then
  CONTAINER_NAME=${DEFAULT_CONTAINER_NAME}
fi

read -p "Input IMAGE_TAG (default:${DEFAULT_IMAGE_TAG}): " IMAGE_TAG
if [ -z "${IMAGE_TAG}" ]; then
  IMAGE_TAG=${DEFAULT_IMAGE_TAG}
fi

read -p "Input CONTAINER_NETWORK (default:${DEFAULT_CONTAINER_NETWORK}): " CONTAINER_NETWORK
if [ -z "${CONTAINER_NETWORK}" ]; then
  CONTAINER_NETWORK=${DEFAULT_CONTAINER_NETWORK}
fi

read -p "Input DOMAIN_NAME (default:${DEFAULT_DOMAIN_NAME} ): " DOMAIN_NAME
if [ -z "$DOMAIN_NAME" ]; then
  DOMAIN_NAME=${DEFAULT_DOMAIN_NAME}
fi
read -p "Input EXPOSE_PORT (0 for not expose, default:${DEFAULT_EXPOSE_PORT}): " EXPOSE_PORT
if [ -z "${EXPOSE_PORT}" ]; then
  EXPOSE_PORT=${DEFAULT_EXPOSE_PORT}
fi

read -n1 -p "Input HTTPS_ENABLE (default:${DEFAULT_HTTPS_ENABLE}): " HTTPS_ENABLE
if [ -z "${HTTPS_ENABLE}" ]; then
  HTTPS_ENABLE=${DEFAULT_HTTPS_ENABLE}
fi

## initial constant shell
echo -n Generate ${SOFTWARE_NAME} container config file...
if [ ! -d ${CONTAINER_CONFIG_PATH} ]; then
  sudo mkdir -p ${CONTAINER_CONFIG_PATH}
fi
NEW_CONFIG_FILE=${CONTAINER_CONFIG_PATH}/${CONFIG_NAME}${CONFIG_SUFFIX}
sudo cp ${CONTAINER_CONFIG_PATH}/template${CONFIG_SUFFIX} ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{SOFTWARE_NAME}/${SOFTWARE_NAME}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CONTAINER_NAME}/${CONTAINER_NAME}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{IMAGE_TAG}/${IMAGE_TAG}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CONTAINER_NETWORK}/${CONTAINER_NETWORK}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{EXPOSE_PORT}/${EXPOSE_PORT}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{DOMAIN_NAME}/${DOMAIN_NAME}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{HTTPS_ENABLE}/${HTTPS_ENABLE}/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CREATE_TIME}/`date +'%Y-%m-%d %T'`/g" ${NEW_CONFIG_FILE}
sudo sed -i'' -e "s/{CREATE_USER}/`whoami`/g" ${NEW_CONFIG_FILE}
echo Done!

echo ===== BEGIN ===== ${NEW_CONFIG_FILE} =====
cat ${NEW_CONFIG_FILE}
echo ===== END ===== ${NEW_CONFIG_FILE} =====

echo Generate ${SOFTWARE_NAME} container config complete!
