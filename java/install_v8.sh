## Install Java

## Version: major.minor.maintenance_update：
JDK_VER_MAJOR=1
JDK_VER_MINOR=8
JDK_VER_MAINTENANCE=0
JDK_VER_UPDATE=131
JDK_VER_BUILD=b11
JDK_VER_HASH=d54c1d3a095b4ff2b6607d096fa80163
JDK_VER=${JDK_VER_MAJOR}.${JDK_VER_MINOR}.${JDK_VER_MAINTENANCE}_${JDK_VER_UPDATE}
JAVA_HOME=/usr/java/jdk${JDK_VER}

sudo yum install -y wget

if [ ! -f "jdk-${JDK_VER_MINOR}u${JDK_VER_UPDATE}-linux-x64.rpm" ]; then
  wget -N --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/${JDK_VER_MINOR}u${JDK_VER_UPDATE}-${JDK_VER_BUILD}/${JDK_VER_HASH}/jdk-${JDK_VER_MINOR}u${JDK_VER_UPDATE}-linux-x64.rpm"
fi

if [ ! -f "jdk-${JDK_VER_MINOR}u${JDK_VER_UPDATE}-linux-x64.rpm" ]; then
  echo "Download java install file error!"
  exit 1
fi

sudo rpm -ivh jdk-${JDK_VER_MINOR}u${JDK_VER_UPDATE}-linux-x64.rpm

## Add Java varable to system profile
cp -pr java_profile.sh java.sh
sed -i "s|{JAVA_HOME}|${JAVA_HOME}|g" java.sh
sudo mv java.sh /etc/profile.d/java.sh
. /etc/profile.d/java.sh

## Check installation result
echo Check installation
java -version

echo Java installed!