## Install Java 11

## Version: major.minor.maintenance_update：
JDK_VER_MAJOR=11
JDK_VER_MINOR=0
JDK_VER_MAINTENANCE=4
JDK_VER_BUILD=10
JDK_VER_HASH=cf1bbcbf431a474eb9fc550051f4ee78
JDK_VER=${JDK_VER_MAJOR}.${JDK_VER_MINOR}.${JDK_VER_MAINTENANCE}
INSTALL_FILE_NAME=jdk-${JDK_VER}_linux-x64_bin.rpm
JAVA_HOME=/usr/java/jdk-${JDK_VER}

sudo yum install -y wget

if [ ! -f ${INSTALL_FILE_NAME} ]; then
  wget -N --no-cookies --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/${JDK_VER}+${JDK_VER_BUILD}/${JDK_VER_HASH}/${INSTALL_FILE_NAME}"
fi

if [ ! -f ${INSTALL_FILE_NAME} ]; then
  echo "Download java install file error!"
  exit 1
fi

sudo rpm -ivh ${INSTALL_FILE_NAME}

## Add Java varable to system profile
cp -pr java_profile.sh java.sh
sed -i "s/{JAVA_HOME}/${JAVA_HOME}/g" java.sh
sudo mv java.sh /etc/profile.d/java.sh
. /etc/profile.d/java.sh

## Check installation result
echo Check installation
java -version

echo Java installed!
