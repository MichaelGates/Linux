#!/usr/bin/env bash

## Install GlusterFS Server
## see https://docs.gluster.org/en/latest/Quick-Start-Guide/Quickstart/

set -e

OS_VERSION=$(osinfo)
echo The OS VERSION is ${OS_VERSION}

function installGlusterInCentOS8() {
  ## centos 8 Unable to find a match: glusterfs-server by default
  ## https://www.server-world.info/en/note?os=CentOS_8&p=glusterfs8
  sudo dnf -y install centos-release-gluster8
  echo Install GlusterFS Server
  sudo dnf --enablerepo=centos-gluster8,powertools -y install glusterfs-server
  gluster --version
  sudo systemctl enable glusterd
}
if [ "${OS_VERSION}" == "centos 8" ]; then
  installGlusterInCentOS8
fi

function installGlusterInCentOS7() {
  sudo yum install glusterfs-server
  sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
  sudo dnf config-manager --set-enabled powertools
  sudo dnf --enablerepo=powertools install glusterfs-server
  gluster --version
  sudo systemctl enable glusterd
}
if [ "${OS_VERSION}" == "centos 7" ]; then
  installGlusterInCentOS7
fi
