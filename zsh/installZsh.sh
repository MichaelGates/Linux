
## install git and zsh
sudo yum install -y git zsh

## change shell to zsh
sudo chsh -s /bin/zsh `whoami`

## install oh-my-zsh with curl
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

## install oh-my-zsh with wget
#sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"

## install zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
sed -i "s/plugins=(/plugins=(zsh-autosuggestions /g" ~/.zshrc
. ~/.zshrc

echo "oh-my-zsh installed. You may need to re-login."
